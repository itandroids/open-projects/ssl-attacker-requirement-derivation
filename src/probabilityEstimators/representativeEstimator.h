#ifndef REPRESENTATIVE_ESTIMATOR_H
#define REPRESENTATIVE_ESTIMATOR_H

#include "probabilityEstimator.h"
#include "distribution_model.h"
#include "performanceMapper.h"
#include <fstream>
#include <vector>
#include <math.h>

class RepresentativeEstimator : public ProbabilityEstimator{
public:
  void setPrefix(std::string prefix);
  void setSamplingDistribution(DistributionModel &distributionModel);
  void setPerformanceMapper(PerformanceMapper &performanceMapper);
  double estimate(std::vector<double> variables) override;
private:
  std::string prefix;
  DistributionModel* distributionModel;
  PerformanceMapper* performanceMapper;
};

#endif