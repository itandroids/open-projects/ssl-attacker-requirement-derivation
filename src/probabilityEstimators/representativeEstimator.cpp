#include "representativeEstimator.h"
#include <iostream>

void RepresentativeEstimator::setSamplingDistribution(DistributionModel &distributionModel){
  this->distributionModel = &distributionModel;
}

void RepresentativeEstimator::setPerformanceMapper(PerformanceMapper &performanceMapper){
  this->performanceMapper = &performanceMapper;
}

void RepresentativeEstimator::setPrefix(std::string prefix){
  this->prefix = prefix;
}

double RepresentativeEstimator::estimate(std::vector<double> variables){
  if(variables.size() != 2){throw "error\n";}
  std::ifstream fileHandler;
  std::cout << prefix +
    std::to_string((int)std::round(variables[0]))+
    "-"+
    std::to_string((int)std::round(variables[1]))+
    ".txt\n";

  fileHandler.open(
    prefix+
    std::to_string((int)std::round(variables[0]))+
    "-"+
    std::to_string((int)std::round(variables[1]))+
    ".txt");
  performanceMapper->loadMap(fileHandler);
  fileHandler.close();
  const std::vector<double> &performanceMap = performanceMapper->getPerformanceMap();
  const std::vector<double> &distribution = distributionModel->getDistribution();
  int nSamples = performanceMap.size();
  double estimation = 0;
  for(int i = 0; i < nSamples; i++){
    estimation += performanceMap[i]*distribution[i];
  }

  return estimation;
}