#ifndef MONTE_CARLO_ESTIMATOR_H
#define MONTE_CARLO_ESTIMATOR_H

#include "probabilityEstimator.h"
#include "performanceModel.h"
#include "sampling_model.h"
#include <memory>

class MonteCarloEstimator : public ProbabilityEstimator{
public:
  void setNumberOfSamples(int nSamples);
  void setPerformanceModel(PerformanceModel &performanceModel, PerformanceModel::ParamType paramType);
  void setSamplingModel(SamplingModel &samplingModel);
  double estimate(std::vector<double> variables) override;

private:
  int nSamples;
  PerformanceModel* performanceModel;
  SamplingModel* samplingModel;
  PerformanceModel::ParamType paramType;
};

#endif