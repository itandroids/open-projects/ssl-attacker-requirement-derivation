#include "monteCarloEstimator.h"

void MonteCarloEstimator::setPerformanceModel(PerformanceModel &performanceModel, PerformanceModel::ParamType paramType){
  this->performanceModel = &performanceModel;
  this->paramType = paramType;
}

void MonteCarloEstimator::setSamplingModel(SamplingModel &samplingModel){
  this->samplingModel = &samplingModel;
}

void MonteCarloEstimator::setNumberOfSamples(int nSamples){
  this->nSamples = nSamples;
}

double MonteCarloEstimator::estimate(std::vector<double> variables){
  performanceModel->setParameters(variables,paramType);
  std::vector<double> sample;
  double estimation = 0;
  for(int i = 0; i < nSamples; i++){
    sample = samplingModel->getNewSample();
    estimation += performanceModel->getPerformance(sample);
  }
  return estimation / nSamples;
}