#ifndef PROBABILITY_ESTIMATOR_H
#define PROBABILITY_ESTIMATOR_H

#include<vector>

class ProbabilityEstimator{
public:
  virtual double estimate(std::vector<double> variables) = 0;
};

#endif