#include "performanceMapper.h"
#include "iostream"

PerformanceMapper::PerformanceMapper(){
}

const std::vector<double> &PerformanceMapper::getPerformanceMap(){
  return performance;
}

void PerformanceMapper::mapPerformance(PerformanceModel &performanceModel, 
                                      RepresentativeGrid &representativeGrid){
  const std::vector<std::vector<double>> &samples = representativeGrid.getRepresentativeSamples();
  int total = samples.size();
  performance.resize(total);
  for(int i = 0; i < total; i++){
    performance[i] = performanceModel.getPerformance(samples[i]);
  }
}

void PerformanceMapper::loadMap(std::ifstream &fileHandler){
  int nSamples;
  fileHandler >> nSamples;

  performance.resize(nSamples);
  for(int i = 0; i < nSamples; i++){
    fileHandler >> performance[i];
  }
}

void PerformanceMapper::saveMap(std::ofstream &fileHandler){
  int nSamples = performance.size();
  fileHandler << nSamples << "\n";
  for(int i = 0; i < nSamples; i++){
    fileHandler << performance[i] << "\n";
  }
}