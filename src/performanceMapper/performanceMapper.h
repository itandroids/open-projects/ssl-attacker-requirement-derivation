#ifndef PERFORMANCE_MAPPER_H
#define PERFORMANCE_MAPPER_H

#include <fstream>
#include "representativeGrid.h"
#include "performanceModel.h"

class PerformanceMapper{
public:
  PerformanceMapper();

  const std::vector<double> &getPerformanceMap();
  void mapPerformance(PerformanceModel &performanceModel, 
                        RepresentativeGrid &representativeGrid);

  void loadMap(std::ifstream &fileHandler);
  void saveMap(std::ofstream &fileHandler);
private:
  std::vector<double> performance;
};

#endif