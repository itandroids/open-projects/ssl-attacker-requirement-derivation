#ifndef CONSTANTS_H
#define CONSTANTS_H

#include "math.h"

class RobotParameters{
public:
  static constexpr double const MAX_SPEED = 5;/* [rad/s] */
  static constexpr double const MAX_ACC = 4; /* [rad/s^2] */
  static constexpr double const MAX_VREF = 5; /* [rad/s] */
};

class AnalysisParameters{
public:
  static constexpr double const MIN_KSI = 0;
  static constexpr double const MAX_KSI = 1;
  static constexpr double const MIN_F = 0; /* [Hz] */
  static constexpr double const MAX_F = 60;/* [Hz] */
  static constexpr double const MAX_ERROR = 20.0/180.0*M_PI/* 0.35 */; /* m */
  static constexpr double const MIN_ERROR = 0;

  static constexpr int const N_ITERATIONS = 25;
};

class GoalieTrajectory{
public:
  static constexpr double const GOALIE_RADIUS = 0.5;
  /* static constexpr bool const ALIGN_ROBOT = true; */
};

class GoalSampling{
public:
  static constexpr double const BALL_SPEED = 8;
  static constexpr double const MIN_RADIUS = 2;//Ball range
  static constexpr double const MAX_RADIUS = 3;
  static constexpr double const MIN_ANGLE = 0;//Ball range
  static constexpr double const MAX_ANGLE = M_PI;
  static constexpr int const N_SAMPLES = 1e6;
};

class GoalDistribution{
public:
  static constexpr double const MIN_TIME = 0.1;
  static constexpr double const MAX_TIME = 0.4;
  static constexpr double const MIN_ERROR = 0;
  static constexpr double const MAX_ERROR = M_PI/2/*M_PI/2 ; 0.35 */;
  static constexpr int const N_ITERATIONS = 50;
};

#endif