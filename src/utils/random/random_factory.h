#ifndef RANDOMFACTORY_H
#define RANDOMFACTORY_H

#include <random>
#include "types.h"

class RandomFactory{
private:  
  class Factory{
  public:
    Factory():normalDistribution(0,1){};
    std::random_device device;
    std::default_random_engine generator = std::default_random_engine(device());
    std::uniform_real_distribution<double> uniformDistribution;
    std::normal_distribution<double> normalDistribution;
  };
  static Factory* factory;

public:
  RandomFactory(){
    if(factory == NULL){
      factory = new Factory;
    }
  };
  Ball getRandomBall();
  Pose getRandomRobotPose();
  double getUniformValue(double min, double max);
  double getNormalValue(double mean, double variance);
};

#endif