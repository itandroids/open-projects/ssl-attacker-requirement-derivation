#include "constants.h"
#include "types.h"
#include <random>
#include "random_factory.h"

RandomFactory::Factory* RandomFactory::factory = NULL;

Ball RandomFactory::getRandomBall(){
  double R = getUniformValue(GoalSampling::MIN_RADIUS,GoalSampling::MAX_RADIUS);
  double angle = getUniformValue(GoalSampling::MIN_ANGLE,GoalSampling::MAX_ANGLE);
  Ball ball;
  ball.x = R*cos(angle);
  ball.y = R*sin(angle);

  Vector2 pos(ball.x,ball.y);
  Vector2 goalLim1 = Vector2(-0.5,0) - pos;
  Vector2 goalLim2 = Vector2(0.5,0) - pos;
  double goalAngle1 = goalLim1.angle();
  double goalAngle2 = goalLim2.angle();
  double ballAngle = getUniformValue(goalAngle1,goalAngle2);

  ball.vx = GoalSampling::BALL_SPEED * cos(ballAngle);
  ball.vy = GoalSampling::BALL_SPEED * sin(ballAngle);

  return ball;
}

Pose RandomFactory::getRandomRobotPose(){
  Pose pose;
  double robotAngle;
  robotAngle = M_PI * factory->uniformDistribution(factory->generator);
  pose.x = GoalieTrajectory::GOALIE_RADIUS*cos(robotAngle);
  pose.y = GoalieTrajectory::GOALIE_RADIUS*sin(robotAngle);
  pose.theta = robotAngle;
  return pose;
}

double RandomFactory::getUniformValue(double min, double max){
  return (min + factory->uniformDistribution(factory->generator)*(max - min));
}

double RandomFactory::getNormalValue(double mean, double variance){
  return mean + factory->normalDistribution(factory->generator)*variance;
}