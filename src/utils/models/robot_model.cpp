#include "robot_model.h"
#include <iostream>

RobotModel::RobotModel():rk4(9){
  rk4.f = [this](double& t, double*& x, double*& dxdt){
    return differentialEquation(t,x,dxdt);
  };
}

void RobotModel::step(double input[3]){
  this->input[0] = input[0];
  this->input[1] = input[1];
  this->input[2] = input[2];
  auxState = rk4.solve(state,stepDuration,integrationStep);
  // std::cout << "auxState[6]: " << auxState[6] << "\n";
  for(int i = 0; i < 9; i++){state[i] = auxState[i];}
  return;
}

void RobotModel::setState(double x[3], double y[3], double theta[3]){
  state[0] = x[0];
  state[1] = y[0];
  state[2] = theta[0];
  state[3] = x[1];
  state[4] = y[1];
  state[5] = theta[1];
  state[6] = x[2];
  state[7] = y[2];
  state[8] = theta[2];

  return;
}

void RobotModel::getState(double x[3], double y[3], double theta[3]){
  x[0] = state[0];
  y[0] = state[1];
  theta[0] = wrapToPi(state[2]);
  x[1] = state[3];
  y[1] = state[4];
  theta[1] = state[5];
  x[2] = state[6];
  y[2] = state[7];
  theta[2] = state[8];

  return;
}

void RobotModel::setState(State state){
  this->state[0] = state.x;
  this->state[1] = state.y;
  this->state[2] = state.theta;
  this->state[3] = 0;
  this->state[4] = 0;
  this->state[5] = 0;
}

void RobotModel::getState(State &state){
  state.x = this->state[0];
  state.y = this->state[1];
  state.theta = wrapToPi(this->state[2]);
  state.vx = this->state[3];
  state.vy = this->state[4];
  state.w = this->state[5];
}

void RobotModel::setStepDuration(double step){
  stepDuration = step;
  integrationStep = stepDuration / 10.0;

  return;
}

void RobotModel::setModelLinear(double xi, double wn){
  this->xiLinear = xi;
  this->wnLinear = wn;

  return;
}

void RobotModel::setModelRotation(double xi, double wn){
  this->xiRotation = xi;
  this->wnRotation = wn;

  return;
}

void RobotModel::setRobotSaturation(double maxLVel, double maxLAcc,
                                      double maxAVel, double maxAAcc){
  maxLinearVelocity = maxLVel;
  maxLinearAcceleration = maxLAcc;
  maxAngularVelocity = maxAVel;
  maxAngularAcceleration = maxAAcc;
  double upper[9] = {1e10,1e10,1e10,maxLVel,maxLVel,maxAVel,maxLAcc,maxLAcc,maxAAcc};
  rk4.setUpper(upper);
  double lower[9] = {-1e10,-1e10,-1e10,-maxLVel,-maxLVel,-maxAVel,-maxLAcc,-maxLAcc,-maxAAcc};
  rk4.setLower(lower);
  return;
}

void RobotModel::differentialEquation(double & t, double *& x, double *& dxdt){
  dxdt[0] = x[3]*cos(x[2]) - x[4]*sin(x[2]);
  dxdt[1] = x[3]*sin(x[2]) + x[4]*cos(x[2]);
  dxdt[2] = x[5];
  dxdt[3] = x[6];
  dxdt[4] = x[7];
  dxdt[5] = x[8];
  dxdt[6] = - 2*xiLinear*wnLinear*x[6] - wnLinear*wnLinear*x[3] + wnLinear*wnLinear*input[0];
  dxdt[7] = - 2*xiLinear*wnLinear*x[7] - wnLinear*wnLinear*x[4] + wnLinear*wnLinear*input[1];
  dxdt[8] = - 2*xiRotation*wnRotation*x[8] - wnRotation*wnRotation*x[5] + wnRotation*wnRotation*input[2];
  
  if(sqrt(x[3]*x[3]+x[4]*x[4]) > maxLinearVelocity){
    double projection = dxdt[3]*cos(x[2])+dxdt[4]*sin(x[2]);
    double tangent = -dxdt[3]*sin(x[2])+dxdt[4]*cos(x[2]);
    if(projection > 0) projection = 0;
    dxdt[3] = projection*cos(x[2]) - tangent*sin(x[2]);
    dxdt[4] = projection*sin(x[2]) + tangent*cos(x[2]);
  }

  if(abs(x[5]) > maxAngularVelocity){
    if(x[5]*dxdt[5]>0){
      dxdt[5]=0;
    }
  }
  if(abs(x[6]) > maxLinearAcceleration){
    if(x[6]*dxdt[6]>0){
      dxdt[6]=0;
    }
  }
  if(abs(x[7]) > maxLinearAcceleration){
    if(x[7]*dxdt[7]>0){
      dxdt[7]=0;
    }
  }
  if(abs(x[8]) > maxAngularAcceleration){
    if(x[8]*dxdt[8]>0){
      dxdt[8]=0;
    }
  }

  /* if(dxdt[0]*dxdt[0]+dxdt[1]*dxdt[1] > maxLinearVelocity*maxLinearVelocity){
    dxdt[0] *= (maxLinearVelocity)/sqrt(dxdt[0]*dxdt[0]+dxdt[1]*dxdt[1]);
    dxdt[1] *= (maxLinearVelocity)/sqrt(dxdt[0]*dxdt[0]+dxdt[1]*dxdt[1]);
  }
  if(abs(dxdt[2]) > maxAngularVelocity){
    dxdt[2] *= (maxAngularVelocity)/abs(dxdt[2]);
  }
  if(dxdt[3]*dxdt[3]+dxdt[4]*dxdt[4] > maxLinearAcceleration*maxLinearAcceleration){
    dxdt[3] *= (maxLinearAcceleration)/sqrt(dxdt[3]*dxdt[3]+dxdt[4]*dxdt[4]);
    dxdt[4] *= (maxLinearAcceleration)/sqrt(dxdt[3]*dxdt[3]+dxdt[4]*dxdt[4]);
  }
  if(abs(dxdt[5]) > maxAngularAcceleration){
    dxdt[5] *= (maxAngularAcceleration)/abs(dxdt[5]);
  } */
  
  return;
}


double RobotModel::wrapToPi(double angle){
  while(angle>M_PI)angle -= 2*M_PI;
  while(angle<=-M_PI)angle += 2*M_PI;

  return angle;
}