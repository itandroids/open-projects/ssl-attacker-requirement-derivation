#ifndef TASK_MODEL_H
#define TASK_MODEL_H

#include "queue"
#include "robot_model.h"
#include "fstream"
#include "types.h"
#include <vector>
#include "constants.h"
#include "random_factory.h"

class TaskModel{
public:
  TaskModel();
  ~TaskModel();

  double simulate(Interception &interception, Pose &robotPose, double mismatchRatio);

  //Linear
  void setEstimatorVarianceL(double variance);
  //Rotational
  void setEstimatorVarianceR(double variance);
  void setResolution(int resolutionX, int resolutionY);
  void setBaseStepPeriod(double baseStepPeriod);
  void setControlUpdateTime(double controlUpdateTime);
  void setDelayTime(double delayTime);
  void setControlParameters(double kp[3], double kd[3]);
  void setTrajectoryRadius(double rd);
  void setInterceptionTolerance(double tol);
  void setMaxVelocity(double maxVelocity);
  void setMaxAcceleration(double maxAcceleration);
  void setRobotModel(double xi, double wn);

private:
  double quantify(double value, double unitResolution);
  void positionControl(double error[3], double prevError[3], Interception &interception, double control[3]);
  void robotState(Pose robotPose, double state[3]);
  void robotDState(double localSpeed[3], double const state[3], double dState[3]);

  double wrap2pi(double angle);

  int numberOfSamples;

  int resX, resY;

  double estimatorVarianceL, estimatorVarianceR;

  double baseStepPeriod, controlUpdateTime, delayTime;

  double kp[3], kd[3], xi, wn, maxVelocity, maxAcceleration;
  double rd;

  RobotModel robot;

  double radialControl, distControl, angleControl;
  std::ofstream filePos, fileVel, fileInput, fileError, fileNoise, fileFinalState;

  RandomFactory randomFactory;
};

#endif