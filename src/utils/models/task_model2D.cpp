#include "task_model2D.h"
#include "fstream"
#include "iostream"

TaskModel2D::TaskModel2D(){
  kp[0] = kp[1] = 15;
  kp[2] = 12;
  kd[0] = kd[1] = 2;
  kd[2] = .2;

  maxLVel = 6;
  maxAVel = 2*M_PI*1.5;
  maxLAcc = 14;
  maxAAcc = 2*M_PI*6.5;

  worldModel.setEstimatorVarianceL(0);
  worldModel.setEstimatorVarianceR(0);
  worldModel.setResolution(1e5,1e5);
  
  worldModel.setSaturation(maxLVel,maxAVel,maxLAcc,maxAAcc);
  worldModel.setModelLinear(0.7,2*M_PI*30);
  worldModel.setModelRotation(0.7,2*M_PI*60);
  
  controlPeriod = 1.0/30;
  delayTime = 0;
  stepPeriod = 1.0/300;

  //Debug
  //filePos.open("debug/trajectoryTest.txt"/* , std::ofstream::out | std::ofstream::app */);
  // fileVel.open("debug/simulationTest-vel.txt"/* , std::ofstream::out | std::ofstream::app */);
  // fileInput.open("debug/simulationTest-input.txt"/* , std::ofstream::out | std::ofstream::app */);
  // fileError.open("debug/simulationTest-error.txt"/* , std::ofstream::out | std::ofstream::app */);
  // fileNoise.open("debug/simulationTest-noise.txt"/* , std::ofstream::out | std::ofstream::app */);
  // fileFinalState.open("debug/simulationTest-finalState.txt"/* , std::ofstream::out | std::ofstream::app */);
  //filePos << "t x y theta vx vy w refx refy reftheta refvx refvy refw cmd0 cmd1 cmd2\n";
  // fileVel << "t v vn wa\n";
  // fileInput << "t v_ref vn_ref wa_ref\n";
  // fileError << "t radialError distError angleError\n";
  // fileNoise << "t xNoise yNoise phiNoise\n";
  // fileFinalState << "t displacement\n";
}

TaskModel2D::~TaskModel2D(){
  //filePos.close();
  // fileVel.close();
  // fileInput.close();
  // fileError.close();
  // fileNoise.close();
  // fileFinalState.close();
}

void TaskModel2D::simulate(std::vector<State> trajectory, Pose initialPose, double &costPos, double &costVel, double &costTheta){
  
  int nWaypoints = trajectory.size();

  State currentState, cameraState;
  double controlCommand[3];

  double auxCostP = 0, auxCostV = 0, auxCostA = 0;

  worldModel.startWorld(initialPose,delayTime,1.0/30.0,stepPeriod);
  int controlRate = std::round(30.0*controlPeriod);

  for(int i = 0; i < nWaypoints; i++){
    worldModel.getCameraData(cameraState);
    if(i%controlRate==0){
      controlLaw(trajectory[i],cameraState,controlCommand);
    }
    worldModel.step(controlCommand);
    worldModel.getCurrentState(currentState);
    updateCost(currentState,trajectory[i],auxCostP,auxCostV, auxCostA);
    
    /* filePos << i*controlPeriod << " "
            << currentState.x << " "
            << currentState.y << " "
            << currentState.theta << " "
            << currentState.vx << " "
            << currentState.vy << " "
            << currentState.w << " "
            << trajectory[i].x << " "
            << trajectory[i].y << " "
            << trajectory[i].theta << " "
            << trajectory[i].vx << " "
            << trajectory[i].vy << " "
            << trajectory[i].w << " "
            << controlCommand[0] << " "
            << controlCommand[1] << " "
            << controlCommand[2] << "\n"; */
  }

  costPos += (auxCostP);
  costVel += (auxCostV);
  costTheta += (auxCostA);

  return;
}

void TaskModel2D::setModelLinear(double xi, double wn){
  worldModel.setModelLinear(xi,wn);
}

void TaskModel2D::setModelRotation(double xi, double wn){
  worldModel.setModelRotation(xi,wn);
}

void TaskModel2D::setTranslationalControl(double kp, double kd){
  this->kp[0] = this->kp[1] = kp;
  this->kd[0] = this->kd[1] = kd;
}

void TaskModel2D::setRotationalControl(double kp, double kd){
  this->kp[2] = kp;
  this->kd[2] = kd;
}

void TaskModel2D::setControlPeriod(double controlPeriod){
  this->controlPeriod = controlPeriod;
}

void TaskModel2D::setDelayTime(double delayTime){
  this->delayTime = delayTime;
}

void TaskModel2D::setStepPeriod(double stepPeriod){
  this->stepPeriod = stepPeriod;
}

void TaskModel2D::setResolution(int resolution){
  this->worldModel.setResolution(resolution,resolution);
}

void TaskModel2D::setTranslationalVariance(double variance){
  this->worldModel.setEstimatorVarianceL(variance);
}

void TaskModel2D::setRotationalVariance(double variance){
  this->worldModel.setEstimatorVarianceR(variance);
}

void TaskModel2D::setTranslationalVarianceVel(double variance){
  this->worldModel.setEstimatorVarianceVL(variance);
}

void TaskModel2D::setRotationalVarianceVel(double variance){
  this->worldModel.setEstimatorVarianceVR(variance);
}

void TaskModel2D::controlLaw(State reference, State currentState, double command[3]){
  double e_front = (reference.x-currentState.x)*cos(currentState.theta)+(reference.y-currentState.y)*sin(currentState.theta);
  /* double d_e_front = reference.vx*cos(currentState.theta)+reference.vy*sin(currentState.theta); */
  double d_e_front = (reference.vx-currentState.vx+currentState.w*(reference.y-currentState.y))*cos(currentState.theta)+
                      (reference.vy-currentState.vy-currentState.w*(reference.x-currentState.x))*sin(currentState.theta);
  double front_ff = reference.vx*cos(reference.theta)+reference.vy*sin(reference.theta);
  
  double e_side = -(reference.x-currentState.x)*sin(currentState.theta)+(reference.y-currentState.y)*cos(currentState.theta);
  /* double d_e_side = -reference.vx*sin(currentState.theta)+reference.vy*cos(currentState.theta); */
  double d_e_side = -(reference.vx-currentState.vx+currentState.w*(reference.y-currentState.y))*sin(currentState.theta)+
                      (reference.vy-currentState.vy-currentState.w*(reference.x-currentState.x))*cos(currentState.theta);
  double side_ff = -reference.vx*sin(reference.theta)+reference.vy*cos(reference.theta);
  
  double e_theta = wrap2pi(reference.theta-currentState.theta);

  command[0] = kp[0]*e_front+kd[0]*d_e_front+front_ff;
  
  command[1] = kp[1]*e_side+kd[1]*d_e_side+side_ff;
  /* command[1] = 0; */
  
  command[2] = kp[2]*e_theta + kd[2]*(reference.w - currentState.w)+reference.w;
  
  
  
  if(sqrt(command[0]*command[0] + command[1]*command[1])> maxLVel){
    double ratio = maxLVel / sqrt(command[0]*command[0] + command[1]*command[1]);
    command[0] *= ratio;
    command[1] *= ratio;
  }
  if(abs(command[2]) > maxAVel){
    double ratio = maxAVel / abs(command[2]);
    command[2] *= ratio;
  }

  return;
}

void TaskModel2D::updateCost(State currentState, State reference, double& costP, double& costV, double& costA){
  costP += (currentState.x-reference.x)*(currentState.x-reference.x) + 
            (currentState.y-reference.y)*(currentState.y-reference.y);
  costV += (currentState.vx-reference.vx)*(currentState.vx-reference.vx) + 
            (currentState.vy-reference.vy)*(currentState.vy-reference.vy);
  costA += wrap2pi(currentState.theta-reference.theta)*
                wrap2pi(currentState.theta-reference.theta);

  return;
}

double TaskModel2D::wrap2pi(double angle){
  while(angle>M_PI)angle -= 2*M_PI;
  while(angle<=-M_PI)angle += 2*M_PI;

  return angle;
}
