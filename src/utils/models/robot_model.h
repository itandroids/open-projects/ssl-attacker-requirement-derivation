#ifndef ROBOT_MODEL_H
#define ROBOT_MODEL_H 
 
#include "math.h"
#include "rk_4.h"
#include "functional"
#include "types.h"

class RobotModel{
public:
  RobotModel();

//state = [x,y,theta,dx,dy,dtheta,ddx,ddy,ddtheta]
//input = [vFront,vSide,vAngle];
  void step(double input[3]);

  void setState(double x[3], double y[3], double theta[3]);
  void setState(State state);
  void getState(double x[3], double y[3], double theta[3]);
  void getState(State &state);

  void setStepDuration(double step);
  void setModelLinear(double xi, double wn);
  void setModelRotation(double xi, double wn);
  void setRobotSaturation(double maxLVel, double maxLAcc,
                          double maxAVel, double maxAAcc);

private:
  double xiLinear, wnLinear, xiRotation, wnRotation, stepDuration, integrationStep;

  double maxLinearVelocity, maxLinearAcceleration;
  double maxAngularVelocity, maxAngularAcceleration;

  double input[3];
  double state[9];
  double const * auxState;

  RungeKutta4 rk4;

  void differentialEquation(double& t,double*& x,double*& dxdt);
  double wrapToPi(double angle);
};

#endif