#ifndef SAMPLING_MODEL_H
#define SAMPLING_MODEL_H

#include "types.h"
#include "constants.h"
#include "random_factory.h"
#include "fstream"
#include "vector"
#include "helper.h"

class SamplingModel{
public:
  SamplingModel(bool alignRobot):alignRobot(alignRobot){};
  Interception getInterception();

  Ball getBall();
  Pose getRobotInitialPose();

  std::vector<double> getNewSample();

private:
  bool alignRobot;
  Ball ball;
  Pose robotPose;

  RandomFactory factory;
};

#endif