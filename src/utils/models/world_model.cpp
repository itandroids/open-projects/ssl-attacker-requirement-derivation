#include "world_model.h"
#include "iostream"

WorldModel::WorldModel(){
  //fileWorld.open("debug/world_log.txt");
  //fileWorld << "t xNoise yNoise\n";
  //fileWorld << "t x y theta vx vy w\n";
  //iteration = 0;
}

void WorldModel::startWorld(Pose initialRobotPose, double delayTime, double controlPeriod, double stepPeriod){
  this->iteration = 0;
  this->delayTime = delayTime;
  this->controlPeriod = controlPeriod;
  this->mismatchTime = 0;//randomFactory.getUniformValue(0,1)*controlPeriod;
  this->stepPeriod = stepPeriod;
  robotModel.setStepDuration(stepPeriod);

  State initialState(initialRobotPose.x,initialRobotPose.y,initialRobotPose.theta,0,0,0);
  robotModel.setState(initialState);
  delayedState = std::queue<State>();
  delayedState.push(initialState);
  for(int i = 0; i < std::ceil((delayTime - mismatchTime) / controlPeriod);i++){
    delayedState.push(initialState);
  }
}

void WorldModel::step(double nextCommand[3]){
  int delaySteps = delayTime / stepPeriod;
  int controlSteps = controlPeriod / stepPeriod;
  int mismatchSteps = 0;//mismatchTime / stepPeriod;
  double xNoise, yNoise, phiNoise;
  double vxNoise, vyNoise, vphiNoise;
  for(int i = 0; i < controlSteps; i++){
    if((i+delaySteps)%controlSteps == mismatchSteps) {//Update delay array
      xNoise = randomFactory.getNormalValue(0,estimatorVarianceL);
      yNoise = randomFactory.getNormalValue(0,estimatorVarianceL);
      phiNoise = randomFactory.getNormalValue(0,estimatorVarianceR);
      //fileWorld << xNoise << " " << yNoise << "\n"; 

      vxNoise = randomFactory.getNormalValue(0,estimatorVarianceVL);
      vyNoise = randomFactory.getNormalValue(0,estimatorVarianceVL);
      vphiNoise = randomFactory.getNormalValue(0,estimatorVarianceVR);

      delayedState.push(State(quantify(currentState.x,1.0/resX)+xNoise,
                              quantify(currentState.y,1.0/resY)+yNoise,
                              currentState.theta+phiNoise,
                              currentState.vx+vxNoise,currentState.vy+vyNoise,currentState.w+vphiNoise));
    }
    if(i == mismatchSteps){//Update control
      for (int j = 0; j<3; j++){
        currentCommand[j] = nextCommand[j];
      }
    }

    robotModel.step(currentCommand);
    robotModel.getState(currentState);
    /* fileWorld << iteration*controlPeriod+i*stepPeriod << " "
              << currentState.x << " "
              << currentState.y << " "
              << currentState.theta << " "
              << currentState.vx*cos(currentState.theta)-currentState.vy*sin(currentState.theta) << " "
              << currentState.vx*sin(currentState.theta)+currentState.vy*cos(currentState.theta) << " "
              << currentState.w << "\n"; */
  }
  //iteration++;
}

void WorldModel::getCameraData(State &robotState){
  cameraState = delayedState.front();
  delayedState.pop();
  
  robotState.x = cameraState.x;
  robotState.y = cameraState.y;
  robotState.theta = cameraState.theta;
  robotState.vx = cameraState.vx*cos(cameraState.theta)-cameraState.vy*sin(cameraState.theta);
  robotState.vy = cameraState.vx*sin(cameraState.theta)+cameraState.vy*cos(cameraState.theta);
  robotState.w = cameraState.w;
}

void WorldModel::getCurrentState(State &robotState){
  robotState.x = currentState.x;
  robotState.y = currentState.y;
  robotState.theta = currentState.theta;
  robotState.vx = currentState.vx*cos(currentState.theta)-currentState.vy*sin(currentState.theta);
  robotState.vy = currentState.vx*sin(currentState.theta)+currentState.vy*cos(currentState.theta);
  robotState.w = currentState.w;
}

void WorldModel::setSaturation(double maxLVel, double maxRVel, double maxLAcc, double maxRAcc){
  robotModel.setRobotSaturation(maxLVel,maxLAcc,maxRVel,maxRAcc);
}

void WorldModel::setModelLinear(double xi, double wn){
  robotModel.setModelLinear(xi,wn);
}

void WorldModel::setModelRotation(double xi, double wn){
  robotModel.setModelRotation(xi,wn);
}

void WorldModel::setResolution(int resolutionX, int resolutionY){
  this->resX = resolutionX;
  this->resY = resolutionY;
}

void WorldModel::setEstimatorVarianceL(double variance){
  this->estimatorVarianceL = variance;
}

void WorldModel::setEstimatorVarianceR(double variance){
  this->estimatorVarianceR = variance;
}

void WorldModel::setEstimatorVarianceVL(double variance){
  this->estimatorVarianceVL = variance;
}

void WorldModel::setEstimatorVarianceVR(double variance){
  this->estimatorVarianceVR = variance;
}

double WorldModel::quantify(double value, double unitLength){
  return (floor(value/unitLength)+0.5)*unitLength;
}