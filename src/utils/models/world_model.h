#ifndef WORLD_MODEL_H
#define WORLD_MODEL_H

#include "robot_model.h"
#include "random_factory.h"
#include "types.h"
#include "queue"
#include "fstream"

class WorldModel{
public:
  WorldModel();

  void step(double nextCommand[3]);

  void getCameraData(State &robotState);
  void getCurrentState(State &robotState);

  void setState(double x[3], double y[3], double theta[3]);
  void getState(double x[3], double y[3], double theta[3]);

/*   void setModel(double xi, double wn);
  void setRobotSaturation(double maxLVel, double maxLAcc,
                          double maxAVel, double maxAAcc); */

  //Linear
  void setEstimatorVarianceL(double variance);
  //Rotational
  void setEstimatorVarianceR(double variance);
  //Linear
  void setEstimatorVarianceVL(double variance);
  //Rotational
  void setEstimatorVarianceVR(double variance);
  
  void setResolution(int resolutionX, int resolutionY);
  
  void startWorld(Pose initialRobotPose, double delayTime, double controlPeriod, double stepPeriod);
  
  void setSaturation(double maxLVel, double maxRVel, double maxLAcc, double maxRAcc);
  void setModelLinear(double xi, double wn);
  void setModelRotation(double xi, double wn);

private:
  RobotModel robotModel;
  State cameraState, currentState;
  std::queue<State> delayedState;
  double delayTime, controlPeriod, mismatchTime, stepPeriod;
  double currentCommand[3];

  double quantify(double value, double unitResolution);
  
  RandomFactory randomFactory;
  double estimatorVarianceL, estimatorVarianceR;
  double estimatorVarianceVL, estimatorVarianceVR;

  double resX,resY;

  double maxVelocity, maxAcceleration;

  double rd;

  std::ofstream fileWorld;
  int iteration;
};

#endif