#ifndef DISTRIBUTION_MODEL_H
#define DISTRIBUTION_MODEL_H

#include <vector>
#include <fstream>
#include "sampling_model.h"
#include "helper.h"
#include "types.h"
#include "representativeGrid.h"

class DistributionModel{
public:
  DistributionModel();
  ~DistributionModel();

  const std::vector<double> &getDistribution();

  void estimateDistribution(SamplingModel &samplingModel, 
                            RepresentativeGrid&  representativeGrid, 
                            const unsigned nIterations);
  void saveDistribution(std::ofstream &fileHandler);
  void loadDistribution(std::ifstream &fileHandler);
private:
  std::vector<double> distribution;
  std::vector<std::vector<double>> representativeSamples;
};


#endif