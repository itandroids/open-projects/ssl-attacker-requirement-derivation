add_library(utils.models "")
target_sources(utils.models
  PRIVATE
    sampling_model.cpp
    discrete_model.cpp
    robot_model.cpp
    task_model.cpp
    distribution_model.cpp
    world_model.cpp
    task_model2D.cpp)

target_include_directories(utils.models PUBLIC ${CMAKE_CURRENT_LIST_DIR})
target_link_libraries(utils.models
  PUBLIC 
    utils.random 
    utils.types 
    utils.constants 
    utils.rk4
    utils.helper
    utils.types
    representativeGrid)