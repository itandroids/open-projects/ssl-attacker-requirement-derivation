#include "discrete_model.h"
#include "math.h"

DiscreteModel::DiscreteModel(){}

DiscreteModel::~DiscreteModel(){}

void DiscreteModel::setModel(double ksi, double wn, double T, double maxVelocity, double maxAcceleration){
  this->T = T;
  this->maxVelocity = maxVelocity;
  this->maxAcceleration = maxAcceleration;

  double Ts = this->T;
  double a = -ksi*wn;
  double b = wn*sqrt(1-ksi*ksi);

  A[0][0] = 1;
  A[0][1] = -2*a/(a*a+b*b)*(1-exp(a*Ts)*cos(b*Ts)) - 1/b*(a*a-b*b)/(a*a+b*b)*exp(a*Ts)*sin(b*Ts);
  A[0][2] =    1/(a*a+b*b)*(1-exp(a*Ts)*cos(b*Ts)) + 1/b*        a/(a*a+b*b)*exp(a*Ts)*sin(b*Ts);
  A[1][0] = 0;
  A[1][1] = 1 -(1-exp(a*Ts)*cos(b*Ts)) - a/b*exp(a*Ts)*sin(b*Ts);
  A[1][2] = 1/b*exp(a*Ts)*sin(b*Ts);
  A[2][0] = 0;
  A[2][1] = -1/b*(a*a+b*b)*exp(a*Ts)*sin(b*Ts);
  A[2][2] = 1 -(1-exp(a*Ts)*cos(b*Ts)) + a/b*exp(a*Ts)*sin(b*Ts);

  B[0] = Ts - 1/(a*a+b*b)*(exp(a*Ts)*(a*cos(b*Ts)+b*sin(b*Ts))-a)+
            a/b/(a*a+b*b)*(exp(a*Ts)*(a*sin(b*Ts)-b*cos(b*Ts))+b);
  B[1] = 1/b*(exp(a*Ts)*(a*sin(b*Ts)-b*cos(b*Ts))+b);  
  B[2] =      (exp(a*Ts)*(a*cos(b*Ts)+b*sin(b*Ts))-a)+
          a/b*(exp(a*Ts)*(a*sin(b*Ts)-b*cos(b*Ts))+b);

  return;
}

/*void DiscreteModel::update(state& error, double& input){
  error[0] = A[0][0]*error[0]+A[0][1]*error[1]+A[0][2]*error[2]+B[0]*input;
  error[1] = A[1][0]*error[0]+A[1][1]*error[1]+A[1][2]*error[2]+B[1]*input;
  error[1] += (error[1] > maxVelocity)*(-error[1]+maxVelocity)+
             (error[1] < -maxVelocity)*(-error[1]-maxVelocity);
  error[2] = A[2][0]*error[0]+A[2][1]*error[1]+A[2][2]*error[2]+B[2]*input;
  error[2] += (error[2] > maxAcceleration)*(-error[2]+maxAcceleration)+
             (error[2] < -maxAcceleration)*(-error[2]-maxAcceleration);
}*/