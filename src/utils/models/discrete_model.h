#ifndef DISCRETE_MODEL_H
#define DISCRETE_MODEL_H

typedef double state[3];

class DiscreteModel{
public:
  DiscreteModel();
  ~DiscreteModel();
/**
 * @param T: Period of discretization
 * @param nSteps: number of simulation steps in a discretization step
 * **/
  void setModel(double ksi, double wn, double T, double maxVelocity=5, double maxAcceleration=4);
  
/**
 * @def: updates the variable error with new state. 
 * **/
  inline void update(state& error, double& input){
    error[0] = A[0][0]*error[0]+A[0][1]*error[1]+A[0][2]*error[2]+B[0]*input;
    error[1] = A[1][0]*error[0]+A[1][1]*error[1]+A[1][2]*error[2]+B[1]*input;
    error[1] += (error[1] > maxVelocity)*(-error[1]+maxVelocity)+
                (error[1] < -maxVelocity)*(-error[1]-maxVelocity);
    error[2] = A[2][0]*error[0]+A[2][1]*error[1]+A[2][2]*error[2]+B[2]*input;
    error[2] += (error[2] > maxAcceleration)*(-error[2]+maxAcceleration)+
                (error[2] < -maxAcceleration)*(-error[2]-maxAcceleration);
  }

private:
  double T;//Period of each simulation step
  int nSteps;

  double maxVelocity, maxAcceleration;

  //e[k+1] = a1*e[k]+a2*eVel[k]+a3*eAcc[k]+a4*u[k]
  //eVel[k+1] = b1*e[k]+b2*eVel[k]+b3*eAcc[k]+b4*u[k]
  //eAcc[k+1] = c1*e[k]+c2*eVel[k]+c3*eAcc[k]+c4*u[k]
  double A[3][3];
  double B[3];
};

#endif