#include "distribution_model.h"
#include "iostream"

DistributionModel::DistributionModel(){
}

DistributionModel::~DistributionModel(){
}

const std::vector<double> &DistributionModel::getDistribution(){
  return distribution;
}

void DistributionModel::estimateDistribution(SamplingModel &samplingModel, 
                                            RepresentativeGrid&  representativeGrid,
                                            const unsigned nIterations){
  std::vector<double> sample;
  representativeSamples = representativeGrid.getRepresentativeSamples();
  int nSamples = representativeSamples.size();
  distribution = std::vector<double>(nSamples,0);
  for(unsigned i = 0; i < nIterations; i++){
    sample = samplingModel.getNewSample();
    distribution[representativeGrid.getClosestSampleId(sample)]++;
  }
  for(int i = 0; i < nSamples; i++){
    distribution[i] /= nIterations;
  }
  return;
}

void DistributionModel::saveDistribution(std::ofstream & fileHandler){
  int nSamples = distribution.size();
  int nVariables = representativeSamples[0].size();
  fileHandler << nSamples << " " << nVariables << "\n";
  for(int i = 0; i < nSamples; i++){
    for(int j = 0; j < nVariables; j++){
      fileHandler << representativeSamples[i][j] << " ";
    }
    fileHandler << distribution[i] << "\n";
  }

  return;
}

void DistributionModel::loadDistribution(std::ifstream & fileHandler){
  int nSamples, nVariables;
  fileHandler >> nSamples >> nVariables;
  distribution.resize(nSamples);
  representativeSamples = std::vector<std::vector<double>>(nSamples,std::vector<double>(nVariables,0));
  for(int i = 0; i < nSamples; i++){
    for(int j = 0; j < nVariables; j++){
      fileHandler >> representativeSamples[i][j];
    }
    fileHandler >> distribution[i];
  }

  return;
}