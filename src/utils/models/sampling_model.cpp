#include "sampling_model.h"

Interception SamplingModel::getInterception(){
  Vector2 pos(ball.x,ball.y);
  Vector2 ballDirection(ball.vx/GoalSampling::BALL_SPEED,ball.vy/GoalSampling::BALL_SPEED);

  double ballSpeed = GoalSampling::BALL_SPEED;
  
  Vector2 normalBallDirection = ballDirection;
  normalBallDirection.rot90();
  double ballProj1 = pos*ballDirection;
  double ballProj2 = pos*normalBallDirection;
  double distToGoal = -ballProj1 - sqrt(GoalieTrajectory::GOALIE_RADIUS*
                                        GoalieTrajectory::GOALIE_RADIUS-
                                        ballProj2*ballProj2);
  Vector2 interceptionPos = pos+distToGoal*ballDirection;

  Interception interception;
  interception.theta = interceptionPos.angle();
  interception.time = distToGoal / ballSpeed;
  return interception;
}

Ball SamplingModel::getBall(){
  return ball;
}
Pose SamplingModel::getRobotInitialPose(){
  return robotPose;
}

std::vector<double> SamplingModel::getNewSample(){
  ball = factory.getRandomBall();
  if(alignRobot){
    robotPose.x = GoalieTrajectory::GOALIE_RADIUS*cos(atan2(ball.y,ball.x));
    robotPose.y = GoalieTrajectory::GOALIE_RADIUS*sin(atan2(ball.y,ball.x));
    robotPose.theta = atan2(ball.y,ball.x);
  }
  else{
    robotPose = factory.getRandomRobotPose();
  }

  Interception interception = getInterception();
  std::vector<double> sample(3,0);
  sample[0] = interception.theta;
  sample[1] = robotPose.theta;
  sample[2] = interception.time;

  return sample;
}