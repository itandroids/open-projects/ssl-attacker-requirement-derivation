#include "task_model.h"

TaskModel::TaskModel(){
  //Debug
  //filePos.open("debug/simulationTest-pos.txt"/* , std::ofstream::out | std::ofstream::app */);
  //fileVel.open("debug/simulationTest-vel.txt"/* , std::ofstream::out | std::ofstream::app */);
  //fileInput.open("debug/simulationTest-input.txt"/* , std::ofstream::out | std::ofstream::app */);
  //fileError.open("debug/simulationTest-error.txt"/* , std::ofstream::out | std::ofstream::app */);
  //fileNoise.open("debug/simulationTest-noise.txt"/* , std::ofstream::out | std::ofstream::app */);
  //fileFinalState.open("debug/simulationTest-finalState.txt"/* , std::ofstream::out | std::ofstream::app */);
  //filePos << "t x y phi refX refY\n";
  //fileVel << "t v vn wa\n";
  //fileInput << "t v_ref vn_ref wa_ref\n";
  //fileError << "t radialError distError angleError\n";
  //fileNoise << "t xNoise yNoise phiNoise\n";
  //fileFinalState << "t displacement\n";
}

TaskModel::~TaskModel(){
  //filePos.close();
  //fileVel.close();
  //fileInput.close();
  //fileError.close();
  //fileNoise.close();
  //fileFinalState.close();
}

double TaskModel::simulate(Interception &interception, Pose &robotPose, double mismatchRatio){
  //Setup
  int totalSteps = std::ceil(interception.time/baseStepPeriod);
  int delaySteps = delayTime / baseStepPeriod;
  int controlSteps = controlUpdateTime / baseStepPeriod;
  int mismatchSteps = (mismatchRatio*controlUpdateTime) / baseStepPeriod;
  double lastStepPeriod = interception.time - baseStepPeriod*(totalSteps-1);

  double control[3] = {0,0,0};
  double xInfo[3] = {robotPose.x,0,0};
  double yInfo[3] = {robotPose.y,0,0};
  double thetaInfo[3] = {robotPose.theta,0,0};
  double state[3] = {0,0,0};
  double dState[3] = {0,0,0};
  double localSpeed[3] = {0,0,0};

  double xNoise, yNoise, phiNoise;

  Pose currentPose;
  currentPose.x = robotPose.x;
  currentPose.y = robotPose.y;
  currentPose.theta = robotPose.theta;

  robot.setState(xInfo,yInfo,thetaInfo);

  std::queue<Pose> delayedPose;
  Pose ref;
  ref.x = rd*cos(interception.theta);
  ref.y = rd*sin(interception.theta);
  ref.theta = interception.theta;
  for(int i = 0; i < std::ceil((1.0 * delaySteps - mismatchSteps) / controlSteps);i++){
    delayedPose.push(ref);
  }
  robot.setStepDuration(baseStepPeriod);

  //Simulation loop
  for(int i = 0; i < totalSteps; i++){
    //Debug
    /*if(i % 10 == 0){
      robotState(currentPose,state);
      filePos << i*baseStepPeriod << " " << currentPose.x << " " << currentPose.y << " " << currentPose.theta << " " <<  ref.x << " " << ref.y << "\n";
      fileVel << i*baseStepPeriod << " " << xInfo[1] << " " << yInfo[1] << " " << thetaInfo[1] << "\n";
      fileInput << i*baseStepPeriod << " " << control[0] << " " << control[1] << " " << control[2] << "\n";
      fileError << i*baseStepPeriod << " " << state[0]-rd << " " << rd*wrap2pi(state[1]-interception.theta) << " " << state[2] << "\n";
      fileNoise << i*baseStepPeriod << " " << xNoise << " " << yNoise << " " << phiNoise << "\n";
    }*/
    
    if((i+delaySteps)%controlSteps == mismatchSteps) {//Update delay array
      xNoise = randomFactory.getNormalValue(0,estimatorVarianceL);
      yNoise = randomFactory.getNormalValue(0,estimatorVarianceL);
      phiNoise = randomFactory.getNormalValue(0,estimatorVarianceR);
      delayedPose.push(Pose(
        quantify(currentPose.x,1.0/resX)+xNoise,
        quantify(currentPose.y,1.0/resY)+yNoise,
        currentPose.theta+phiNoise));
    }
    if(i%controlSteps == mismatchSteps){//Update control
      robotState(delayedPose.front(), state);
      robotDState(localSpeed, state, dState);
      positionControl(state,dState,interception,control);
      delayedPose.pop();
    }
    if(i+1 == totalSteps){//Last step
      robot.setStepDuration(lastStepPeriod);
    }
    robot.step(control);
    robot.getState(xInfo,yInfo,thetaInfo);
    currentPose.x = xInfo[0];
    currentPose.y = yInfo[0];
    currentPose.theta = thetaInfo[0];
    localSpeed[0] = xInfo[1];
    localSpeed[1] = yInfo[1];
    localSpeed[2] = thetaInfo[1];
  }

  /*fileFinalState << interception.time << " " << sqrt((currentPose.x-robotPose.x)*(currentPose.x-robotPose.x)+
                                                      (currentPose.y-robotPose.y)*(currentPose.y-robotPose.y)) << "\n";*/
  return sqrt((currentPose.x-ref.x)*(currentPose.x-ref.x)+(currentPose.y-ref.y)*(currentPose.y-ref.y));
}

double TaskModel::quantify(double value, double unitLength){
  return (floor(value/unitLength)+0.5)*unitLength;
}

void TaskModel::robotState(Pose robotPose, double state[3]){
  state[0] = sqrt(robotPose.x*robotPose.x+robotPose.y*robotPose.y);
  state[1] = atan2(robotPose.y,robotPose.x);
  state[2] = wrap2pi(robotPose.theta-state[1]);

  return;
}

void TaskModel::robotDState(double localSpeed[3], double const state[3], double dState[3]){
  dState[0] = localSpeed[0]*cos(state[2]) - localSpeed[1]*sin(state[2]);
  dState[1] = (abs(state[0]) > 1e-6) * ((localSpeed[0]*sin(state[2]) + localSpeed[1]*cos(state[2]))/state[0]);
  dState[2] = localSpeed[2] - dState[1];

  return;
}

void TaskModel::positionControl(double state[3], double dState[3],Interception &interception, double control[3]){
  radialControl = -kp[0]*(state[0]-rd)-kd[0]*dState[0];
  distControl = -kp[1]*wrap2pi(state[1]-interception.theta)-kd[1]*dState[1];
  angleControl = -kp[2]*(state[2])-kd[2]*dState[2];

  control[0] = radialControl * cos(state[2]) + state[0] * distControl * sin(state[2]);
  control[1] = -radialControl * sin(state[2]) + state[0] * distControl * cos(state[2]);
  control[2] = angleControl + distControl;

  if(sqrt(control[0]*control[0] + control[1]*control[1])> maxVelocity){
    double ratio = maxVelocity / sqrt(control[0]*control[0] + control[1]*control[1]);
    control[0] *= ratio;
    control[1] *= ratio;
    control[2] *= ratio;
  }
  if(abs(control[2]) > maxVelocity/rd){
    double ratio = maxVelocity / rd / abs(control[2]);
    control[0] *= ratio;
    control[1] *= ratio;
    control[2] *= ratio;
  }

  return;
}

double TaskModel::wrap2pi(double angle){
  while(angle>M_PI)angle -= 2*M_PI;
  while(angle<=-M_PI)angle += 2*M_PI;

  return angle;
}

void TaskModel::setBaseStepPeriod(double baseStepPeriod){
  this->baseStepPeriod = baseStepPeriod;
  robot.setStepDuration(baseStepPeriod);
}
void TaskModel::setControlUpdateTime(double controlUpdateTime){
  this->controlUpdateTime = controlUpdateTime;
}
void TaskModel::setDelayTime(double delayTime){
  this->delayTime = delayTime;
}
void TaskModel::setControlParameters(double kp[3], double kd[3]){
  this->kp[0] = kp[0];
  this->kp[1] = kp[1];
  this->kp[2] = kp[2];
  this->kd[0] = kd[0];
  this->kd[1] = kd[1];
  this->kd[2] = kd[2];
}
void TaskModel::setTrajectoryRadius(double rd){
  this->rd = rd;
  robot.setRobotSaturation(maxVelocity,maxAcceleration,maxVelocity/rd,maxAcceleration/rd);
}
void TaskModel::setMaxVelocity(double maxVelocity){
  this->maxVelocity = maxVelocity;
  robot.setRobotSaturation(maxVelocity,maxAcceleration,maxVelocity/rd,maxAcceleration/rd);
}
void TaskModel::setMaxAcceleration(double maxAcceleration){
  this->maxAcceleration = maxAcceleration;
  robot.setRobotSaturation(maxVelocity,maxAcceleration,maxVelocity/rd,maxAcceleration/rd);
}
void TaskModel::setRobotModel(double xi, double wn){
  this->xi = xi;
  this->wn = wn;
  robot.setModelLinear(xi,wn);
  robot.setModelRotation(xi,wn);
}

void TaskModel::setResolution(int resolutionX, int resolutionY){
  this->resX = resolutionX;
  this->resY = resolutionY;
}

void TaskModel::setEstimatorVarianceL(double variance){
  this->estimatorVarianceL = variance;
}

void TaskModel::setEstimatorVarianceR(double variance){
  this->estimatorVarianceR = variance;
}