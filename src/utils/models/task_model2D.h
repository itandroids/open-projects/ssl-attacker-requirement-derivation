#ifndef TRAJECTORYTEST_TASK_MODEL2D_H
#define TRAJECTORYTEST_TASK_MODEL2D_H

#include "world_model.h"
#include "fstream"

class TaskModel2D{
public:
  TaskModel2D();
  ~TaskModel2D();

  void simulate(std::vector<State> trajectory, Pose initialPose, double &costPos, double &costVel, double &costTheta);

  void setModelLinear(double xi, double wn);
  void setModelRotation(double xi, double wn);
  void setTranslationalControl(double kp, double kd);
  void setRotationalControl(double kp, double kd);
  void setControlPeriod(double controlPeriod);
  void setDelayTime(double delayTime);
  void setStepPeriod(double stepPeriod);
  void setResolution(int resolution);
  void setTranslationalVariance(double variance);
  void setRotationalVariance(double variance);
  void setTranslationalVarianceVel(double variance);
  void setRotationalVarianceVel(double variance);

private:
  void controlLaw(State reference, State currentState, double command[3]);

  void updateCost(State currentState, State reference, double& costP, double& costV, double& costA);

  double kp[3], kd[3];

  double delayTime, controlPeriod, stepPeriod;
  double maxLVel, maxAVel, maxLAcc, maxAAcc;

  WorldModel worldModel;

  double radialControl, distControl, angleControl;
  std::ofstream filePos, fileVel, fileInput, fileError, fileNoise, fileFinalState;

  double wrap2pi(double angle);

};

#endif