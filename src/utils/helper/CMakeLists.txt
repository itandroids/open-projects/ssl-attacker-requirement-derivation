add_library(utils.helper "")

target_sources(utils.helper
  PRIVATE
    helper.cpp)
target_include_directories(utils.helper PUBLIC ${CMAKE_CURRENT_LIST_DIR})