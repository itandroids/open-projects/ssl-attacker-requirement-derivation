#include "helper.h"

int closest(double value, const std::vector<double> &representativeSet){
  int closest = 0;
  int upper = representativeSet.size()-1;
  int lower = 0;
  bool found = false;

  while(!found){//Binary search
    if(value<representativeSet[lower + (upper-lower)/2]){
      upper = lower + (upper-lower) / 2;
    }
    else{
      lower = lower + (upper-lower) / 2;
    }
    if(upper - lower < 2){
      found = true;
    }
  }
  if(fabs(value - representativeSet[lower]) < fabs(value - representativeSet[upper])){
    closest = lower;
  }
  else{
    closest = upper;
  }

  return closest;
}

std::vector<double> linspace(double min, double max, int n){
  std::vector<double> v(n,0);
  for(int i = 0; i < n; i++){
    v[i] = min + i*(max-min)/(n-1);
  }
  return v;
}