#ifndef UTILS_HELPER
#define UTILS_HELPER

#include "vector"
#include "math.h"

int closest(double value, const std::vector<double> &representativeSet);

std::vector<double> linspace(double min, double max, int n);

#endif