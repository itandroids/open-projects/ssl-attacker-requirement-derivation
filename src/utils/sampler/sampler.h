#ifndef UTILS_SAMPLER
#define UTILS_SAMPLER

#include <functional>
#include <vector>

template<typename FunctionDomain>
struct Point{
  Point<FunctionDomain>(){}
  Point<FunctionDomain>(double x, FunctionDomain y):x(x),y(y){}
  double x;
  FunctionDomain y;
};

struct SamplerDist{
public:
  SamplerDist(){
    iB = 0;
    iM = 0;
    iE = 0;
    dist = 0;
  }
  unsigned iB, iM, iE;
  double dist;
};

/**
 * @brief 
 * 
 * @tparam FunctionDomain : domain of y = f(x)
 * @param xMin : minimum value of the grid.
 * @param xMax : maximum value of the grid.
 * @param x0 : initial pivot of grid. Default value: (xMin+xMax)/2.0).
 * @param nPoints : number of points of the grid. Minimum value: 3.
 * @param f : Function used in y = f(x).
 * @param distanceToLine : Function used to compute refinement of the grid.
 * @param newPointFunction : Function used to compute next point to be added in the grid.
 * @return std::vector<Point<FunctionDomain>> 
 */
template<typename FunctionDomain>
std::vector<Point<FunctionDomain>> sampler(double xMin, double xMax, double x0, unsigned nPoints, 
                                            std::function<FunctionDomain(double)> f, 
                                            std::function<double(Point<FunctionDomain>,Point<FunctionDomain>,Point<FunctionDomain>)> distanceToLine,
                                            std::function<double(double,double)> newPointFunction){
  if(nPoints<3)
    nPoints = 3;
  std::vector<Point<FunctionDomain>> pointList(nPoints,Point<FunctionDomain>());
  std::vector<SamplerDist> distanceList(nPoints-2,SamplerDist());
  pointList[0] = Point<FunctionDomain>(xMin,f(xMin));
  pointList[1] = Point<FunctionDomain>(x0, f(x0));
  pointList[2] = Point<FunctionDomain>(xMax, f(xMax));

  distanceList[0].iB = 0;
  distanceList[0].iM = 1;
  distanceList[0].iE = 2;
  distanceList[0].dist = distanceToLine(pointList[0],pointList[1],pointList[2]);

  unsigned iB, iM, iE;
  double xB,xM,xE, xNew;
  unsigned  tailDistId = 1;
  unsigned  maxDistId = 0;
  double dist = 0;
  for(unsigned i = 3; i < nPoints; i++){
    dist = distanceList[0].dist;
    maxDistId = 0;
    for(unsigned j = 1; j < tailDistId; j++){
      if(distanceList[j].dist>dist){
        maxDistId = j;
        dist = distanceList[j].dist;
      }
    }
    iB = distanceList[maxDistId].iB;
    iM = distanceList[maxDistId].iM;
    iE = distanceList[maxDistId].iE;
    xB = pointList[iB].x;
    xM = pointList[iM].x;
    xE = pointList[iE].x;
    if(xM-xB > xE-xM){
      xNew = newPointFunction(xB,xM);
      pointList[i] = Point<FunctionDomain>(xNew,f(xNew));
      distanceList[maxDistId].iM = i;
      distanceList[maxDistId].iE = iM;
      distanceList[maxDistId].dist = distanceToLine(pointList[iB],pointList[i],pointList[iM]);
      distanceList[tailDistId].iB = i;
      distanceList[tailDistId].iM = iM;
      distanceList[tailDistId].iE = iE;
      distanceList[tailDistId].dist = distanceToLine(pointList[i],pointList[iM],pointList[iE]);
      tailDistId++;
      if(xB>xMin){
        unsigned iPrev = 0;
        for(unsigned prevDistId = 0; prevDistId<tailDistId-1; prevDistId++){
          if(distanceList[prevDistId].iM == iB){
            iPrev = distanceList[prevDistId].iB;
            distanceList[prevDistId].iE = i;
            distanceList[prevDistId].dist = distanceToLine(pointList[iPrev],pointList[iB],pointList[i]);
            break;
          }
        }
      }
    }
    else{
      xNew = newPointFunction(xM,xE);
      pointList[i] = Point<FunctionDomain>(xNew,f(xNew));
      distanceList[maxDistId].iB = iM;
      distanceList[maxDistId].iM = i;
      distanceList[maxDistId].dist = distanceToLine(pointList[iM],pointList[i],pointList[iE]);
      distanceList[tailDistId].iB = iB;
      distanceList[tailDistId].iM = iM;
      distanceList[tailDistId].iE = i;
      distanceList[tailDistId].dist = distanceToLine(pointList[iB],pointList[iM],pointList[i]);
      tailDistId++;
      if(xE<xMax){
        unsigned iNext = 0;
        for(unsigned nextDistId = 0; nextDistId<tailDistId-1; nextDistId++){
          if(distanceList[nextDistId].iM == iE){
            iNext = distanceList[nextDistId].iE;
            distanceList[nextDistId].iB = i;
            distanceList[nextDistId].dist = distanceToLine(pointList[i],pointList[iE],pointList[iNext]);
            break;
          }
        }
      }
    }
  }

  return pointList;
}

template<typename FunctionDomain>
std::vector<Point<FunctionDomain>> sampler(double xMin, double xMax, unsigned nPoints, 
                                            std::function<FunctionDomain(double)> f, 
                                            std::function<double(Point<FunctionDomain>,Point<FunctionDomain>,Point<FunctionDomain>)> distanceToLine,
                                            std::function<double(double,double)> newPointFunction){
  return sampler<FunctionDomain>(xMin,xMax,(xMin+xMax)/2.0,nPoints,f,distanceToLine,newPointFunction);
}

template<typename FunctionDomain>
std::vector<Point<FunctionDomain>> sampler(double xMin, double xMax, double x0, unsigned nPoints, 
                                            std::function<FunctionDomain(double)> f, 
                                            std::function<double(Point<FunctionDomain>,Point<FunctionDomain>,Point<FunctionDomain>)> distanceToLine){
  std::function<double(double,double)> newPointFunction = [=](double x1, double x2){return (x1 + x2)/2.0;}; 
  return sampler<FunctionDomain>(xMin,xMax,x0,nPoints,f,distanceToLine,newPointFunction);
}

template<typename FunctionDomain>
std::vector<Point<FunctionDomain>> sampler(double xMin, double xMax, unsigned nPoints, 
                                            std::function<FunctionDomain(double)> f, 
                                            std::function<double(Point<FunctionDomain>,Point<FunctionDomain>,Point<FunctionDomain>)> distanceToLine){
  std::function<double(double,double)> newPointFunction = [=](double x1, double x2){return (x1 + x2)/2.0;}; 
  return sampler<FunctionDomain>(xMin,xMax,(xMin+xMax)/2.0,nPoints,f,distanceToLine,newPointFunction);
}

template<typename FunctionDomain>
std::vector<Point<FunctionDomain>> sampler(double xMin, double xMax, unsigned nPoints,\
                                            std::function<FunctionDomain(double)> f){
  std::function<double(Point<FunctionDomain>,Point<FunctionDomain>,Point<FunctionDomain>)> distanceToLine = \
    [](Point<FunctionDomain>p1,Point<FunctionDomain>,Point<FunctionDomain>p3){return abs(p3.x-p1.x);};
  std::function<double(double,double)> newPointFunction = [=](double x1, double){return x1 + (xMax-xMin)/(nPoints-1);}; 
  return sampler<FunctionDomain>(xMin,xMax,xMin+(xMax-xMin)/(nPoints-1),nPoints,f,distanceToLine,newPointFunction);
}

#endif