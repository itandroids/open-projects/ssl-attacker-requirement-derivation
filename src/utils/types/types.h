#ifndef TYPES_H
#define TYPES_H

#include "vector2.h"
#include <vector>

struct Interception{
  double time;
  double theta;
};

struct Ball{
  double x;
  double y;
  double vx;
  double vy;
};

class Pose{
public:
  Pose(double x, double y, double theta):x(x),y(y),theta(theta){}
  Pose():Pose(0,0,0){}

  double x,y,theta;
};

class State{
public:
  State(double x, double y, double theta, double vx, double vy, double w):x(x),y(y),theta(theta),vx(vx),vy(vy),w(w){}
  State():State(0,0,0,0,0,0){}
  
  double x,y,theta,vx,vy,w;
};

class Array2D{
public:
  Array2D():Array2D(0,0){}
  Array2D(unsigned sz1, unsigned sz2):sz1(sz1),sz2(sz2){data.resize(sz1*sz2);}
  const double& at(int id1, int id2) const {return data.at(id2+id1*sz2);} 
  double& at(int id1, int id2) {return data.at(id2+id1*sz2);} 
  const double& at(int id) const {return data.at(id);} 
  double& at(int id){return data.at(id);}
  void resize(unsigned sz1, unsigned sz2){
    data.resize(sz1*sz2);
    this->sz1 = sz1;
    this->sz2 = sz2;
  }
private:
  std::vector<double> data;
  unsigned sz1,sz2;
};

class Array3D{
public:
  Array3D():Array3D(0,0,0){}
  Array3D(unsigned sz1, unsigned sz2, unsigned sz3):sz1(sz1),sz2(sz2),sz3(sz3){data.resize(sz1*sz2*sz3);}
  const double& at(int id1, int id2, int id3) const {return data.at(id3+id2*sz3+id1*sz3*sz2);} 
  double& at(int id1, int id2, int id3) {return data.at(id3+id2*sz3+id1*sz3*sz2);} 
  const double& at(int id) const {return data.at(id);} 
  double& at(int id){return data.at(id);}
  void resize(unsigned sz1, unsigned sz2, unsigned sz3){
    data.resize(sz1*sz2*sz3);
    this->sz1 = sz1;
    this->sz2 = sz2;
    this->sz3 = sz3;
  }
private:
  std::vector<double> data;
  unsigned sz1,sz2,sz3;
};

class MultiArray{
public:
  MultiArray(std::vector<unsigned> sz):sz(sz),nDim(sz.size()){
    prodSz[nDim-1] = 1;
    for(int i = nDim-2; i >= 0; i--){
      prodSz[i] = prodSz[i+1]*sz[i+1];
    }
    data.resize(prodSz[0]*sz[0]);
  }
  const double& at(std::vector<int> ids) const {
    int id = 0;
    for(int i = 0; i < nDim; i++){
      id += ids[i]*prodSz[i];
    }
    return data.at(id);
  }
  double& at(std::vector<int> ids) {
    int id = 0;
    for(int i = 0; i < nDim; i++){
      id += ids[i]*prodSz[i];
    }
    return data.at(id);
  }
  const double& at(int id) const {return data.at(id);} 
  double& at(int id){return data.at(id);}

private:
  int nDim;
  std::vector<double> data;
  std::vector<unsigned> sz;
  std::vector<unsigned> prodSz;
};

#endif