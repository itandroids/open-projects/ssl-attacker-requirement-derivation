#include "vector2.h"
#include <math.h>

Vector2::Vector2(double x, double y):x(x),y(y){}

Vector2 operator + (Vector2 lhs, Vector2 const &rhs){
    lhs += rhs;
    return lhs;
}
Vector2 operator - (Vector2 lhs, Vector2 const &rhs){
  lhs -= rhs;
  return lhs;
}
double operator * (Vector2 const &lhs, Vector2 const &rhs){
  return lhs.getX()*rhs.getX()+lhs.getY()*rhs.getY();
}
Vector2 operator * (Vector2 lhs, double const &rhs){
  lhs *= rhs;
  return lhs;
}
Vector2 operator * (double const &lhs, Vector2 rhs){
  rhs *= lhs;
  return rhs;
}
Vector2 operator * (Vector2 lhs, int const &rhs){
  lhs *= rhs;
  return lhs;
}
Vector2 operator * (int const &lhs, Vector2 rhs){
  rhs *= lhs;
  return rhs;
}
Vector2 operator / (Vector2 lhs, double const &rhs){
  lhs /= rhs;
  return lhs;
}
Vector2 operator / (Vector2 lhs, int const &rhs){
  lhs /= rhs;
  return lhs;
}

void Vector2::rot90(){
  double x = this->x;
  this->x = - this->y;
  this->y = x;
}
void Vector2::rot180(){
  x *= -1;
  y *= -1;
}
void Vector2::rot270(){
  double x = this->x;
  this->x = this->y;
  this->y = -x;
}
void Vector2::rotate(double const &angle){
  double x = this->x;
  double y = this->y;
  this->x = x*cos(angle)-y*sin(angle);
  this->y = x*sin(angle)+y*cos(angle);
}
void Vector2::normalize(){
  double norm = sqrt(x*x+y*y);
  x /= norm;
  y /= norm;
}
double Vector2::abs(){
  return sqrt(x*x+y*y);
}
double Vector2::angle(){
  return atan2(y,x);
}

