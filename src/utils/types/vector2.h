#ifndef VECTOR2_H
#define VECTOR2_H

class Vector2{
private:
  double x;
  double y;
public:
  Vector2(double x, double y);
  
  inline double getX() const {return x;}
  inline double getY() const {return y;}
  Vector2& operator += (Vector2 const &other){
    x += other.x;
    y += other.y;
    return *this;
  }
  Vector2& operator -= (Vector2 const &other){
    x -= other.x;
    y -= other.y;
    return *this;
  }
  Vector2& operator *= (double const &other){
    x *= other;
    y *= other;
    return *this;
  }
  Vector2& operator *= (int const &other){
    x *= other;
    y *= other;
    return *this;
  }
  Vector2& operator /= (double const &other){
    x /= other;
    y /= other;
    return *this;
  }
  Vector2& operator /= (int const &other){
    x /= other;
    y /= other;
    return *this;
  }
  void rot90();
  void rot180();
  void rot270();
  void rotate(double const &angle);
  void normalize();
  double abs();
  double angle();
};

Vector2 operator + (Vector2 lhs, Vector2 const &rhs);
Vector2 operator - (Vector2 lhs, Vector2 const &rhs);
double operator * (Vector2 const &lhs, Vector2 const &rhs);
Vector2 operator * (Vector2 lhs, double const &rhs);
Vector2 operator * (double const &lhs, Vector2 rhs);
Vector2 operator * (Vector2 lhs, int const &rhs);
Vector2 operator * (int const &lhs, Vector2 rhs);
Vector2 operator / (Vector2 lhs, double const &rhs);
Vector2 operator / (Vector2 lhs, int const &rhs);


#endif
