#include "theta_tuner.h"

ThetaTuner::ThetaTuner():samplingModel(true){
  wn = 2*M_PI*15;
  xi = 0.2;
  maxVelocity = 5;
  maxAcceleration = 4;
  kpMin = 1, kpMax = 20;
  kdMin = 0, kdMax = 10;
  nElements = 100;
  res = std::vector<std::vector<double> >(nElements,std::vector<double>(nElements,0));
}
ThetaTuner::~ThetaTuner(){

}

void ThetaTuner::run(){
  int nElements = 20;
  std::vector<double> errorVector(nElements,0);
  double minError = -0.4;
  double maxError = 0.4;
  for(int i = 0; i < nElements; i++){
    errorVector[i] = minError + i*(maxError-minError)/(nElements-1.0);
  }
  std::vector<double> timeVector(nElements,0);
  double minTime = 0.1;
  double maxTime = 0.3;
  for(int i = 0; i < nElements; i++){
    timeVector[i] = minTime + i*(maxTime-minTime)/(nElements-1.0);
  }

  std::vector<std::vector<std::vector<double>>> jointProb(
    nElements,std::vector<std::vector<double>>(nElements,std::vector<double>(nElements,0)));
  std::ifstream inFile;
  inFile.open("errorDistribution.txt");
  std::string auxstring;
  inFile >> auxstring >> auxstring >> auxstring;
  /* samplingModel.loadDistribution(inFile, jointProb); */


  file.open("thetaTuning.txt");
  file << "kp ratio meanError meanVelocity\n";

  std::array<double,20> kp;
  for(int i = 0; i < 20 ; i++){
    kp[i] = i*60.0/19;
  }
  std::array<double,5> alpha;
  for(int i = 0; i < 5 ; i++){
    alpha[i] = 0.1/4*i;
  }

  Interception interception;
  Pose robot;

  int nSamples = 1000;

  std::function<int(double,double)> f = [&](double kp, double alpha){
    /* double mean=0;
    for(int i = 0; i < nSamples; i++){
      samplingModel.getNewIC(true);
      interception = samplingModel.getInterception();
      robot = samplingModel.getRobotInitialPose();
      ic.eTheta0 = interception.theta - atan2(robot.y,robot.x);
      ic.tFinal = interception.time;
      mean += fabs(simulate(kp,kp*alpha));
    }
    mean /= nSamples; */
    /* double mean=0, meanVel=0;
    for(int i = 0; i < nElements; i++){
      for(int j = 0; j < nElements; j++){
        ic.eTheta0 = errorVector[i];
        ic.tFinal = timeVector[j];
        mean += (fabs(simulate1D(kp,kp*alpha)))*jointProb[i][j];
        meanVel += abs(vel)*jointProb[i][j];
      }
    }
    file << kp << " " << alpha << " " << mean << " " << meanVel << "\n"; */
    return 0;
  };
  gridFunction(f,kp,alpha);

  file.close();
}

void ThetaTuner::run2D(){
  int nElements = 20;
  std::vector<double> errorVector(nElements,0);
  double minError = -0.4;
  double maxError = 0.4;
  for(int i = 0; i < nElements; i++){
    errorVector[i] = minError + i*(maxError-minError)/(nElements-1.0);
  }
  std::vector<double> timeVector(nElements,0);
  double minTime = 0.1;
  double maxTime = 0.3;
  for(int i = 0; i < nElements; i++){
    timeVector[i] = minTime + i*(maxTime-minTime)/(nElements-1.0);
  }
  std::vector<double> phiVector(3,0);
  double minPhi = 0;
  double maxPhi = 30/180.0*M_PI;
  for(int i = 0; i < 3; i++){
    phiVector[i] = minPhi + i*(maxPhi-minPhi)/(3-1.0);
  }

  std::vector<std::vector<std::vector<double>>> jointProb(
    nElements,std::vector<std::vector<double>>(nElements,std::vector<double>(nElements,0)));
  std::ifstream inFile;
  inFile.open("errorDistribution.txt");
  std::string auxstring;
  inFile >> auxstring >> auxstring >> auxstring;
  /* samplingModel.loadDistribution(inFile, jointProb); */


  file.open("thetaTuning2D.txt");
  file << "kp ratio meanError meanPhi meanErrorVel meanPhiVel\n";

  std::array<double,20> kp;
  for(int i = 0; i < 20 ; i++){
    kp[i] = i*60.0/19;
  }
  std::array<double,5> alpha;
  for(int i = 0; i < 5 ; i++){
    alpha[i] = 0.1/4*i;
  }

  Interception interception;
  Pose robot;

  int nSamples = 1000;

  std::function<int(double,double)> f = [&](double kp, double alpha){
    /* double meanError=0, meanPhi=0, meanErrorVel=0, meanPhiVel=0;
    double P[2] = {12,kp};
    double D[2] = {0.4,kp*alpha};
    for(int i = 0; i < nElements; i++){
      for(int j = 0; j < nElements; j++){
        for(int k = 0; k < 3; k++){
          ic.eTheta0 = errorVector[i];
          ic.tFinal = timeVector[j];
          ic.phiError = phiVector[k];
          meanError += (fabs(simulate2D(P,D)))*jointedProb[i][j]*(1.0)/3;
          meanPhi += fabs(phi)*jointedProb[i][j]*(1.0)/3;
          meanErrorVel += fabs(vel)*jointedProb[i][j]*(1.0)/3;
          meanPhiVel += fabs(phiVel)*jointedProb[i][j]*(1.0)/3;
        }
      }
    }
    file << kp << " " << alpha << " " << meanError << " " << meanPhi << 
                                  " " << meanErrorVel << " " << meanPhiVel << "\n"; */
    return 0;
  };
  gridFunction(f,kp,alpha);

  file.close();
}

void ThetaTuner::runFixedIC(){
  file.open("thetaTuning-FixedIC.txt");
  file << "kp alpha eFinal vFinal\n";

  std::array<double,100> kp;
  for(int i = 0; i < 100 ; i++){
    kp[i] = i*20.0/99;
  }
  std::array<double,1> alpha;
  for(int i = 0; i < 1 ; i++){
    alpha[i] = 0.2/99*i;
  }

  double error0 = 0.2;
  double time = 0.2;


  std::function<double(double,double)> f = 
  [&](double kp, double alpha){
    ic.eTheta0 = error0;
    ic.tFinal = time;
    file << kp << " " << alpha << " ";
    double eFinal = simulate1D(kp,kp*alpha);
    file << eFinal << " " << vel <<  "\n";
    return 0;
  };
  gridFunction(f,kp,alpha);

  file.close();
}

int ThetaTuner::tuning(InitialConditions ic){
  this->ic.eTheta0 = ic.eTheta0;
  this->ic.tFinal = ic.tFinal;

  double currentError, minError = 1e3;
  std::vector<double> currentGain(2,0);
  /* nlopt::opt opt(nlopt::algorithm::GN_AGS, 2);
  std::vector<double> lb(2,0);
  opt.set_lower_bounds(lb);
  std::vector<double> ub(2,30);
  opt.set_upper_bounds(ub);
  opt.set_min_objective(wrap,this);
  opt.set_xtol_rel(1e-2);
  count = 0;
  opt.optimize(bestGain,minError);
  std::cout << "Solved in: " << count << " iterations\n"; */
  

  //std::cout << "kp = " << pd[0] << " kd = " << pd[1] << "\n";
  file << currentGain[0] << " " << currentGain[1] << "\n";

  return 0;
}

double ThetaTuner::wrap(const std::vector<double>&x,std::vector<double>&grad, void* data){
  return fabs(reinterpret_cast<ThetaTuner*>(data)->simulate1D(x[0],x[1]));
}

double ThetaTuner::simulate1D(double kp, double kd){
  count++;
  RungeKutta4 rk4(3);
  rk4.f = [&](double& t, double*& x, double*& dxdt){
    dxdt[0] = x[1];
    dxdt[1] = x[2];
    dxdt[2] = -2*xi*wn*x[2]-wn*wn*(1+kd)*x[1]-wn*wn*kp*x[0];
    if(abs(x[1]) > maxVelocity){
      if(x[1]*dxdt[1]>0)
        dxdt[1] = 0;
    }
    if(abs(x[2]) > maxAcceleration){
      if(x[2]*dxdt[2]>0)
        dxdt[2] = 0;
    }
    return;
  };
  double x[3] = {ic.eTheta0,0,0};
  auto xFinal = rk4.solve(x,ic.tFinal,ic.tFinal/10000.0);
  vel = xFinal[1];
  return xFinal[0];
}

double ThetaTuner::simulate2D(double kp[2], double kd[2]){
  count++;
  RungeKutta4 rk4(6);
  rk4.f = [&](double& t, double*& x, double*& dxdt){
    dxdt[0] = x[2];
    dxdt[1] = x[3];
    dxdt[2] = x[4];
    dxdt[3] = x[5];
    dxdt[4] = -2*xi*wn*x[4]-(wn*wn*(1+kd[0])-x[3]*x[3])*x[2]-wn*wn*kp[0]*x[0];
    dxdt[5] = -2*xi*wn*x[5]-(wn*wn*(1+kd[1])+x[2]*x[3])*x[3]-wn*wn*kp[1]*x[1];
    if(abs(x[2]) > maxVelocity){
      if(x[2]*dxdt[2]>0)
        dxdt[2] = 0;
    }
    if(abs(x[3]) > maxVelocity){
      if(x[3]*dxdt[3]>0)
        dxdt[3] = 0;
    }
    if(abs(x[4]) > maxAcceleration){
      if(x[4]*dxdt[4]>0)
        dxdt[4] = 0;
    }
    if(abs(x[5]) > maxAcceleration){
      if(x[5]*dxdt[5]>0)
        dxdt[5] = 0;
    }
    return;
  };
  double x[6] = {ic.eTheta0,ic.phiError,0,0,0,0};
  auto xFinal = rk4.solve(x,ic.tFinal,ic.tFinal/10000.0);
  phi = xFinal[1];
  vel = xFinal[2];
  phiVel = xFinal[3];
  return xFinal[0];
}