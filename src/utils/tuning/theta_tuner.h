#ifndef THETATUNER_H
#define THETATUNER_H

#include "nlopt.hpp"
#include "functional"
#include "vector"
#include "array"
#include "rk_4.h"
#include "gridfunction.h"
#include "fstream"
#include "iostream"
#include "sampling_model.h"

class ThetaTuner{

struct InitialConditions{
  double eTheta0, tFinal, phiError;
};

public:
  ThetaTuner();
  ~ThetaTuner();
  void run();
  void run2D();
  void runFixedIC();
private:
  int tuning(InitialConditions ic);
  double simulate1D(double kp, double kd);
  double simulate2D(double kp[2], double kd[2]);


  static double wrap(const std::vector<double>&x,std::vector<double>&grad, void* data);
  std::ofstream file;
  double xi, wn, maxVelocity, maxAcceleration;
  InitialConditions ic;

  double kpMin, kpMax, kdMin, kdMax;
  int nElements;
  std::vector<std::vector<double>> res;

  int count;
  double vel, phi, phiVel;

  SamplingModel samplingModel;
};

#endif