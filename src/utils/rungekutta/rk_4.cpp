#include "rk_4.h"

RungeKutta4::RungeKutta4(int dim):dim(dim){
  K1 = new double[dim];
  K2 = new double[dim];
  K3 = new double[dim];
  K4 = new double[dim];
  x = new double[dim];
  xAux = new double[dim];
  upperX = new double[dim];
  lowerX = new double[dim];
  for(i = 0; i < dim; i++){
    upperX[i] = 1e10;
    lowerX[i] = -1e10;
  }
}

RungeKutta4::~RungeKutta4(){
  delete[] K1;
  delete[] K2;
  delete[] K3;
  delete[] K4;
  delete[] x;
  delete[] upperX;
  delete[] lowerX;
}

void RungeKutta4::setUpper(double const * const upper){
  for(i = 0; i < dim; i++){
    upperX[i] = upper[i];
  }
}

void RungeKutta4::setLower(double const * const lower){
  for(i = 0; i < dim; i++){
    lowerX[i] = lower[i];
  }
}

double const * RungeKutta4::solve(double const * const &x0,
                                        double const &tf,
                                        double const &stepSize){
  nSteps = tf / stepSize;
  for(i = 0; i < dim; i++)
    x[i] = x0[i];
  double t = 0;
  for(j = 0; j < nSteps; j++){
    f(t,x,K1);
    
    t += stepSize/2.0;
    for(i=0;i<dim;i++)
      xAux[i]=x[i]+K1[i]*stepSize/2.0;
    f(t,xAux,K2);

    for(i=0;i<dim;i++)
      xAux[i]=x[i]+K2[i]*stepSize/2.0;
    f(t,xAux,K3);

    t += stepSize/2.0;
    for(i=0;i<dim;i++)
      xAux[i]=x[i]+K3[i]*stepSize;
    f(t,xAux,K4);

    for(i=0;i<dim;i++)
      x[i]+=(K1[i]+2*K2[i]+2*K3[i]+K4[i])*stepSize/6.0;

    /* for(i=0;i<dim;i++){
      x[i] += (x[i]>upperX[i])*(upperX[i]-x[i])+(x[i]<lowerX[i])*(lowerX[i]-x[i]);
    } */
  }
  double const * p = x;
  return p;
}