#ifndef RK_4_H
#define RK_4_H

#include <functional>

class RungeKutta4{
public:
  RungeKutta4(int dim);
  ~RungeKutta4();
  std::function<void(double&,double*&,double*&)> f;
  double const * solve(double const * const &x0,
                             double const &tf,
                             double const &stepSize);
  void setUpper(double const * const upper);
  void setLower(double const * const lower);
private:
  double *K1,*K2,*K3,*K4, *x, *xAux, *upperX, *lowerX;
  int dim;
  int i,j, nSteps;
};

#endif
