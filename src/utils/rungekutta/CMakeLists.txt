add_library(utils.rk4 "")
target_sources(utils.rk4 
  PRIVATE
    rk_4.cpp)
target_include_directories(utils.rk4 PUBLIC ${CMAKE_CURRENT_LIST_DIR})