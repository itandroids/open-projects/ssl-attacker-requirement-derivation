#ifndef GRIDFUNCTION_H
#define GRIDFUNCTION_H

#include <functional>

template<typename T, unsigned ... Rest> struct Grid;

template<typename T, unsigned First>
struct Grid<T, First>{
  typedef T type[First];
  type data;
  T& operator[](unsigned i){return data[i];}
};

template<typename T, unsigned First, unsigned ... Rest>
struct Grid<T, First, Rest...>{
  typedef typename Grid<T,Rest...>::type OneDimensionDown;
  typedef OneDimensionDown type[First];
  type data;
  OneDimensionDown& operator[](unsigned i){return data[i];}
}; 

template<typename T, typename G, typename First>
void recursiveFunction(
  std::function<T(typename First::value_type)>f, 
  G& grid, First first) {
  typename First::iterator begin = first.begin();
  typename First::iterator current = first.begin();
  typename First::iterator end = first.end();
  while(current != end){
    grid[std::distance(begin,current)] = f(*current);
    current++;
  }
}

template<typename T, typename G, typename First, typename... Rest>
void recursiveFunction(
  std::function<T(typename First::value_type, typename Rest::value_type ...)>f, 
  G& grid, First first, Rest... rest) {
  typename First::iterator begin = first.begin();
  typename First::iterator current = first.begin();
  typename First::iterator end = first.end();
  while(current != end){
    recursiveFunction(
      std::function<T(typename Rest::value_type ...)>
        ([=](typename Rest::value_type ... r){return f(*current,r...);}),
      grid[std::distance(begin,current)],
      rest...);
    current++;
  }
}

template<typename T, typename... Args>
Grid<T,std::tuple_size<Args>::value ...> gridFunction(
  std::function<T(typename Args::value_type ...)> f, Args... args)  {
  Grid <T,std::tuple_size<Args>::value ...> grid;
  recursiveFunction(f,grid,args...);
  return grid;
}

#endif