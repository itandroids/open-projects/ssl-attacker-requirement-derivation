trajList = [];
load('trajectoriesVel.mat');
trajList = [trajList,fTrajList];
load('trajectoriesAcc.mat');
trajList = [trajList,fTrajList];
load('trajectoriesRotVel.mat');
trajList = [trajList,fTrajList];
load('trajectoriesRotAcc.mat');
trajList = [trajList,fTrajList];

l = length(trajList);

for i=1:l
    traj = trajList(i);

    h = fopen(['trajectories/traj',num2str(i),'.txt'],'w');

    fprintf(h,'t x y theta dxdt dydt dthetadt\n');
    n = length(traj.t);
    for j=1:n
        fprintf(h,'%f %f %f %f %f %f %f\n',traj.t(j),traj.x(j),traj.y(j),traj.theta(j),traj.vx(j),traj.vy(j),traj.dthetadt(j));
    end
    fclose(h);
end