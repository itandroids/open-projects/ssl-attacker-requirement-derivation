function dAngleDt = estimateAngleDerivative(angle, t, windowWidth)
    windowPadding = floor(windowWidth/2);
    dT = t(windowWidth:end) - t(1:(end-windowWidth+1));
    dT = [windowWidth/30.*ones(windowPadding,1); dT; windowWidth/30.*ones(windowPadding,1)];
    dAngle = [angle((windowPadding+1):end);angle(end).*ones(windowPadding,1)] - ...
             [angle(1)*ones(windowPadding,1);angle(1:(end-windowPadding))];

    dAngle(dAngle>pi) = -2*pi + dAngle(dAngle>pi);
    dAngle(dAngle<-pi) = 2*pi + dAngle(dAngle<-pi);
    
%     dAngleAux = dAngle;
%     
%     dAngleAux(dAngle>pi/2) = -pi + dAngle(dAngle>pi/2);
%     dAngleAux(dAngle<-pi/2) = pi + dAngle(dAngle<-pi/2);
%         
%     l = length(dAngle);
%     
%     for i = 2:l
%         value = dAngle(i);
%         if abs(dAngleAux(i)-dAngle(i-1)) < abs(dAngle(i)-dAngle(i-1))
%             value = dAngleAux(i);
%         end
%         dAngle(i) = value;
%     end
    
    dAngleDt = dAngle ./ dT;
    dAngleDt(1:windowPadding) = zeros(windowPadding,1);
    
    %Filter derivative
    extW = [dAngleDt(1).*ones(windowPadding,1);dAngleDt;dAngleDt(end).*ones(windowPadding,1)];
    dAngleDt = weightedSum(extW((1:windowWidth) + (1:length(dAngleDt))' - 1));
end

function V = weightedSum(A)
    ASize = size(A);
    nRows = ASize(1);
    nColumns = ASize(2);
    midColumn = floor((nColumns-1)/2)+1;
    auxA = A;
    for i = 1:nColumns
        auxA(:,i) = A(:,i) .* max(0,(1-abs(midColumn-i)./midColumn)./midColumn);
    end

    V = sum(auxA,2);
end