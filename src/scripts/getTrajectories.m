function trajList = getTrajectories()
trajList = [];
files = dir('logFiles/kf-log*.csv');
nFiles = length(files);
maxV = 0;
for i = 1:nFiles
    filename = files(i).name;
    folder = files(i).folder;
    mTable = readtable(fullfile(folder,filename));
    traj.t = mTable.t;
    traj.x = mTable.x;
%     traj.vx = mTable.vx;
%     traj.ax = mTable.ax;
    traj.vx = estimateDerivative(traj.x,traj.t,11);
    traj.ax = estimateDerivative(traj.vx,traj.t,11);
    traj.y = mTable.y;
%     traj.vy = mTable.vy;
%     traj.ay = mTable.ay;
    traj.vy = estimateDerivative(traj.y,traj.t,11);
    traj.ay = estimateDerivative(traj.vy,traj.t,11);
    traj.v = sqrt(traj.vx.^2+traj.vy.^2);
    traj.a = sqrt(traj.ax.^2+traj.ay.^2);
    traj.theta = mTable.rawtheta;
    traj.dthetadt = estimateAngleDerivative(traj.theta,traj.t,11);
    traj.ddthetadt2 = estimateDerivative(traj.dthetadt,traj.t,11);
%     if max(abs(traj.v))>maxV
%         maxTraj = traj;
%         maxV = max(abs(traj.v));
%         maxId = i;
%     end
    trajList = [trajList;splitTrajectory(traj)];
end

save('kftrajectories.mat','trajList');
end