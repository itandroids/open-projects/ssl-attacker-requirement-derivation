%%trajList = getTrajectories;
%%save('trajectories.mat','trajList');
load('trajectories.mat','trajList');

%% Get top 5% maximum velocity trajectories
l = length(trajList);
maxValue = zeros(l,1);
for i = 1:l
    maxValue(i) = max(trajList(i).v);
end
sortedList = sort(maxValue);
valueRef = sortedList(round(l*0.95));

indexes = find(maxValue > valueRef);

clear fTrajList
for i = 1:length(indexes)
    fTrajList(i) = trajList(indexes(i));
end

save('trajectoriesVel.mat','fTrajList');
% trajList = trajList(setdiff(1:end,indexes));
%% Get top 5% maximum acceleration trajectories
l = length(trajList);
maxValue = zeros(l,1);
for i = 1:l
    maxValue(i) = max(abs(trajList(i).linA));
end
sortedList = sort(maxValue);
valueRef = sortedList(round(l*0.95));

indexes = find(maxValue > valueRef);

clear fTrajList
for i = 1:length(indexes)
    fTrajList(i) = trajList(indexes(i));
end

save('trajectoriesAcc.mat','fTrajList');
% trajList = trajList(setdiff(1:end,indexes));
%% Get top 5% maximum rotation around axis trajectories
l = length(trajList);
maxValue = zeros(l,1);
for i = 1:l
    maxValue(i) = max(abs(trajList(i).dthetadt));
end
sortedList = sort(maxValue);
valueRef = sortedList(round(l*0.95));

indexes = find(maxValue > valueRef);

clear fTrajList
for i = 1:length(indexes)
    fTrajList(i) = trajList(indexes(i));
end

save('trajectoriesRotVel.mat','fTrajList');
% trajList = trajList(setdiff(1:end,indexes));
%% Get top 5% maximum rotation acceleration around axis trajectories
l = length(trajList);
maxValue = zeros(l,1);
for i = 1:l
    maxValue(i) = max(abs(trajList(i).ddthetadt2));
end
sortedList = sort(maxValue);
valueRef = sortedList(round(l*0.95));

indexes = find(maxValue > valueRef);

clear fTrajList
for i = 1:length(indexes)
    fTrajList(i) = trajList(indexes(i));
end

save('trajectoriesRotAcc.mat','fTrajList');
% trajList = trajList(setdiff(1:end,indexes));
% % % %%
%%
% % hold on;
% % subplot(2,1,1)
% % i = 1;
% % minTraj = 1;
maxTraj = 1;
attrib = cell(length(fTrajList),1);
fig = figure;
hold on
for i = 1:length(fTrajList)
    attrib{i}.Data = abs(fTrajList(i).dthetadt);
    attrib{i}.Time = fTrajList(i).t;
    plot(attrib{i}.Time, attrib{i}.Data);
    if max(attrib{i}.Data) > max(attrib{maxTraj}.Data)
        maxTraj = i;
    end
end
set(fig,'renderer','painters');
set(gca, 'TickLabelInterpreter', 'latex')
% title('Top 5\% linear velocity', 'Interpreter','latex')
xlabel('Time (s)', 'Interpreter','latex')
ylabel('Rotational velocity (rad/s)', 'Interpreter','latex')

folderpath = '~/ITA/Mestrado/publications/Tese/Discussion/';
print([folderpath,'topRVelocitiestopRVelocities.eps'],'-depsc');
% plot(attrib{maxTraj}.Time,attrib{maxTraj}.Data,'k','linewidth',3);
% % 
% % set(gca,'ColorOrderIndex',1)
% % for i = 1:length(attrib)
% %     j = find(attrib{i}.Data == max(attrib{i}.Data));
% %     plot(attrib{i}.Time(j),attrib{i}.Data(j),'.','markersize',20);
% %     if max(attrib{i}.Data) < max(attrib{minTraj}.Data)
% %         minTraj = i;
% %     end
% %     if max(attrib{i}.Data) > max(attrib{maxTraj}.Data)
% %         maxTraj = i;
% %     end
% % end
% % plot(attrib{minTraj}.Time,attrib{minTraj}.Data,'k','linewidth',3);
% % plot(pTrajList{maxTraj}.t,pTrajList{maxTraj}.K,'k','linewidth',3);
% % subplot(2,1,2)
% % for i = 1:length(velTrajList)
% %     plot(velTrajList{i}.t,velTrajList{i}.v);
% % end
% % hold off;
% % 
% % % %%
% % id = maxTraj;
% % subplot(1,2,1)
% % yPlot = fTrajList{id}.dthetadt;
% % plot(fTrajList{id}.t, yPlot/max(yPlot));
% % hold on;
% % yPlot = fTrajList{id}.theta;
% % plot(fTrajList{id}.t, yPlot/max(yPlot));
% % yPlot = fTrajList{id}.omega;
% % plot(fTrajList{id}.t, yPlot/max(yPlot));
% % subplot(1,2,2)
% % 
% % nPlotRows = 5;
% % nPlotColumns = 2;
% % axList = [];
% % idPlot = 1;
% % axList = [axList,subplot(nPlotRows,nPlotColumns,idPlot)];
% % idPlot = idPlot+1;
% % yPlot = fTrajList(id).x;
% % plot(fTrajList(id).t, yPlot);ylabel('x')
% % axList = [axList,subplot(nPlotRows,nPlotColumns,idPlot)];
% % idPlot = idPlot+1;
% % yPlot = fTrajList(id).y;
% % plot(fTrajList(id).t, yPlot);ylabel('y')
% % axList = [axList,subplot(nPlotRows,nPlotColumns,idPlot)];
% % idPlot = idPlot+1;
% % yPlot = fTrajList(id).vx;
% % plot(fTrajList(id).t, yPlot);ylabel('dxdt')
% % axList = [axList,subplot(nPlotRows,nPlotColumns,idPlot)];
% % idPlot = idPlot+1;
% % yPlot = fTrajList(id).vy;
% % plot(fTrajList(id).t, yPlot);ylabel('dydt')
% % axList = [axList,subplot(nPlotRows,nPlotColumns,idPlot)];
% % idPlot = idPlot+1;
% % yPlot = fTrajList(id).ax;
% % plot(fTrajList(id).t, yPlot);ylabel('ddxdt')
% % axList = [axList,subplot(nPlotRows,nPlotColumns,idPlot)];
% % idPlot = idPlot+1;
% % yPlot = fTrajList(id).ay;
% % plot(fTrajList(id).t, yPlot);ylabel('ddydt')
% % axList = [axList,subplot(nPlotRows,nPlotColumns,idPlot)];
% % idPlot = idPlot+1;
% % yPlot = fTrajList(id).linA;
% % plot(fTrajList(id).t, yPlot);ylabel('linA')
% % axList = [axList,subplot(nPlotRows,nPlotColumns,idPlot)];
% % idPlot = idPlot+1;
% % yPlot = fTrajList(id).rotA;
% % plot(fTrajList(id).t, yPlot);ylabel('rotA')
% % axList = [axList,subplot(nPlotRows,nPlotColumns,idPlot)];
% % idPlot = idPlot+1;
% % yPlot = fTrajList(id).theta;
% % plot(fTrajList(id).t, yPlot);ylabel('theta')
% % axList = [axList,subplot(nPlotRows,nPlotColumns,idPlot)];
% % idPlot = idPlot+1;
% % yPlot = fTrajList(id).dthetadt;
% % plot(fTrajList(id).t, yPlot);ylabel('dthetadt')
% % 
% % linkaxes(axList,'x')