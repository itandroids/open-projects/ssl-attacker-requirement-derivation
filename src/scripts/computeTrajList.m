function trajList = computeTrajList(param)
    x = param.dataX(ids);
    y = param.dataY(ids);
    theta = param.dataTheta(ids);

    t0 = min(tCapture);
    %% Get x,y,theta
    [tUnique,~,ids] = unique(round((tCapture-t0).*30));
    tUnique = tUnique ./ 30;
    xMean = accumarray(ids,x,[],@mean);
    yMean = accumarray(ids,y,[],@mean);
    thetaMean = accumarray(ids,theta,[],@mean);

    %% Filter x and y
    windowWidth = 21; %Must be odd
    windowPadding = floor(windowWidth/2);
    extX = [xMean(1).*ones(windowPadding,1);xMean;xMean(end).*ones(windowPadding,1)];
    extY = [yMean(1).*ones(windowPadding,1);yMean;yMean(end).*ones(windowPadding,1)];

%     xFtr = sum(extX((1:windowWidth) + (1:length(xMean))' - 1),2)./windowWidth;
    xFtr = weightedSum(extX((1:windowWidth) + (1:length(xMean))' - 1));

%     yFtr = sum(extY((1:windowWidth) + (1:length(yMean))' - 1),2)./windowWidth;
    yFtr = weightedSum(extY((1:windowWidth) + (1:length(yMean))' - 1));

    %% Get VelX and VelY
    windowWidth = 5; %Must be odd
    
    dxdt = derive(0,xFtr,tUnique,windowWidth);
    dydt = derive(0,yFtr,tUnique,windowWidth);
    dthetadt = deriveAngle(0,thetaMean,tUnique,windowWidth);
    angle = atan2(dydt,dxdt);
%     angle(angle>pi/2) = angle(angle>pi/2) - pi;
%     angle(angle<-pi/2) = angle(angle<-pi/2) + pi;

    %% Get AccX and AccY
%     windowWidth = 21; %Must be odd

    ddxdt = derive(0,dxdt,tUnique,windowWidth);
    ddydt = derive(0,dydt,tUnique,windowWidth);
    ddthetadt = derive(0,dthetadt,tUnique,windowWidth);
    windowWidth = 3; %Must be odd
    omega = deriveAngle(0,angle,tUnique,windowWidth);
    windowWidth = 21; %Must be odd
    
    %% Get Curvature and Rotation Radius
    K = sqrt((dxdt.*ddydt-dydt.*ddxdt).^2./(dxdt.^2+dydt.^2).^3);
    R = 1./K;
    %% Curvature derivative
    windowPadding = floor(windowWidth/2);
    dT = tUnique(windowWidth:end) - tUnique(1:(end-windowWidth+1));
    dT = [1/30.*ones(windowPadding,1); dT; 1/30.*ones(windowPadding,1)];
    dK = K(windowWidth:end) - K(1:(end-windowWidth+1));
    dK = [0.*ones(windowPadding,1);dK;dK(end).*ones(windowPadding,1)];
    dR = R(windowWidth:end) - R(1:(end-windowWidth+1));
    dR = [0.*ones(windowPadding,1);dR;dR(end).*ones(windowPadding,1)];

    dKdt = dK ./ dT;
    dRdt = dR ./ dT;

    %% Get angular velocity
    vel = sqrt(dxdt.^2+dydt.^2);
    acc = sqrt(ddxdt.^2+ddydt.^2);

    
end
%% Functions
function dxdt = derive(v0,xVec,tVec,windowWidth)
    windowPadding = floor(windowWidth/2);
    dT = tVec(windowWidth:end) - tVec(1:(end-windowWidth+1));
    dT = [windowWidth/30.*ones(windowPadding,1); dT; windowWidth/30.*ones(windowPadding,1)];
    dX = [xVec((windowPadding+1):end);xVec(end).*ones(windowPadding,1)] - ...
         [xVec(1)*ones(windowPadding,1);xVec(1:(end-windowPadding))];
    dxdt = dX ./ dT;
    dxdt(1:windowPadding) = v0.*ones(windowPadding,1);
end
function omega = deriveAngle(w0,angleVec,tVec,windowWidth)
    windowPadding = floor(windowWidth/2);
    dT = tVec(windowWidth:end) - tVec(1:(end-windowWidth+1));
    dT = [windowWidth/30.*ones(windowPadding,1); dT; windowWidth/30.*ones(windowPadding,1)];
    dAngle = [angleVec((windowPadding+1):end);angleVec(end).*ones(windowPadding,1)] - ...
             [angleVec(1)*ones(windowPadding,1);angleVec(1:(end-windowPadding))];

    dAngle(dAngle>pi) = -2*pi + dAngle(dAngle>pi);
    dAngle(dAngle<-pi) = 2*pi + dAngle(dAngle<-pi);
    
    dAngleAux = dAngle;
    
    dAngleAux(dAngle>pi/2) = -pi + dAngle(dAngle>pi/2);
    dAngleAux(dAngle<-pi/2) = pi + dAngle(dAngle<-pi/2);
        
    l = length(dAngle);
    
    for i = 2:l
        value = dAngle(i);
        if abs(dAngleAux(i)-dAngle(i-1)) < abs(dAngle(i)-dAngle(i-1))
            value = dAngleAux(i);
        end
        dAngle(i) = value;
    end
    
    omega = dAngle ./ dT;
    omega(1:windowPadding) = w0.*ones(windowPadding,1);
end
function V = weightedSum(A)
    ASize = size(A);
    nRows = ASize(1);
    nColumns = ASize(2);
    midColumn = floor((nColumns-1)/2)+1;
    auxA = A;
    for i = 1:nColumns
        auxA(:,i) = A(:,i) .* max(0,(1-abs(midColumn-i)./midColumn)./midColumn);
    end

    V = sum(auxA,2);
end

function meanValue = meanAngle(angleVec)
    meanValue = atan2(mean(sin(angleVec)),mean(cos(angleVec)));
end