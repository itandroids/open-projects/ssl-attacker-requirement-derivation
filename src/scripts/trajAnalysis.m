trajectoryTest = readtable('debug/trajectoryTest.txt');
% worldLog = readtable('debug/world_log.txt');
figure;
ax = [];

cmdx = trajectoryTest.cmd0.*cos(trajectoryTest.theta)-trajectoryTest.cmd1.*sin(trajectoryTest.theta);
cmdy = trajectoryTest.cmd0.*sin(trajectoryTest.theta)+trajectoryTest.cmd1.*cos(trajectoryTest.theta);

ax = [ax,subplot(3,2,1)];
plot(trajectoryTest.t,trajectoryTest.x)
hold on
plot(trajectoryTest.t,trajectoryTest.refx)
% plot(worldLog.t,worldLog.x)

ax = [ax,subplot(3,2,2)];
plot(trajectoryTest.t,trajectoryTest.vx)
hold on
plot(trajectoryTest.t,trajectoryTest.refvx)
plot(trajectoryTest.t,cmdx)
% plot(worldLog.t,worldLog.vx)

ax = [ax,subplot(3,2,3)];
plot(trajectoryTest.t,trajectoryTest.y)
hold on
plot(trajectoryTest.t,trajectoryTest.refy)
% plot(worldLog.t,worldLog.x)

ax = [ax,subplot(3,2,4)];
plot(trajectoryTest.t,trajectoryTest.vy)
hold on
plot(trajectoryTest.t,trajectoryTest.refvy)
plot(trajectoryTest.t,cmdy)
% plot(worldLog.t,worldLog.vx)

ax = [ax,subplot(3,2,5)];
plot(trajectoryTest.t,trajectoryTest.theta*180/pi)
hold on;
plot(trajectoryTest.t,trajectoryTest.reftheta*180/pi)
% plot(worldLog.t,worldLog.theta*180/pi)

ax = [ax,subplot(3,2,6)];
plot(trajectoryTest.t,trajectoryTest.w*180/pi)
hold on;
plot(trajectoryTest.t,trajectoryTest.refw*180/pi)
plot(trajectoryTest.t,trajectoryTest.cmd2*180/pi)

linkaxes(ax,'x');