for n = 1:17
number = num2str(n);
data = importdata(['visionLog',number,'.txt']);
param.FrameNumber = data.data(:,1);
param.TCapture = data.data(:,2);
param.TSent = data.data(:,3);
param.isBlue = data.data(:,4);
param.CameraId = data.data(:,5);
param.robotId = data.data(:,6);
param.dataX = data.data(:,7)./1000;
param.dataY = data.data(:,8)./1000;
param.dataTheta = data.data(:,9);

maxBlueRID = max(param.robotId(param.isBlue == true));
maxYellowRID = max(param.robotId(param.isBlue == false));

for rId = 1:maxBlueRID
    trajectory = PadTrajectory(param,rId,true);
    for i = 1:length(trajectory)
        writetable(trajectory{i},['log-',number,'-blue-',num2str(rId),'-',num2str(i),'.csv']);
    end
end

for rId = 1:maxYellowRID
    trajectory = PadTrajectory(param,rId,false);
    for i = 1:length(trajectory)
        writetable(trajectory{i},['log-',number,'-yellow-',num2str(rId),'-',num2str(i),'.csv']);
    end
end

end

function trajectory = PadTrajectory(param,rId,isBlue)
    id1 = find(param.robotId == rId & param.isBlue == isBlue);
    
    trajectory = {};
    nTraj = 0;
    nValid = 0;

    t0 = min(param.TCapture(id1));
    
    [t,~,id2] = unique(round(30*(param.TCapture(id1)-t0)));
    t = t ./ 30;
    x = accumarray(id2,param.dataX(id1),[],@mean);
    y = accumarray(id2,param.dataY(id1),[],@mean);
    theta = accumarray(id2,param.dataTheta(id1),[],@meanAngle);
    
    if(length(x) < 1)
        return
    end

    L1 = length(t);
    
    minDT = min(t(2:end)-t(1:end-1));

    maxL2 = 30;

    xPadded = [x(1)];
    yPadded = [y(1)];
    thetaPadded = [theta(1)];
    tPadded = [t(1)];
    kPadded = [1];
    t0 = t(1);

    for i = 2:L1
        dt = t(i)-t(i-1);
        L2 = floor(dt/minDT-1);
        if L2 > maxL2
            if nValid > 300
                nTraj = nTraj+1;
                trajectory{nTraj} = table(xPadded,yPadded,thetaPadded,tPadded,kPadded,'VariableNames',{'x';'y';'theta';'t';'k'});
            end
            nValid = 0;
            xPadded = [];
            yPadded = [];
            thetaPadded = [];
            tPadded = [];
            kPadded = [];
            t0 = t(i);
        else
            for j = 1:L2
                xPadded = [xPadded;x(i-1)];
                yPadded = [yPadded;y(i-1)];
                thetaPadded = [thetaPadded;theta(i-1)];
                tPadded = [tPadded;t(i-1)+minDT*j-t0];
                kPadded = [kPadded;0];
            end
        end
        nValid = nValid + 1;
        xPadded = [xPadded;x(i)];
        yPadded = [yPadded;y(i)];
        thetaPadded = [thetaPadded;theta(i)];
        tPadded = [tPadded;t(i)-t0];
        kPadded = [kPadded;1];
    end
    
    if nValid > 300
        nTraj = nTraj+1;
        trajectory{nTraj} = table(xPadded,yPadded,thetaPadded,tPadded,kPadded,'VariableNames',{'x';'y';'theta';'t';'k'});
    end
end

function meanValue = meanAngle(angleVec)
    meanValue = atan2(mean(sin(angleVec)),mean(cos(angleVec)));
end