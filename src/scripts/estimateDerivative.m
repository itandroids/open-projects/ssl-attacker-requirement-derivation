function dxdt = estimateDerivative(x, t, windowWidth)
    windowPadding = floor(windowWidth/2);
    dT = t(windowWidth:end) - t(1:(end-windowWidth+1));
    dT = [windowWidth/30.*ones(windowPadding,1); dT; windowWidth/30.*ones(windowPadding,1)];
    dX = [x((windowPadding+1):end);x(end).*ones(windowPadding,1)] - ...
             [x(1)*ones(windowPadding,1);x(1:(end-windowPadding))];
    
    l = length(dX);
    
    dxdt = dX ./ dT;
    dxdt(1:windowPadding) = zeros(windowPadding,1);
    
    %Filter derivative
    extW = [dxdt(1).*ones(windowPadding,1);dxdt;dxdt(end).*ones(windowPadding,1)];
    dxdt = weightedSum(extW((1:windowWidth) + (1:length(dxdt))' - 1));
end

function V = weightedSum(A)
    ASize = size(A);
    nRows = ASize(1);
    nColumns = ASize(2);
    midColumn = floor((nColumns-1)/2)+1;
    auxA = A;
    for i = 1:nColumns
        auxA(:,i) = A(:,i) .* max(0,(1-abs(midColumn-i)./midColumn)./midColumn);
    end

    V = sum(auxA,2);
end