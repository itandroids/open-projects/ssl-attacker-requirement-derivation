import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy.constants import g
from scipy.spatial.transform import Rotation as R
import navpy
import auto_ks
from pykalman import KalmanFilter
import IPython as ipy
import time

from os import listdir

#from utils import latexify

np.random.seed(0)
np.set_printoptions(precision=1)

# read data
good_filenames = [name for name in listdir('src/scripts') if name.startswith("log-") and name.endswith(".csv")]
good_filenames.sort()

for filename in good_filenames:
    df = pd.read_csv('src/scripts/'+filename)
    pose = df[['x', 'y', 'theta']]
    t = df[['t']]
    k = df[['k']]

    # concatenate measurements
    y = np.array(df[['x', 'y', 'theta']])

    # create known measurements mask; gps and velocity measurements are only valid when they change
    #K_gps = np.append(np.array([True]), np.linalg.norm(np.diff(y[:, :3], axis=0), axis=1) != 0)
    #K_vel = np.append(np.array([True]), np.linalg.norm(np.diff(y[:, -2:], axis=0), axis=1) != 0)
    """ K = np.hstack([
        np.repeat(K_gps[:, None], 3, axis=1),
        np.ones((K_gps.size, 3), dtype=np.bool),
        np.repeat(K_vel[:, None], 2, axis=1)
    ]) """
    K = np.repeat(np.array(k, dtype=bool),3,axis=1)
    # initialize parameters
    eye = np.eye(1)
    zeros = np.zeros((1, 1))
    h = 1. / 30.0
    lam = 1e-10
    alpha = 1e-4
    n = 7
    m = 3
    A = np.bmat([
        [eye, h * eye, 0.5 * h * h * eye, zeros, zeros,           zeros, zeros],
        [zeros, eye,           h * eye, zeros, zeros,           zeros, zeros],
        [zeros, zeros,           eye, zeros, zeros,           zeros, zeros],
        [zeros, zeros,           zeros, eye, h * eye, 0.5 * h * h * eye, zeros],
        [zeros, zeros,           zeros, zeros, eye,           h * eye, zeros],
        [zeros, zeros,           zeros, zeros, zeros,           eye, zeros],
        [zeros, zeros,           zeros, zeros, zeros,           zeros, eye],
    ])
    C = np.bmat([
        [eye, zeros, zeros, zeros, zeros, zeros, zeros],
        [zeros, zeros, zeros, eye, zeros, zeros, zeros],
        [zeros, zeros, zeros, zeros, zeros, zeros, eye]
    ])
    W_neg_sqrt = np.eye(n)
    V_neg_sqrt = .01*np.eye(m)
    params_initial = auto_ks.KalmanSmootherParameters(np.array(A), np.array(W_neg_sqrt), np.array(C), np.array(V_neg_sqrt))

    # proximal operator for r
    def prox(params, t):
        r = 0.0
        W_neg_sqrt = params.W_neg_sqrt / (t * alpha + 1.0)
        idx = np.arange(W_neg_sqrt.shape[0])
        W_neg_sqrt[idx, idx] = 0.0
        r += alpha * np.sum(np.square(W_neg_sqrt))
        W_neg_sqrt[idx, idx] = np.diag(params.W_neg_sqrt)

        V_neg_sqrt = params.V_neg_sqrt / (t * alpha + 1.0)
        idx = np.arange(V_neg_sqrt.shape[0])
        V_neg_sqrt[idx, idx] = 0.0
        r += alpha * np.sum(np.square(V_neg_sqrt))
        V_neg_sqrt[idx, idx] = np.diag(params.V_neg_sqrt)

        return auto_ks.KalmanSmootherParameters(A, W_neg_sqrt, C, V_neg_sqrt), r

    # choose missing measurements
    gps_meas_idx = np.where(K)[0]
    np.random.shuffle(gps_meas_idx)
    M = np.zeros(y.shape, dtype=np.bool)
    M[gps_meas_idx[:int(gps_meas_idx.size * .2)], :2] = True

    # choose test measurements
    M_test = np.zeros(y.shape, dtype=np.bool)
    M_test[gps_meas_idx[int(gps_meas_idx.size * .2):int(gps_meas_idx.size * .4)], :2] = True

    if M_test.sum() <= 0 or M.sum() <= 0:
        continue

    # evaluate initial parameters
    xhat_initial, yhat_initial, DT = auto_ks.kalman_smoother(params_initial, y, K & ~M_test, lam)
    loss_auto = np.linalg.norm(yhat_initial[M_test] - y[M_test])**2 / M_test.sum()
    print("initial test loss", loss_auto)

    # run kalman smoother auto-tuning
    tic = time.time()
    params, info = auto_ks.tune(params_initial, prox, y, K & ~M_test, lam, M=M, lr=1e-2, verbose=True, niter=25)
    toc = time.time()
    print("time", toc-tic, "s")
    xhat, yhat, DT = auto_ks.kalman_smoother(params, y, K & ~M_test, lam)

    # evaluate
    loss_auto = np.linalg.norm(yhat[M_test] - y[M_test])**2 / M_test.sum()
    print ("final_test_loss:", loss_auto)

    print(np.diag(np.linalg.inv(params.W_neg_sqrt @ params.W_neg_sqrt.T)))
    print(np.diag(np.linalg.inv(params.V_neg_sqrt @ params.V_neg_sqrt.T)))

    d = {'t': np.array(t.transpose())[0], 'k': np.array(k.transpose())[0],'x': xhat[:,0], 'vx' : xhat[:,1],'ax': xhat[:,2], 'y' : xhat[:,3], 'vy': xhat[:,4], 'ay' : xhat[:,5], 'theta' : xhat[:,6], 'rawtheta' : y[:,2]}
    df = pd.DataFrame(data=d)
    df.to_csv('src/scripts/kf-'+filename)

""" 
latexify(4, 3)
plt.plot(yhat_initial[::50, 0], yhat_initial[::50, 1], '--', alpha=.5, c='black', label='before')
plt.plot(yhat[::50, 0], yhat[::50, 1], '-', alpha=.5, c='black', label='after')
plt.scatter(y[::50, 0], y[::50, 1], s=.5, c='black')
plt.legend()
plt.xlabel("East (m)")
plt.ylabel("North (m)")
plt.subplots_adjust(left=.15, bottom=.2)
plt.savefig("figs/driving.pdf")
plt.close()

ipy.embed() """