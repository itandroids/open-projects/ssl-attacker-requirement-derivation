dataError = importdata('simulationTest-error.txt');
dataFinalState = importdata('simulationTest-finalState.txt');
dataInput = importdata('simulationTest-input.txt');
dataNoise = importdata('simulationTest-noise.txt');
dataPos = importdata('simulationTest-pos.txt');
dataVel = importdata('simulationTest-vel.txt');

%%
t = dataPos.data(:,1);
startId = find(t==0);
x = dataPos.data(:,2);
y = dataPos.data(:,3);
phi = dataPos.data(:,4);
refX = dataPos.data(:,5);
refY = dataPos.data(:,6);

v = dataVel.data(:,2);
vn = dataVel.data(:,3);
wa = dataVel.data(:,4);

v_ref = dataInput.data(:,2);
vn_ref = dataInput.data(:,3);
wa_ref = dataInput.data(:,4);

displacement = dataFinalState.data(:,2);
% figure; 
hold on;

histogram(displacement,'binwidth',0.001)

% plotBorder(t,sqrt(v.^2+vn.^2));
% plotData(t,sqrt(v.^2+vn.^2),s tartId);
%plotData(x,y,startId);

% plotData(t,sqrt(v.^2+vn.^2),startId);
% plotBorder(t,sqrt(v.^2+vn.^2));

% plot(displacement);

hold off;
 

%meanError(x,y,refX,refY,startId)

function mError = meanError(x,y,xRef,yRef,startId)
lastId = [(startId(2:end)-1);length(x)];

mError = mean(sqrt((x(lastId)-xRef(lastId)).^2+(y(lastId)-yRef(lastId)).^2));
end

function plotData(xData,yData,startId)
for i = 1:(length(startId)-1)
    plot(xData(startId(i):(startId(i+1)-1)),yData(startId(i):(startId(i+1)-1)));
end
plot(xData(startId(end):end),yData(startId(end):end));
end

function plotBorder(t, yData)
[tUnique,~,ids] = unique(t);
yMin = accumarray(ids,yData,[],@min);
yMax = accumarray(ids,yData,[],@max);
plot(tUnique,yMin,'b','LineWidth',3);
plot(tUnique,yMax,'b','LineWidth',3);
end