data = importdata('representation/map/R05/grid2/simulationMap-R05-100-100.txt', ' ',1);
s = data.data;
%%
data = importdata('representation/distribution/g2-M-distribution.txt', ' ',1);

bT = data.data(:,1);
bR = data.data(:,2);
rT = data.data(:,3);
l = data.data(:,4);
t = -0.5 + l.*(1.0);

B = [bR.*cos(bT),bR.*sin(bT)]';
T = [t,zeros(length(t),1)]';
vB = (T-B)./sqrt((B(1,:)-T(1,:)).*(B(1,:)-T(1,:))+(B(2,:)-T(2,:)).*(B(2,:)-T(2,:)));
d = -(B(1,:).*vB(1,:)+B(2,:).*vB(2,:))-sqrt(0.5-(B(1,:).*vB(2,:)-B(2,:).*vB(1,:))'.^2)';
I = B + [d;d].*vB;
interceptionTheta = atan2(I(2,:),I(1,:));
interceptionTime = d./8;
robotTheta = rT';

% interceptionTheta = data.data(:,1);
% robotTheta = data.data(:,2);
% interceptionTime = data.data(:,3);

p = data.data(:,5);
errorTheta = interceptionTheta - robotTheta;

[xq,yq] = meshgrid(-0.8:0.05:0.8, -0.8:0.05:0.8);
vq = griddata(interceptionTheta,robotTheta,p,xq,yq);
%mesh(xq,yq,vq)

zscaled = p*10;
cn = 3; % Number Of Colors (Scale AsAppropriate)
colormap jet;
scatter3(errorTheta,interceptionTime,p,'.');
%figure;
%scatter3(robotTheta,interceptionTheta,s,'.');
%%
figure;
set(gca,'xscale','log','yscale','log');
hold on;
data = importdata('debug3-R05-M-analysis-1e5.txt', ' ');
rX = log10(data(:,1));
rY = log10(data(:,2));
p = data(:,3);

[xq,yq] = meshgrid(min(rX):(max(rX)-min(rX))/50:max(rX), min(rY):(max(rY)-min(rY))/50:max(rY));
vq = griddata(rX,rY,p,xq,yq);
meshHandler = mesh(10.^xq,10.^yq,vq);
%colormap jet
meshHandler.EdgeColor = 'flat';
meshHandler.FaceColor = 'none';
hold on
scatter3(10.^rX,10.^rY,p,'.r')

xlabel('Variance X,Y');
ylabel('Variance Theta');
zlabel('Defense probability');

% v1 = find(InterceptionTheta>=0);
% v2 = find(RobotTheta(v1)>=0);
% validId = v1(v2);
% 
% errorTheta = InterceptionTheta - RobotTheta;

% for i = 1:length(validId)
%    id1 = find(errorTheta(validId) == errorTheta(validId(i)));
%    id2 = find(ArrivalTime(validId(id1)) == ArrivalTime(validId(i)));
%    equivalentId = validId(id1(id2));
%    Result(equivalentId) = max(Result(equivalentId));
% end

%tri = delaunay(errorTheta(validId), ArrivalTime(validId));
%trisurf(tri, errorTheta(validId), ArrivalTime(validId), Result(validId));

% scatter3(errorTheta(validId), ArrivalTime(validId), Result(validId),'.');