%% Split trajectories
function trajList = splitTrajectory(traj)
    trajList = [];

    Traj = struct;
    Traj.x = [];
    Traj.y = [];
    Traj.theta = [];
    Traj.t = [];
    Traj.vx = [];
    Traj.vy = [];
    Traj.dthetadt = [];
    Traj.ddthetadt2 = [];
    Traj.ax = [];
    Traj.ay = [];
    Traj.v = [];
    Traj.a = [];
    
    l = length(traj.t);

    running = false;
    nTraj = 0;
    tBegin = traj.t(1);
    tPrev = traj.t(1);
    xBegin = traj.x(1);
    yBegin = traj.y(1);
    currentTraj = Traj;
    minVel = 0.15;%0.15;
    minAcc = 0.15;
    maxDT = 1;%0.2;
    for i = 1:l
        if running && traj.t(i) - tPrev > maxDT
            running = false;
            if length(currentTraj.x) > 50
                nTraj = nTraj+1;
                
                currentTraj.v = sqrt(currentTraj.vx.^2+currentTraj.vy.^2);
                currentTraj.a = sqrt(currentTraj.ax.^2+currentTraj.ay.^2);
                currentTraj.linA = (currentTraj.ax.*currentTraj.vx+currentTraj.ay.*currentTraj.vy)./currentTraj.v;
                currentTraj.rotA = (-currentTraj.ax.*currentTraj.vy+currentTraj.ay.*currentTraj.vx)./currentTraj.v;
                if abs(currentTraj.v(end)) < 1
                    trajList = [trajList;currentTraj];
                end
            end
        end
        if running || abs(traj.v(i)) >= minVel || abs(traj.a(i)) >= minAcc
            if ~running
                running = true;
                tBegin = traj.t(i);
                xBegin = traj.x(i);
                yBegin = traj.y(i);
                currentTraj = Traj;
            end
            currentTraj.x = [currentTraj.x;traj.x(i)-xBegin];
            currentTraj.y = [currentTraj.y;traj.y(i)-yBegin];
            currentTraj.theta = [currentTraj.theta;traj.theta(i)];
            currentTraj.t = [currentTraj.t;traj.t(i)-tBegin];
            currentTraj.vx = [currentTraj.vx;traj.vx(i)];
            currentTraj.vy = [currentTraj.vy;traj.vy(i)];
            currentTraj.dthetadt = [currentTraj.dthetadt;traj.dthetadt(i)];
            currentTraj.ddthetadt2 = [currentTraj.ddthetadt2;traj.ddthetadt2(i)];
            currentTraj.ax = [currentTraj.ax;traj.ax(i)];
            currentTraj.ay = [currentTraj.ay;traj.ay(i)];
        end
        if running && abs(traj.v(i)) < minVel && abs(traj.a(i)) < minAcc
            running = false;
            if length(currentTraj.x) > 50
                nTraj = nTraj+1;   

                currentTraj.v = sqrt(currentTraj.vx.^2+currentTraj.vy.^2);
                currentTraj.a = sqrt(currentTraj.ax.^2+currentTraj.ay.^2);
                currentTraj.linA = (currentTraj.ax.*currentTraj.vx+currentTraj.ay.*currentTraj.vy)./currentTraj.v;
                currentTraj.rotA = (-currentTraj.ax.*currentTraj.vy+currentTraj.ay.*currentTraj.vx)./currentTraj.v;
                if abs(currentTraj.v(end)) < 1
                    trajList = [trajList;currentTraj];
                end
            end
        end
        tPrev = traj.t(i);
    end
    if running
        if length(currentTraj.x) > 50
            nTraj = nTraj+1;   

            currentTraj.v = sqrt(currentTraj.vx.^2+currentTraj.vy.^2);
            currentTraj.a = sqrt(currentTraj.ax.^2+currentTraj.ay.^2);
            currentTraj.linA = (currentTraj.ax.*currentTraj.vx+currentTraj.ay.*currentTraj.vy)./currentTraj.v;
            currentTraj.rotA = (-currentTraj.ax.*currentTraj.vy+currentTraj.ay.*currentTraj.vx)./currentTraj.v;
            if abs(currentTraj.v(end)) < 1
                trajList = [trajList;currentTraj];
            end
        end
    end
end