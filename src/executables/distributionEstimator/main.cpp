#include "distribution_model.h"
#include "uniformEstimator.h"
#include "representativeGrid.h"
#include "sampling_model.h"
#include <iostream>


int main(){

  int nRepresentativeElements[4] = {10,10,40,10};
  RepresentativeGrid representativeGrid(4);
  representativeGrid.setLinearVariable(0,
    0.5/nRepresentativeElements[0]*M_PI,(1-0.5/nRepresentativeElements[0])*M_PI,
    nRepresentativeElements[0]);
  representativeGrid.setLinearVariable(1,
    2.0 + 0.5/nRepresentativeElements[1],2.0 + (1-0.5/nRepresentativeElements[1]),
    nRepresentativeElements[1]);
  representativeGrid.setRelativeVariable(2,0,
  -M_PI/4,M_PI/4,
  nRepresentativeElements[2]);
  representativeGrid.setLinearVariable(3,
  0.5/nRepresentativeElements[3],(1-0.5/nRepresentativeElements[3]),
  nRepresentativeElements[3]);
  representativeGrid.computeRepresentativeSamples();

  UniformEstimator uniformDistribution;
  std::vector<UniformEstimator::Model> variableModels;
  variableModels.resize(4);
  variableModels[0].min = 0.0;
  variableModels[0].max = M_PI;
  variableModels[0].local = false;
  variableModels[1].min = 2.0;
  variableModels[1].max = 3.0;
  variableModels[1].local = false;
  //Aligned
  variableModels[2].min = 0.0;
  variableModels[2].max = 0.0;
  variableModels[2].local = true; 

  //Misaligned
  /* variableModels[2].min = 0.0;
  variableModels[2].max = M_PI;
  variableModels[2].local = false; */

  variableModels[3].min = 0.0;
  variableModels[3].max = 1.0;
  variableModels[3].local = false;
  uniformDistribution.setModels(variableModels);
  uniformDistribution.estimate(representativeGrid);

  std::ofstream file;
  file.open("../out/resolution/representation/distribution/g1-A-distribution.txt");
  uniformDistribution.saveDistribution(file);
  file.close();

  return 0;
}