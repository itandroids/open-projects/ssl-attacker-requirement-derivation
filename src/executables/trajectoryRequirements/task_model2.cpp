#include "task_model2.h"
#include "fstream"
#include "iostream"

TaskModel2::TaskModel2(){
  kp[0] = kp[1] = 12;
  kp[2] = 10;
  kd[0] = kd[1] = 0.2;
  kd[2] = 0.4;

  worldModel.setEstimatorVarianceL(0);
  worldModel.setEstimatorVarianceR(0);
  worldModel.setResolution(1e5,1e5);
  
  worldModel.setSaturation(5,5,4,4);
  worldModel.setRobotModel(0.7,2*M_PI*15);

  //Debug
  //filePos.open("debug/trajectoryTest.txt"/* , std::ofstream::out | std::ofstream::app */);
  //fileVel.open("debug/simulationTest-vel.txt"/* , std::ofstream::out | std::ofstream::app */);
  //fileInput.open("debug/simulationTest-input.txt"/* , std::ofstream::out | std::ofstream::app */);
  //fileError.open("debug/simulationTest-error.txt"/* , std::ofstream::out | std::ofstream::app */);
  //fileNoise.open("debug/simulationTest-noise.txt"/* , std::ofstream::out | std::ofstream::app */);
  //fileFinalState.open("debug/simulationTest-finalState.txt"/* , std::ofstream::out | std::ofstream::app */);
  //filePos << "t x y theta vx vy w refx refy reftheta cmd0 cmd1 cmd2\n";
  //fileVel << "t v vn wa\n";
  //fileInput << "t v_ref vn_ref wa_ref\n";
  //fileError << "t radialError distError angleError\n";
  //fileNoise << "t xNoise yNoise phiNoise\n";
  //fileFinalState << "t displacement\n";
}

TaskModel2::~TaskModel2(){
  //filePos.close();
  //fileVel.close();
  //fileInput.close();
  //fileError.close();
  //fileNoise.close();
  //fileFinalState.close();
}

void TaskModel2::simulate(std::vector<State> trajectory, Pose initialPose, double &costPos, double &costVel, double &costTheta){
  
  int nWaypoints = trajectory.size();

  State currentState, cameraState;
  double controlCommand[3];

  double auxCostP = 0, auxCostV = 0, auxCostA = 0;

  delayTime = 0, controlPeriod = 1.0/30, stepPeriod = 1.0/300;

  worldModel.startWorld(initialPose,delayTime,controlPeriod,stepPeriod);


  for(int i = 0; i < nWaypoints; i++){
    worldModel.getCameraData(cameraState);
    controlLaw(trajectory[i],cameraState,controlCommand);
    worldModel.step(controlCommand);
    worldModel.getCurrentState(currentState);
    updateCost(currentState,trajectory[i],auxCostP,auxCostV, auxCostA);
    
    /* filePos << i/30.0 << " "
            << currentState.x << " "
            << currentState.y << " "
            << currentState.theta << " "
            << currentState.vx << " "
            << currentState.vy << " "
            << currentState.w << " "
            << trajectory[i].x << " "
            << trajectory[i].y << " "
            << trajectory[i].theta << " "
            << controlCommand[0] << " "
            << controlCommand[1] << " "
            << controlCommand[2] << "\n"; */
  }

  costPos += (auxCostP);
  costVel += (auxCostV);
  costTheta += (auxCostA);

  return;
}

void TaskModel2::setRobotModel(double xi, double wn){
  worldModel.setRobotModel(xi,wn);
}

void TaskModel2::controlLaw(State reference, State currentState, double command[3]){
  double e_front = (reference.x-currentState.x)*cos(currentState.theta)+(reference.y-currentState.y)*sin(currentState.theta);
  double d_e_front = reference.vx*cos(currentState.theta)+reference.vy*sin(currentState.theta);
  
  double e_side = -(reference.x-currentState.x)*sin(currentState.theta)+(reference.y-currentState.y)*cos(currentState.theta);
  double d_e_side = -reference.vx*sin(currentState.theta)+reference.vy*cos(currentState.theta);
  
  double e_theta = wrap2pi(reference.theta-currentState.theta);

  command[0] = kp[0]*e_front+/* kd[0]* */d_e_front;
  
  command[1] = kp[1]*e_side+/* kd[1]* */d_e_side;
  /* command[1] = 0; */
  
  command[2] = kp[2]*e_theta + /* kd[2]* */(reference.w - currentState.w);
  
  if(sqrt(command[0]*command[0] + command[1]*command[1])> 5){
    double ratio = 5 / sqrt(command[0]*command[0] + command[1]*command[1]);
    command[0] *= ratio;
    command[1] *= ratio;
    command[2] *= ratio;
  }
  if(abs(command[2]) > 5){
    double ratio = 5 / abs(command[2]);
    command[0] *= ratio;
    command[1] *= ratio;
    command[2] *= ratio;
  }

  return;
}

void TaskModel2::updateCost(State currentState, State reference, double& costP, double& costV, double& costA){
  costP += (currentState.x-reference.x)*(currentState.x-reference.x) + 
            (currentState.y-reference.y)*(currentState.y-reference.y);
  costV += (currentState.vx-reference.vx)*(currentState.vx-reference.vx) + 
            (currentState.vy-reference.vy)*(currentState.vy-reference.vy);
  costA += wrap2pi(currentState.theta-reference.theta)*
                wrap2pi(currentState.theta-reference.theta);

  return;
}


double TaskModel2::wrap2pi(double angle){
  while(angle>M_PI)angle -= 2*M_PI;
  while(angle<=-M_PI)angle += 2*M_PI;

  return angle;
}
