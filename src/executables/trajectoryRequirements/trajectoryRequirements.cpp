#include "task_model2D.h"
#include "vector"
#include "types.h"
#include "math.h"
#include "iostream"
#include "fstream"
#include "string"
#include <filesystem>
#include "helper.h"
#include "sampler.h"

namespace fs = std::filesystem;

struct CostError{
  double posError, velError, thetaError;
};

void TranslationalClosedLoopReq(std::vector<std::vector<State>> trajectories, int nPoints,
                                double xiLinear, 
                                double fLinear, 
                                double xiRotation, 
                                double fRotation){
  auto performanceFunction1 = [=](double kp, double kd){
    std::cout << "kp: " << kp << " kd: " << kd << "\n";
    double costPos, costVel, costTheta;
    Pose initialPose;
    TaskModel2D task;
    costPos = 0;
    costVel = 0;
    costTheta = 0;
    task.setModelLinear(xiLinear,2*M_PI*fLinear);
    task.setModelRotation(xiRotation,2*M_PI*fRotation);
    task.setTranslationalControl(kp,kd);
    task.setRotationalControl(0,0);
    for(auto trajectory : trajectories){
      initialPose.x = trajectory[0].x;
      initialPose.y = trajectory[0].y;
      initialPose.theta = trajectory[0].theta;
      task.simulate(trajectory,initialPose,costPos,costVel,costTheta);
    }
    CostError costError;
    costError.posError = sqrt(costPos / nPoints);
    costError.velError = sqrt(costVel / nPoints);
    costError.thetaError = sqrt(costTheta / nPoints);
    return costError;
  };

  auto performanceFunction2 = [=](double kp){
    auto f = [=](double kd){return performanceFunction1(kp,kd);};
    return sampler<CostError>(0,5,11,f);
  };

  auto res = sampler<std::vector<Point<CostError>>>(1,15,15,performanceFunction2);
  std::ofstream fileOut;
  fileOut.open("trajReqDyn-transl2-withFF-kp-kd.txt");
  fileOut << "kp kd posError velError thetaError\n";
  for(auto elem1 : res){
    for(auto elem2 : elem1.y){
      fileOut << elem1.x << " " << elem2.x << " " << elem2.y.posError << " " << elem2.y.velError << " " << elem2.y.thetaError << std::endl;
    }
  }
  fileOut.close();

  return;
}

void RotationalClosedLoopReq(std::vector<std::vector<State>> trajectories, int nPoints,
                                double xiLinear, 
                                double fLinear, 
                                double xiRotation, 
                                double fRotation,
                                double kpTranslational,
                                double kdTranslational){
  auto performanceFunction1 = [=](double kp, double kd){
    std::cout << "kp: " << kp << " kd: " << kd << "\n";
    double costPos, costVel, costTheta;
    Pose initialPose;
    TaskModel2D task;
    costPos = 0;
    costVel = 0;
    costTheta = 0;
    task.setModelLinear(xiLinear,2*M_PI*fLinear);
    task.setModelRotation(xiRotation,2*M_PI*fRotation);
    task.setTranslationalControl(kpTranslational,kdTranslational);
    task.setRotationalControl(kp,kd);
    for(auto trajectory : trajectories){
      initialPose.x = trajectory[0].x;
      initialPose.y = trajectory[0].y;
      initialPose.theta = trajectory[0].theta;
      task.simulate(trajectory,initialPose,costPos,costVel,costTheta);
    }
    CostError costError;
    costError.posError = sqrt(costPos / nPoints);
    costError.velError = sqrt(costVel / nPoints);
    costError.thetaError = sqrt(costTheta / nPoints);
    return costError;
  };

  auto performanceFunction2 = [=](double kp){
    auto f = [=](double kd){return performanceFunction1(kp,kd);};
    return sampler<CostError>(0,5,11,f);
  };

  auto res = sampler<std::vector<Point<CostError>>>(1,15,15,performanceFunction2);
  std::ofstream fileOut;
  fileOut.open("trajReqDyn-rot-kp-kd.txt");
  fileOut << "kp kd posError velError thetaError\n";
  for(auto elem1 : res){
    for(auto elem2 : elem1.y){
      fileOut << elem1.x << " " << elem2.x << " " << elem2.y.posError << " " << elem2.y.velError << " " << elem2.y.thetaError << std::endl;
    }
  }
  fileOut.close();

  return;
}

void TranslationalOpenLoopReq(std::vector<std::vector<State>> trajectories, int nPoints,
                                double xiRotation, 
                                double fRotation,
                                double kpTranslational,
                                double kdTranslational,
                                double kpRotational,
                                double kdRotational){
  auto performanceFunction1 = [=](double xi, double f){
    std::cout << "xi: " << xi << " f: " << f << "\n";
    double costPos, costVel, costTheta;
    Pose initialPose;
    TaskModel2D task;
    costPos = 0;
    costVel = 0;
    costTheta = 0;
    task.setModelLinear(xi,2*M_PI*f);
    task.setModelRotation(xiRotation,2*M_PI*fRotation);
    task.setTranslationalControl(kpTranslational,kdTranslational);
    task.setRotationalControl(kpRotational,kdRotational);
    for(auto trajectory : trajectories){
      initialPose.x = trajectory[0].x;
      initialPose.y = trajectory[0].y;
      initialPose.theta = trajectory[0].theta;
      task.simulate(trajectory,initialPose,costPos,costVel,costTheta);
    }
    CostError costError;
    costError.posError = sqrt(costPos / nPoints);
    costError.velError = sqrt(costVel / nPoints);
    costError.thetaError = sqrt(costTheta / nPoints);
    return costError;
  };

  auto performanceFunction2 = [=](double xi){
    auto f = [=](double f){return performanceFunction1(xi,f);};
    return sampler<CostError>(0,60,7,f);
  };

  auto res = sampler<std::vector<Point<CostError>>>(0.1,0.9,9,performanceFunction2);
  std::ofstream fileOut;
  fileOut.open("trajReqDyn-transl4-xi-f.txt");
  fileOut << "xi f posError velError thetaError\n";
  for(auto elem1 : res){
    for(auto elem2 : elem1.y){
      fileOut << elem1.x << " " << elem2.x << " " << elem2.y.posError << " " << elem2.y.velError << " " << elem2.y.thetaError << std::endl;
    }
  }
  fileOut.close();

  return;
}

void RotationalOpenLoopReq(std::vector<std::vector<State>> trajectories, int nPoints,
                                double xiLinear, 
                                double fLinear,
                                double kpTranslational,
                                double kdTranslational,
                                double kpRotational,
                                double kdRotational){
  auto performanceFunction1 = [=](double xi, double f){
    std::cout << "xi: " << xi << " f: " << f << "\n";
    double costPos, costVel, costTheta;
    Pose initialPose;
    TaskModel2D task;
    costPos = 0;
    costVel = 0;
    costTheta = 0;
    task.setModelLinear(xiLinear,2*M_PI*fLinear);
    task.setModelRotation(xi,2*M_PI*f);
    task.setTranslationalControl(kpTranslational,kdTranslational);
    task.setRotationalControl(kpRotational,kdRotational);
    for(auto trajectory : trajectories){
      initialPose.x = trajectory[0].x;
      initialPose.y = trajectory[0].y;
      initialPose.theta = trajectory[0].theta;
      task.simulate(trajectory,initialPose,costPos,costVel,costTheta);
    }
    CostError costError;
    costError.posError = sqrt(costPos / nPoints);
    costError.velError = sqrt(costVel / nPoints);
    costError.thetaError = sqrt(costTheta / nPoints);
    return costError;
  };

  auto performanceFunction2 = [=](double xi){
    auto f = [=](double f){return performanceFunction1(xi,f);};
    return sampler<CostError>(10,150,15,f);
  };

  auto res = sampler<std::vector<Point<CostError>>>(0.1,0.9,9,performanceFunction2);
  std::ofstream fileOut;
  fileOut.open("trajReqDyn-rot3-xi-f.txt");
  fileOut << "xi f posError velError thetaError\n";
  for(auto elem1 : res){
    for(auto elem2 : elem1.y){
      fileOut << elem1.x << " " << elem2.x << " " << elem2.y.posError << " " << elem2.y.velError << " " << elem2.y.thetaError << std::endl;
    }
  }
  fileOut.close();

  return;
}

void DiscretizationAndDelayReq(std::vector<std::vector<State>> trajectories, int nPoints,
                                double xiLinear, 
                                double fLinear,
                                double xiRotational, 
                                double fRotational,
                                double kpTranslational,
                                double kdTranslational,
                                double kpRotational,
                                double kdRotational){
                                  std::cout << "DiscretizationAndDelayReq\n";
  auto performanceFunction1 = [=](double controlRatio, double Tdelay){
    double Tdisc = (1.0*controlRatio) / 30.0;
    std::cout << "Tdisc: " << Tdisc << " Tdelay: " << Tdelay << "\n";
    double costPos, costVel, costTheta;
    Pose initialPose;
    TaskModel2D task;
    costPos = 0;
    costVel = 0;
    costTheta = 0;
    task.setModelLinear(xiLinear,2*M_PI*fLinear);
    task.setModelRotation(xiRotational,2*M_PI*fRotational);
    task.setTranslationalControl(kpTranslational,kdTranslational);
    task.setRotationalControl(kpRotational,kdRotational);
    task.setControlPeriod(Tdisc);
    task.setDelayTime(Tdelay);
    for(auto trajectory : trajectories){
      initialPose.x = trajectory[0].x;
      initialPose.y = trajectory[0].y;
      initialPose.theta = trajectory[0].theta;
      task.simulate(trajectory,initialPose,costPos,costVel,costTheta);
    }
    CostError costError;
    costError.posError = sqrt(costPos / nPoints);
    costError.velError = sqrt(costVel / nPoints);
    costError.thetaError = sqrt(costTheta / nPoints);
    return costError;
  };

  auto performanceFunction2 = [=](double controlRatio){
    auto f = [=](double Tdelay){return performanceFunction1(controlRatio,Tdelay);};
    return sampler<CostError>(0,0.1,11,f);
  };

  auto res = sampler<std::vector<Point<CostError>>>(1,4,4,performanceFunction2);
  std::ofstream fileOut;
  fileOut.open("trajReqDyn-Disc-Delay6.txt");
  fileOut << "Fdisc Tdelay posError velError thetaError\n";
  for(auto elem1 : res){
    for(auto elem2 : elem1.y){
      fileOut << 30.0/elem1.x << " " << elem2.x << " " << elem2.y.posError << " " << elem2.y.velError << " " << elem2.y.thetaError << std::endl;
    }
  }
  fileOut.close();

  return;
}

void QuantizationReq(std::vector<std::vector<State>> trajectories, int nPoints,
                    double xiLinear, 
                    double fLinear,
                    double xiRotational, 
                    double fRotational,
                    double kpTranslational,
                    double kdTranslational,
                    double kpRotational,
                    double kdRotational,
                    double Tdisc,
                    double Tdelay){
  auto performanceFunction1 = [=](int resolution){
    std::cout << "Resolution: " << resolution << "\n";
    double costPos, costVel, costTheta;
    Pose initialPose;
    TaskModel2D task;
    costPos = 0;
    costVel = 0;
    costTheta = 0;
    task.setModelLinear(xiLinear,2*M_PI*fLinear);
    task.setModelRotation(xiRotational,2*M_PI*fRotational);
    task.setTranslationalControl(kpTranslational,kdTranslational);
    task.setRotationalControl(kpRotational,kdRotational);
    task.setControlPeriod(Tdisc);
    task.setDelayTime(Tdelay);
    task.setResolution(resolution);
    for(auto trajectory : trajectories){
      initialPose.x = trajectory[0].x;
      initialPose.y = trajectory[0].y;
      initialPose.theta = trajectory[0].theta;
      task.simulate(trajectory,initialPose,costPos,costVel,costTheta);
    }
    CostError costError;
    costError.posError = sqrt(costPos / nPoints);
    costError.velError = sqrt(costVel / nPoints);
    costError.thetaError = sqrt(costTheta / nPoints);
    return costError;
  };

  auto res = sampler<CostError>(10,210,21,performanceFunction1);
  std::ofstream fileOut;
  fileOut.open("trajReqDyn-resolution.txt");
  fileOut << "Resolution\n";
  for(auto elem1 : res){
      fileOut << elem1.x << " " << elem1.y.posError << " " << elem1.y.velError << " " << elem1.y.thetaError << std::endl;
  }
  fileOut.close();

  return;
}

void NoiseTranslReq(std::vector<std::vector<State>> trajectories, int nPoints,
                    double xiLinear, 
                    double fLinear,
                    double xiRotational, 
                    double fRotational,
                    double kpTranslational,
                    double kdTranslational,
                    double kpRotational,
                    double kdRotational,
                    double Tdisc,
                    double Tdelay,
                    int resolution){
  auto performanceFunction1 = [=](double VariancePos, double VarianceVel){
    std::cout << "VariancePos: " << VariancePos << " VarianceVel: " << VarianceVel << "\n";
    double costPos, costVel, costTheta;
    Pose initialPose;
    TaskModel2D task;
    costPos = 0;
    costVel = 0;
    costTheta = 0;
    task.setModelLinear(xiLinear,2*M_PI*fLinear);
    task.setModelRotation(xiRotational,2*M_PI*fRotational);
    task.setTranslationalControl(kpTranslational,kdTranslational);
    task.setRotationalControl(kpRotational,kdRotational);
    task.setControlPeriod(Tdisc);
    task.setDelayTime(Tdelay);
    task.setResolution(resolution);
    task.setTranslationalVariance(VariancePos);
    task.setTranslationalVarianceVel(VarianceVel);
    int nRepetitions = 16;
    for (int i = 1; i <= nRepetitions; i++){
      for(auto trajectory : trajectories){
        initialPose.x = trajectory[0].x;
        initialPose.y = trajectory[0].y;
        initialPose.theta = trajectory[0].theta;
        task.simulate(trajectory,initialPose,costPos,costVel,costTheta);
      }
    }
    CostError costError;
    costError.posError = sqrt(costPos / (nPoints * nRepetitions));
    costError.velError = sqrt(costVel / (nPoints * nRepetitions));
    costError.thetaError = sqrt(costTheta / (nPoints * nRepetitions));
    return costError;
  };

  auto performanceFunction2 = [=](double VariancePos){
    auto f = [=](double VarianceVel){return performanceFunction1(VariancePos,VarianceVel);};
    return sampler<CostError>(0,1.0,5,f);
  };

  auto res = sampler<std::vector<Point<CostError>>>(0,0.2,6,performanceFunction2);
  std::ofstream fileOut;
  fileOut.open("trajReqDyn-noise-translREP16.txt");
  fileOut << "VariancePos VarianceVel posError velError thetaError\n";
  for(auto elem1 : res){
    for(auto elem2 : elem1.y){
      fileOut << elem1.x << " " << elem2.x << " " << elem2.y.posError << " " << elem2.y.velError << " " << elem2.y.thetaError << std::endl;
    }
  }
  fileOut.close();

  return;
}

void NoiseRotReq(std::vector<std::vector<State>> trajectories, int nPoints,
                    double xiLinear, 
                    double fLinear,
                    double xiRotational, 
                    double fRotational,
                    double kpTranslational,
                    double kdTranslational,
                    double kpRotational,
                    double kdRotational,
                    double Tdisc,
                    double Tdelay,
                    double VariancePos,
                    double VarianceVel,
                    int resolution){
  auto performanceFunction1 = [=](double VarianceTheta, double VarianceOmega){
    std::cout << "VarianceTheta: " << VarianceTheta << " VarianceOmega: " << VarianceOmega << "\n";
    double costPos, costVel, costTheta;
    Pose initialPose;
    TaskModel2D task;
    costPos = 0;
    costVel = 0;
    costTheta = 0;
    task.setModelLinear(xiLinear,2*M_PI*fLinear);
    task.setModelRotation(xiRotational,2*M_PI*fRotational);
    task.setTranslationalControl(kpTranslational,kdTranslational);
    task.setRotationalControl(kpRotational,kdRotational);
    task.setControlPeriod(Tdisc);
    task.setDelayTime(Tdelay);
    task.setResolution(resolution);
    task.setTranslationalVariance(VariancePos);
    task.setTranslationalVarianceVel(VarianceVel);
    task.setRotationalVariance(VarianceTheta*M_PI/180.0);
    task.setRotationalVarianceVel(VarianceOmega*M_PI/180.0);
    int nRepetitions = 16;
    for (int i = 1; i <= nRepetitions; i++){
      for(auto trajectory : trajectories){
        initialPose.x = trajectory[0].x;
        initialPose.y = trajectory[0].y;
        initialPose.theta = trajectory[0].theta;
        task.simulate(trajectory,initialPose,costPos,costVel,costTheta);
      }
    }
    CostError costError;
    costError.posError = sqrt(costPos / (nPoints * nRepetitions));
    costError.velError = sqrt(costVel / (nPoints * nRepetitions));
    costError.thetaError = sqrt(costTheta / (nPoints * nRepetitions));
    return costError;
  };

  auto performanceFunction2 = [=](double VarianceTheta){
    auto f = [=](double VarianceOmega){return performanceFunction1(VarianceTheta,VarianceOmega);};
    return sampler<CostError>(0,10,6,f);
  };

  auto res = sampler<std::vector<Point<CostError>>>(0,10,6,performanceFunction2);
  std::ofstream fileOut;
  fileOut.open("trajReqDyn-noise-rotREP16.txt");
  fileOut << "VarianceTheta VarianceOmega posError velError thetaError\n";
  for(auto elem1 : res){
    for(auto elem2 : elem1.y){
      fileOut << elem1.x << " " << elem2.x << " " << elem2.y.posError << " " << elem2.y.velError << " " << elem2.y.thetaError << std::endl;
    }
  }
  fileOut.close();

  return;
}

int main(int argc, char ** argv){
  std::string path = "/home/gabriel/ITA/Mestrado/development/RequirementDerivation/src/scripts/trajectories";
  std::string aux;

  int nPoints = 0;
  std::vector<std::vector<State>> trajectories;
  State auxState;
  double t,x,y,theta,dxdt,dydt,dthetadt;
  const int nColumns = 7;
  for (const auto & entry : fs::directory_iterator(path)){
    std::ifstream file;
    file.open(entry.path());
    if (!file.is_open()){
      std::cout << "failed to open file\n";
      break;
    }
    for(int i = 0; i < nColumns; i++){
      file >> aux;
    }
    std::vector<State> traj;
    while(file >> t >> x >> y >> theta >> dxdt >> dydt >> dthetadt){
      auxState.x=x;
      auxState.y=y;
      auxState.theta=theta;
      auxState.vx=dxdt;
      auxState.vy=dydt;
      auxState.w=dthetadt;
      traj.push_back(auxState);
      nPoints++;
    }
    trajectories.push_back(traj);
    file.close();
  }

  //TranslationalClosedLoopReq(trajectories,nPoints,0.7,30,0.7,120);//Result: Kp = 10, Kd = 0.5
  //RotationalClosedLoopReq(trajectories,nPoints,0.7,30,0.7,120,10,0.5);//Result: Kp = 10, Kd = 3 // Kp = 5, Kd = 0.5
  //TranslationalOpenLoopReq(trajectories,nPoints,0.7,120,10,0.5,5,0.3);//Result: xi = 0.5, f = 20
  //RotationalOpenLoopReq(trajectories, nPoints, 0.5, 20, 10,0.5,5,0.5);//Result: xi = 0.5, f = 80
  //DiscretizationAndDelayReq(trajectories,nPoints,0.5,20,0.5,80,10,0.5,5,0.5);//Result: F = 15Hz, Tdelay = 0.03s
  /* NoiseTranslReq(trajectories, nPoints, 0.5, 20, 0.5, 80,
                    10, 0.5, 5, 0.5, 1.0/15.0, 0.03,
                    1e5);  *///Result: sigmaPos = 0.1 m,sigmaVel = 1 m/s 
  NoiseRotReq(trajectories, nPoints, 0.5, 20, 0.5, 80,
                    10, 0.5, 5, 0.5, 1.0/15.0, 0.05,
                    0.05,0.25,
                    1e5);

  return 0;
}