#include "sampling_model.h"
#include "vector"
#include "fstream"
#include "string"
#include "iostream"
#include "helper.h"

int main(){
  SamplingModel samplingModel(true);
  int nElements = 50;
  std::vector<double> thetaIVector = linspace(0, M_PI, nElements);
  std::vector<double> thetaRVector = linspace(0, M_PI, nElements);
  std::vector<double> timeVector = linspace(0.1, 0.3, nElements);
  std::ofstream file;
  file.open("samplingDistribution.txt");
  file << "thetaI thetaR time jointedProb\n";
  /* samplingModel.saveDistribution(file,thetaIVector,thetaRVector,timeVector,false,1e7); */
  file.close();

 /*  std::vector<std::vector<double>> checkData(nElements,std::vector<double>(nElements,0));
  std::ifstream inFile;
  inFile.open("errorDistribution.txt");
  std::string auxstring;
  inFile >> auxstring >> auxstring >> auxstring >> auxstring;
  samplingModel.loadErrorDistribution(inFile, checkData);

  std::cout << "error time jointedProb\n";
  for(int i = 0; i < nElements; i++){
    for(int j = 0; j < nElements; j++){
      std::cout << errorVector[i] << " " << timeVector[j] << " " << checkData[i][j] << "\n";
    }
  }
  inFile.close(); */

  return 0;
}