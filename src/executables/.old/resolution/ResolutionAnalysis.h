#ifndef RESOLUTIONANALYSIS_H
#define RESOLUTIONANALYSIS_H

#include "sampling_model.h"
#include "task_model.h"
#include "fstream"

class ResolutionAnalysis{
public:
  ResolutionAnalysis();
  ~ResolutionAnalysis();

  double DefenseEstimation(int resolutionX, int resolutionY);
  double DefenseEstimation2(int resolutionX, int resolutionY);

private:
  TaskModel task;
  RandomFactory randomFactory;
  SamplingModel samplingModel;
  int numberOfSamples;
  double controlUpdateTime;
  double tol;
  std::ofstream file;
};

#endif