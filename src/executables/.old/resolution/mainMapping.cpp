#include "helper.h"
#include "fstream"
#include "string"
#include "task_model.h"
#include "random_factory.h"
#include "constants.h"
#include <iostream>

int main(){
  int nElements = 50;
  std::vector<double> representativeInterception = linspace(0,M_PI,nElements);
  std::vector<double> representativeRobotInitial = linspace(0,M_PI,nElements);
  std::vector<double> representativeTime = linspace(0.1, 0.3, nElements);
  
  std::array<int,8> resolutionX = {5, 10 ,20, 30, 40, 50, 75, 100};
  std::array<int,8> resolutionY = {5, 10 ,20, 30, 40, 50, 75, 100};
  
  std::ofstream file;
  std::string filename("simulationMap-v2");

  TaskModel task;
  task.setBaseStepPeriod(1.0 / 1200);
  double controlUpdateTime = 1.0 / 60;
  task.setControlUpdateTime(controlUpdateTime);
  task.setDelayTime(1.0 / 30);
  task.setRobotModel(0.2,2*M_PI*15);
  double kp[3] = {12,12,12}/* 12.6573 */;
  double kd[3] = {0.4,0.4,0.4}/* 12.6573 */;
  task.setControlParameters(kp,kd);
  task.setMaxVelocity(5);
  task.setMaxAcceleration(4);
  double tol = 4e-2;
  task.setTrajectoryRadius(1.0);

  RandomFactory randomFactory;

  Interception interception;
  Pose robotPose;

  double mean = 0;

  for(int i = 0; i < 8; i++){
    for(int j = 0; j < 8; j++){
      std::cout << "starting: " <<
        filename + "-" + std::to_string(resolutionX[i]) + "-" + std::to_string(resolutionY[j]) << "\n";
      file.open(filename + "-" + std::to_string(resolutionX[i]) + "-" + std::to_string(resolutionY[j]) + ".txt");
      file << nElements*nElements*nElements <<"\n";
      for(int r = 0; r < nElements; r++){
        for(int s = 0; s < nElements; s++){
          for(int t = 0; t < nElements; t++){
            file << representativeInterception[r] << " ";
            file << representativeRobotInitial[s] << " ";
            file << representativeTime[t] << " ";
            interception.theta = representativeInterception[r];
            interception.time = representativeTime[t];
            robotPose.theta = representativeRobotInitial[s];
            robotPose.x = GoalieTrajectory::GOALIE_RADIUS * cos(robotPose.theta);
            robotPose.y = GoalieTrajectory::GOALIE_RADIUS * sin(robotPose.theta);
            mean = 0;
            for(int m = 0; m < 5; m++){
              mean += (task.simulate(interception,robotPose,
                    randomFactory.getUniformValue(0,controlUpdateTime),
                    resolutionX[i],resolutionY[j])<tol);
            }
            file << mean/5 << "\n";
          }
        }
      }
      file.close();
    }
  }

  return 0;
}