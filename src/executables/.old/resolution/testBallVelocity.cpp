#include "random_factory.h"
#include "types.h"
#include "constants.h"

#include <fstream>
#include <iostream>
#include <math.h>

double quantify(double x, double const & unitLength){
  return (std::floor(x/unitLength) + 1.0/2.0)*unitLength;
}

int main(){
  RandomFactory factory;
  Ball ball, ballQ;

  int total = 100000;

  int resolution = 200;
  double unitLength = 6.0 / resolution;
  double controlPeriod = 1 / 60.0;

  std::ofstream file;
  file.open("ballTest.txt"/* , std::ofstream::out | std::ofstream::app */);

  for(int i = 0; i < total; i++){
    ball = factory.getRandomBall();
    ballQ.vx = quantify(ball.vx*controlPeriod, unitLength)/controlPeriod;
    ballQ.vy = quantify(ball.vy*controlPeriod, unitLength)/controlPeriod;
    //std::cout << ballQ.vy << "\n";
    file << atan2(-ballQ.vy,ballQ.vx)/M_PI*180-atan2(-ball.vy,ball.vx)/M_PI*180/* << " "  <<
            ball.x << " " << ball.y */ << "\n";
  }
  file.close();

  return 0;
}