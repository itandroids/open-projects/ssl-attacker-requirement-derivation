#include "gainOptimization.h"
#include "iostream"

GainOptimization::GainOptimization(){
  numberOfSamples = 10;
  task.setBaseStepPeriod(1.0 / 12000);
  controlUpdateTime = 1.0 / 60;
  task.setControlUpdateTime(1.0 / 60);
  task.setDelayTime(1.0 / 30);
  task.setRobotModel(0.2,2*M_PI*15);
  task.setMaxVelocity(5);
  task.setMaxAcceleration(4);
  
}

GainOptimization::~GainOptimization(){
}

double GainOptimization::optimizeGains(double rd, double goalTheta, double time, double startingTheta, double kp[3], double kd[3]){
  task.setTrajectoryRadius(rd);
  interception.time = time;
  interception.theta = goalTheta;
  this->startingPose.x = rd*cos(startingTheta);
  this->startingPose.y = rd*sin(startingTheta);
  this->startingPose.theta = startingTheta;
  nlopt::opt opt(nlopt::LN_COBYLA, 6);
  std::function<double(std::vector<double> &, std::vector<double> &)>f = 
    [&](std::vector<double> &x, std::vector<double> &grad){
      return costFunction(x,grad);
  };
  const std::vector<double> lb(6,0.0);
  opt.set_lower_bounds(lb);
  opt.set_min_objective(GainOptimization::wrap, this);
  opt.set_xtol_rel(1e-4);
  std::vector<double> x(6);
  double minError;
  x[0] = 12, x[1] = 12, x[2] = 12, x[3] = 1, x[4] = 1, x[5] = 1;
  try{
      opt.optimize(x, minError);
  }
  catch(std::exception &e) {
      std::cout << "nlopt failed: " << e.what() << std::endl;
  }
  kp[0] = x[0];
  kp[1] = x[1];
  kp[2] = x[2];
  kd[0] = x[3];
  kd[1] = x[4];
  kd[2] = x[5];
  return minError;
}

double GainOptimization::costFunction(const std::vector<double> &x, std::vector<double> &grad){
  kp[0] = x[0];
  kp[1] = x[1];
  kp[2] = x[2];
  kd[0] = x[3];
  kd[1] = x[4];
  kd[2] = x[5];
  task.setControlParameters(kp,kd);
  double mean = 0;
  for(int i = 0; i < numberOfSamples; i++){
    mean += task.simulate(interception,startingPose,
      randomFactory.getUniformValue(0,controlUpdateTime),1e6,1e6);
  }
  return mean / numberOfSamples;
}

double GainOptimization::wrap(const std::vector<double> &x, std::vector<double> &grad, void * data){
  return reinterpret_cast<GainOptimization*>(data)->costFunction(x,grad);
}