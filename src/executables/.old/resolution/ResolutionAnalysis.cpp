#include "ResolutionAnalysis.h"
#include "iostream"
#include "helper.h"
#include "constants.h"

#include <fstream>
#include <vector>

ResolutionAnalysis::ResolutionAnalysis():samplingModel(true){
  numberOfSamples = 1e4;
  task.setBaseStepPeriod(1.0 / 12000);
  controlUpdateTime = 1.0 / 60;
  task.setControlUpdateTime(1.0 / 60);
  task.setDelayTime(1.0 / 30);
  task.setRobotModel(0.2,2*M_PI*15);
  double kp[3] = {12,12,12}/* 12.6573 */;
  double kd[3] = {0.4,0.4,0.4}/* 12.6573 */;
  task.setControlParameters(kp,kd);
  task.setMaxVelocity(5);
  task.setMaxAcceleration(4);
  tol = 4e-2;
  task.setTrajectoryRadius(1.0);
  
  file.open("defenseProbRes.txt"/* , std::ofstream::out | std::ofstream::app */);
  file << "resX resY p\n";
}

ResolutionAnalysis::~ResolutionAnalysis(){
  file.close();
}
double ResolutionAnalysis::DefenseEstimation(int resolutionX, int resolutionY){
  double numberOfSuccesses = 0;
  Interception interception;
  Pose robotPose;
  robotPose.x = 0.5*cos(M_PI*0.25);
  robotPose.y = 0.6*sin(M_PI*0.25);
  robotPose.theta = M_PI/3.0 /* atan2(robotPose.y,robotPose.x) */;
  std::cout << "\n";
  for(int i = 0; i < numberOfSamples; i++){
    if(((100*i)%numberOfSamples) == 0)
    {std::cout<<"\033[1A\r" << "Percentage: " << (100*i)/numberOfSamples << "%\n" << std::flush;}
    samplingModel.getNewSample();
    interception = samplingModel.getInterception();
/*     interception.time = 5.0; */
    /* interception.theta = M_PI*0.7; */
    robotPose = samplingModel.getRobotInitialPose();
    numberOfSuccesses += (task.simulate(interception,robotPose,
      randomFactory.getUniformValue(0,controlUpdateTime),resolutionX,resolutionY)<tol);
  }
  file << resolutionX << " " << resolutionY << " " << numberOfSuccesses / numberOfSamples << "\n";

  return numberOfSuccesses / numberOfSamples;
}

double ResolutionAnalysis::DefenseEstimation2(int resolutionX, int resolutionY){
  double meanProb = 0;
  int nElements = 50;
  std::vector<double> interceptionVector = linspace(0, M_PI, nElements);
  std::vector<double> robotVector = linspace(0, M_PI, nElements);
  std::vector<double> timeVector = linspace(0.1, 0.3, nElements);
  std::vector<std::vector<std::vector<double>>> jointProb(
    nElements,std::vector<std::vector<double>>(nElements,std::vector<double>(nElements,0)));
  std::ifstream inFile;
  inFile.open("errorDistribution.txt");
  std::string auxstring;
  inFile >> auxstring >> auxstring >> auxstring;
  /* samplingModel.loadDistribution(inFile, jointProb) */;

  Interception interception;
  Pose robotPose;
  std::cout << "\n";
  for(int i = 0; i < nElements; i++){
    for(int j = 0; j < nElements; j++){
      for(int k = 0; k < nElements; k++){
        if(((100*(nElements*nElements*i+nElements*j+k))%
            (nElements*nElements*nElements)) == 0){
          std::cout<<"\033[1A\r" << "Percentage: " << 
            ((100*(nElements*nElements*i+nElements*j+k))/
              (nElements*nElements*nElements)) << "%\n" << std::flush;
        }

        interception.theta = interceptionVector[i];
        interception.time = timeVector[k];
        robotPose.theta = robotVector[j];
        robotPose.x = GoalieTrajectory::GOALIE_RADIUS * cos(robotPose.theta);
        robotPose.y = GoalieTrajectory::GOALIE_RADIUS * sin(robotPose.theta);
        meanProb += (task.simulate(interception,robotPose,
        randomFactory.getUniformValue(0,controlUpdateTime),resolutionX,resolutionY)<tol)*jointProb[i][j][k];
      }
    }
  }
  file << resolutionX << " " << resolutionY << " " << meanProb << "\n";
  return meanProb;
}