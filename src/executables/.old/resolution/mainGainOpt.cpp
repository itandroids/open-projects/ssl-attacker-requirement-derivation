#include "gridfunction.h"
#include "gainOptimization.h"
#include "types.h"

#include <iostream>
#include <fstream>
#include <array>
#include <queue>
#include <functional>


int main(){
  std::ofstream file;
  file.open("gainOpt.txt");
  file << "rd error time Kp0 Kp1 Kp2 Kd0 Kd1 Kd2\n";
  double minError, kp[3], kd[3];

  GainOptimization optimization;
  std::array<double,2> rd = {0.5, 1.0};
  std::array<double,10> error;
  for(int i = 0; i < 10; i++){
    error[i] = 0.1 + i*0.4/9.0;
  }
  std::array<double,10> time;
  for(int i = 0; i < 10; i++){
    time[i] = 0.1 + i*0.4/9.0;
  }

  std::function<double(double,double,double)>f = 
  [&](double rd, double error, double time){
    file << rd << " " << error << " " << time;
    minError = optimization.optimizeGains(rd,M_PI * 0.5,time,M_PI * 0.5 - error / rd, kp, kd);

    std::cout << "minError: " << minError << " kp0: " << kp[0] << " kp1: " << kp[1] << " kp2: " << kp[2]
              << " kd0: " << kd[0] << " kd1: " << kd[1] << " kd2: " << kd[2] << "\n";
    file << " " << kp[0] << " " << kp[1] << " " << kp[2]
          << " " << kd[0] << " " << kd[1] << " " << kd[2] << "\n";

    return 0;
  };

  gridFunction(f,rd,error,time);

  file.close();
  return 0;
}
