#ifndef GAINOPTIMIZATION_H
#define GAINOPTIMIZATION_H

#include "task_model.h"
#include "random_factory.h"
#include "fstream"

#include <nlopt.hpp>

class GainOptimization{
public:
  GainOptimization();
  ~GainOptimization();

  double optimizeGains(double rd, double goalTheta, double time, double startingTheta, double kp[3], double kd[3]);

private:
  static double wrap(const std::vector<double> &x, std::vector<double> &grad, void * data);
  double costFunction(const std::vector<double> &x, std::vector<double> &grad);
  int numberOfSamples;
  double controlUpdateTime;
  double kp[3];
  double kd[3];
  Interception interception;
  Pose startingPose;
  RandomFactory randomFactory;
  TaskModel task;
};

#endif