#include "gridfunction.h"
#include "ResolutionAnalysis.h"

#include <iostream>
#include <array>
#include <queue>
#include <functional>


int main(){
  ResolutionAnalysis analysis;
  std::array<int,8> resolutionX = {5, 10 ,20, 30, 40, 50, 75, 100};
  std::array<int,8> resolutionY = {5, 10 ,20, 30, 40, 50, 75, 100};

  std::function<double(int,int)>f = [&](int a, int b){return analysis.DefenseEstimation(a,b);};

  gridFunction(f,resolutionX,resolutionY);

  return 0;
}