#include "task_model.h"
#include "performanceModel.h"
#include "representativeGrid.h"
#include "performanceMapper.h"
#include <vector>
#include <array>
#include <iostream>
#include "chrono"

int main(){
  TaskModel taskModel;
  taskModel.setBaseStepPeriod(1.0 / 1200);
  taskModel.setControlUpdateTime(1.0 / 60);
  taskModel.setDelayTime(1.0 / 30);
  taskModel.setRobotModel(0.2,2*M_PI*15);
  double kp[3] = {12,12,12}/* 12.6573 */;
  double kd[3] = {0.4,0.4,0.4}/* 12.6573 */;
  taskModel.setControlParameters(kp,kd);
  taskModel.setMaxVelocity(5);
  taskModel.setMaxAcceleration(4);
  taskModel.setTrajectoryRadius(GoalieTrajectory::GOALIE_RADIUS);
  taskModel.setEstimatorVarianceL(0);
  taskModel.setEstimatorVarianceR(0);

  PerformanceModel performanceModel(taskModel);
  performanceModel.setTolerance(4e-2);
  performanceModel.setNumberOfIterations(10);

  /*int nRepresentativeElements[3] = {10,50,10};
  RepresentativeGrid representativeGrid(3);
  representativeGrid.setLinearVariable(0,0,M_PI,nRepresentativeElements[0]);
  representativeGrid.setRelativeVariable(1,0,-0.5*M_PI,0.5*M_PI,nRepresentativeElements[1]);
  representativeGrid.setLinearVariable(2,0.1,0.3,nRepresentativeElements[2]); */

  int nRepresentativeElements[4] = {10,10,40,10};
  RepresentativeGrid representativeGrid(4);
  representativeGrid.setLinearVariable(0,
    0.5/nRepresentativeElements[0]*M_PI,(1-0.5/nRepresentativeElements[0])*M_PI,
    nRepresentativeElements[0]);
  representativeGrid.setLinearVariable(1,
    2.0 + 0.5/nRepresentativeElements[1],2.0 + (1-0.5/nRepresentativeElements[1]),
    nRepresentativeElements[1]);
  representativeGrid.setRelativeVariable(2,0,
  -M_PI/4,M_PI/4,
  nRepresentativeElements[2]);
  representativeGrid.setLinearVariable(3,
  0.5/nRepresentativeElements[3],(1-0.5/nRepresentativeElements[3]),
  nRepresentativeElements[3]);
  representativeGrid.computeRepresentativeSamples();

  PerformanceMapper performanceMapper;

  const unsigned nElementsX = 8;
  const unsigned nElementsY = 8;
  std::array<int,nElementsX> resolutionX = {5, 10, 20, 30, 40, 50, 75, 100};
  std::array<int,nElementsY> resolutionY = {5, 10, 20, 30, 40, 50, 75, 100};
  std::ofstream file;
  std::string filename("../out/resolution/representation/map/R10/tmp-simulationMap-R10");
  std::vector<double> parameters(2,0);

  auto start = std::chrono::steady_clock::now();
  auto end = start;
  std::chrono::duration<double> elapsedTime;
  int seconds;
  int minutes;
  int hours;
  for(int i = 0; i < nElementsX; i++){
    for(int j = 0; j < nElementsY; j++){
      end =  std::chrono::steady_clock::now();
      elapsedTime = end-start;
      hours = ((int)elapsedTime.count())/3600;
      minutes = ((int)elapsedTime.count())/60-60*hours;
      seconds = (int)elapsedTime.count()-60*minutes-3600*hours;
      std::cout << "[t = " << hours << ":" << minutes << ":" << seconds << "] ";
      std::cout << "Mapping: " <<
        filename + "-" + std::to_string(resolutionX[i]) + "-" + std::to_string(resolutionY[j]) << "\n";
      file.open(filename + "-" + std::to_string(resolutionX[i]) + "-" + std::to_string(resolutionY[j]) + ".txt");
      parameters[0] = resolutionX[i];
      parameters[1] = resolutionY[j];
      performanceModel.setParameters(parameters,PerformanceModel::ParamType::RESOLUTION);
      performanceMapper.mapPerformance(performanceModel,representativeGrid);
      performanceMapper.saveMap(file);
      file.close();
    }
  }

  return 0;
}