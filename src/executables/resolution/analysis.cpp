#include "performanceAnalysis.h"
#include "representativeEstimator.h"
#include "monteCarloEstimator.h"
#include "distribution_model.h"
#include <fstream>
#include <iostream>

enum AnalysisType{REPRESENTATIVE, MONTECARLO};

void runRepresentativeAnalysis(PerformanceAnalysis &analysis, PerformanceModel::ParamType &paramType){
  DistributionModel distributionModel;
  std::ifstream distributionFile;
  distributionFile.open("../out/resolution/representation/distribution/g1-M-distribution.txt");
  distributionModel.loadDistribution(distributionFile);
  distributionFile.close();
  
  PerformanceMapper performanceMapper;
  
  RepresentativeEstimator estimator;
  estimator.setPrefix(std::string("../out/resolution/representation/map/R10/grid1/simulationMap-R10-"));
  estimator.setSamplingDistribution(distributionModel);
  estimator.setPerformanceMapper(performanceMapper);

  analysis.setProbabilityEstimator(estimator);
  analysis.analyze();

  return;
}

void runMonteCarloAnalysis(PerformanceAnalysis &analysis, PerformanceModel::ParamType &paramType){
  bool aligned = false;
  SamplingModel samplingModel(aligned);

  TaskModel taskModel;
  taskModel.setBaseStepPeriod(1.0 / 1200);
  taskModel.setControlUpdateTime(1.0 / 60);
  taskModel.setDelayTime(1.0 / 30);
  taskModel.setRobotModel(0.2,2*M_PI*15);
  double kp[3] = {12,12,12}/* 12.6573 */;
  double kd[3] = {0.4,0.4,0.4}/* 12.6573 */;
  taskModel.setControlParameters(kp,kd);
  taskModel.setMaxVelocity(5);
  taskModel.setMaxAcceleration(4);
  taskModel.setTrajectoryRadius(GoalieTrajectory::GOALIE_RADIUS);
  taskModel.setResolution(100,100);
  taskModel.setEstimatorVarianceL(0);
  taskModel.setEstimatorVarianceR(0);
  
  PerformanceModel performanceModel(taskModel);
  performanceModel.setTolerance(4e-2);
  performanceModel.setNumberOfIterations(1);//Number of iterations for each sample with random tMismatch
  
  MonteCarloEstimator estimator;
  estimator.setNumberOfSamples(1e5);
  estimator.setSamplingModel(samplingModel);
  estimator.setPerformanceModel(performanceModel, paramType);

  analysis.setProbabilityEstimator(estimator);
  analysis.analyze();

  return;
}

int main(){
  int nVariables = 2;
  PerformanceAnalysis analysis(nVariables);
  
  /* std::vector<double> resX{5,10,20,30,40,50,75,100};
  std::vector<double> resY{5,10,20,30,40,50,75,100};
  analysis.setVariableSet(0,resX);
  analysis.setVariableSet(1,resY); */

/*   std::vector<double> varianceL{1e-2*1e-2,4e-2*4e-2,7e-2*7e-2,
                              1e-1*1e-1,15e-2*15e-2,2e-1*2e-1,25e-2*25e-2,3e-1*3e-1,4e-1*4e-1,5e-1*5e-1,
                              6e-1*6e-1,7e-1*7e-1,8e-1*8e-1,9e-1*9e-1,1e0*1e0,15e-1*15e-1, 2e0*2e0
                              }; */
  std::vector<double> varianceL{ 1e-2,pow(10,-1.7),pow(10,-1.3),1e-1,pow(10,-0.7),pow(10,-0.3),1e0,pow(10,0.3),pow(10,0.7),1e1,pow(10,1.3),pow(10,1.7), 1e2};
  /* std::vector<double> varianceR{1e-2*1e-2,4e-2*4e-2,7e-2*7e-2,
                              1e-1*1e-1,15e-2*15e-2,2e-1*2e-1,25e-2*25e-2,3e-1*3e-1,4e-1*4e-1,5e-1*5e-1,
                              6e-1*6e-1,7e-1*7e-1,8e-1*8e-1,9e-1*9e-1,1e0*1e0,15e-1*15e-1, 2e0*2e0
                              }; */
  std::vector<double> varianceR{ 1e-2,pow(10,-1.7),pow(10,-1.3),1e-1,pow(10,-0.7),pow(10,-0.3),1e0,pow(10,0.3),pow(10,0.7),1e1,pow(10,1.3),pow(10,1.7), 1e2};
  analysis.setVariableSet(0,varianceL);
  analysis.setVariableSet(1,varianceR);

  PerformanceModel::ParamType paramType = PerformanceModel::ParamType::SENSORNOISE;

  switch(AnalysisType::MONTECARLO){
    case AnalysisType::REPRESENTATIVE:
      runRepresentativeAnalysis(analysis, paramType);
      break;
    case AnalysisType::MONTECARLO:
      runMonteCarloAnalysis(analysis, paramType);
      break;
  }

  std::ofstream file;
  file.open("../out/noise/tmp/debug3-R05-M-analysis-1e5.txt");
  analysis.saveAnalysis(file);
  file.close();

  return 0;
}