#ifndef TRAJECTORYTEST_TASK_MODEL2_H
#define TRAJECTORYTEST_TASK_MODEL2_H

#include "world_model.h"
#include "fstream"

class TaskModel2{
public:
  TaskModel2();
  ~TaskModel2();

  void simulate(std::vector<State> trajectory, Pose initialPose, double &costPos, double &costVel, double &costTheta);

  void setModelLinear(double xi, double wn);
  void setModelRotation(double xi, double wn);

private:
  void controlLaw(State reference, State currentState, double command[3]);

  void updateCost(State currentState, State reference, double& costP, double& costV, double& costA);

  double kp[3], kd[3];

  double delayTime, controlPeriod, stepPeriod;
  double maxLVel, maxAVel, maxLAcc, maxAAcc;

  WorldModel worldModel;


  double radialControl, distControl, angleControl;
  std::ofstream filePos, fileVel, fileInput, fileError, fileNoise, fileFinalState;

  double wrap2pi(double angle);

};

#endif