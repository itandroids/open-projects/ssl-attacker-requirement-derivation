#include "task_model2D.h"
#include "vector"
#include "types.h"
#include "math.h"
#include "iostream"
#include "fstream"
#include "string"
#include <filesystem>
#include "helper.h"
#include "sampler.h"

namespace fs = std::filesystem;

int main(int argc, char ** argv){
  std::string path = "/home/gabriel/ITA/Mestrado/development/RequirementDerivation/src/scripts/trajectories";
  std::string aux;

  int nPoints = 0;
  std::vector<std::vector<State>> trajectories;
  std::vector<std::string> trajNames;
  State auxState;
  double t,x,y,theta,dxdt,dydt,dthetadt;
  const int nColumns = 7;
  for (const auto & entry : fs::directory_iterator(path)){
    std::ifstream file;
    file.open(entry.path());
    if (!file.is_open()){
      std::cout << "failed to open file\n";
      break;
    }
    for(int i = 0; i < nColumns; i++){
      file >> aux;
    }
    std::vector<State> traj;
    while(file >> t >> x >> y >> theta >> dxdt >> dydt >> dthetadt){
      auxState.x=x;
      auxState.y=y;
      auxState.theta=theta;
      auxState.vx=dxdt;
      auxState.vy=dydt;
      auxState.w=dthetadt;
      traj.push_back(auxState);
      nPoints++;
    }
    trajNames.push_back(entry.path());
    trajectories.push_back(traj);
    file.close();
  }

  double costPos, costVel, costTheta;
  Pose initialPose;
  TaskModel2D task;
  costPos = 0;
  costVel = 0;
  costTheta = 0;
  
  int i = 0;
  int maxCostPosId=0, maxCostVelId=0, maxCostThetaId=0;
  double maxCostPos = 0, maxCostVel = 0, maxCostTheta = 0;
  double currentCostPos = 0, currentCostVel = 0, currentCostTheta = 0;


  double xiLinear = 0.7, fLinear = 30;
  double xiRotation = 0.7, fRotation = 60;
  double kpTranslational = 10, kdTranslational = 0.5;
  double kp = 10, kd = 3;
  task.setModelLinear(xiLinear,2*M_PI*fLinear);
  task.setModelRotation(xiRotation,2*M_PI*fRotation);
  task.setTranslationalControl(kpTranslational,kdTranslational);
  task.setRotationalControl(kp,kd);
  
  /* for(auto trajectory : trajectories){ */
  auto trajectory = trajectories[1190];
    currentCostPos = 0;
    currentCostVel = 0;
    currentCostTheta = 0;
    initialPose.x = trajectory[0].x;
    initialPose.y = trajectory[0].y;
    initialPose.theta = trajectory[0].theta;
    task.simulate(trajectory,initialPose,currentCostPos,currentCostVel,currentCostTheta);
    costPos+=currentCostPos;
    costVel+=currentCostVel;
    costTheta+=currentCostTheta;
    if(currentCostPos/trajectory.size()>maxCostPos){
      maxCostPosId = i;
      maxCostPos = currentCostPos/trajectory.size();
    }
    if(currentCostVel/trajectory.size()>maxCostVel){
      maxCostVelId = i;
      maxCostVel = currentCostVel/trajectory.size();
    }
    if(currentCostTheta/trajectory.size()>maxCostTheta){
      maxCostThetaId = i;
      maxCostTheta = currentCostTheta/trajectory.size();
    }
    i++;
  /* } */
  
  std::cout << "Mean costPos: " << sqrt(costPos/nPoints) << "\n";
  std::cout << "Mean costVel: " << sqrt(costVel/nPoints) << "\n";
  std::cout << "Mean costTheta: " << sqrt(costTheta/nPoints) << "\n";

  std::cout << "maxCostPosId: " <<  maxCostPosId << " maxCostPos: " << maxCostPos <<"\n";
  std::cout << trajNames[maxCostPosId] << "\n";
  std::cout << "maxCostVelId: " <<  maxCostVelId << " maxCostVel: " << maxCostVel  << "\n";
  std::cout << trajNames[maxCostVelId] << "\n";
  std::cout << "maxCostThetaId: " <<  maxCostThetaId << " maxCostTheta: " << maxCostTheta  << "\n";
  std::cout << trajNames[maxCostThetaId] << "\n";
  
  return 0;
}