#include "sampler.h"
#include "vector"
#include "vector2.h"
#include "functional"
#include "iostream"
#include "math.h"

double distance1(Point<double> p1, Point<double> p2, Point<double> p3){
  return abs(p3.x-p1.x);
  //abs((p3.x-p1.x)*p2.y+(p2.x-p3.x)*p1.y+(p1.x-p2.x)*p3.y)\
        /sqrt((p3.x-p1.x)*(p3.x-p1.x)+(p3.y-p1.y)*(p3.y-p1.y));
}

double distance2(Point<std::vector<Point<double>>> p1, 
                  Point<std::vector<Point<double>>> p2, 
                  Point<std::vector<Point<double>>> p3){
  double mean1 = 0, mean2 = 0, mean3 = 0;
  for(Point<double> elem : p1.y){
    mean1 += elem.y;
  }
  mean1 /= p1.y.size();

  for(Point<double> elem : p2.y){
    mean2 += elem.y;
  }
  mean2 /= p2.y.size();

  for(Point<double> elem : p3.y){
    mean3 += elem.y;
  }
  mean3 /= p3.y.size();

  return abs(p3.x-p1.x);//abs((p3.x-p1.x)*mean2+(p2.x-p3.x)*mean1+(p1.x-p2.x)*mean3)\
        /sqrt((p3.x-p1.x)*(p3.x-p1.x)+(mean3-mean1)*(mean3-mean1));
}

double testFunction1(double x, double y){
  return 1/(1+exp(-x))+1/(1+exp(-y));
}

std::vector<Point<double>> testFunction2(double x){
  std::function<double(double)> f1 = [=](double y){return testFunction1(x,y);};
  return sampler<double>(-10,10,10,f1);
}

int main(){
  auto res = sampler<std::vector<Point<double>>>(-10,10,10,testFunction2);

  for(Point<std::vector<Point<double>>> elem1 : res){
    for(Point<double> elem2 : elem1.y){
      std::cout << elem1.x << " " << elem2.x << " " << elem2.y << std::endl;
    }
  }

  return 0;
}