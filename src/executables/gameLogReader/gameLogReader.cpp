#include "logfile.h"
#include "iostream"

#include "fstream"

#include "ssl_referee.pb.h"
#include "messages_robocup_ssl_wrapper_legacy.pb.h"
#include "messages_robocup_ssl_wrapper.pb.h"

struct Frame {
        qint64 time;
        MessageType type;
        QByteArray data;
};

int main(){
  QString filename("2019-07-07_06-58_RoboIME-vs-UBC_Thunderbots.log");

  std::ofstream visionLog, commandLog;
  visionLog.open("../doc/gamelogs/visionLog17.txt");
  commandLog.open("../doc/gamelogs/commandLog17.txt");

  LogFile logFile(filename);

  if(!logFile.openRead()){
    return 1;
  }

  QList<Frame*> packets;

  for (;;) {
      Frame* packet = new Frame;

      if (!logFile.readMessage(packet->data, packet->time, packet->type)) {
          delete packet;
          break;
      }

      packets.append(packet);
  }

  RoboCup2014Legacy::Wrapper::SSL_WrapperPacket legacyVisionPacket;
  SSL_WrapperPacket visionPacket;
  SSL_Referee refereePacket;


  visionLog << "frameNumber" << " " 
            << "tCapture" << " " 
            << "tSent" << " "  
            << "isBlueTeam" << " " 
            << "cameraId" << " " 
            << "robotId" << " " 
            << "x" << " " 
            << "y" << " " 
            << "theta" << "\n";
  visionLog.precision(15);

  commandLog << "commandTimestamp commandId\n";

  for(Frame* packet : packets){
    if (packet->type == MESSAGE_BLANK) {
        // ignore
    } else if (packet->type == MESSAGE_UNKNOWN) {
        // OK, let's try to figure this out by parsing the message
        if (refereePacket.ParseFromArray(packet->data.data(), packet->data.size())) {
            
        } else if (legacyVisionPacket.ParseFromArray(packet->data.data(), packet->data.size())) {
            
        } else {
            std::cout << "Error unsupported or corrupt packet found in log file!" << std::endl;
        }
    } else if (packet->type == MESSAGE_SSL_VISION_2010) {
        
    } else if (packet->type == MESSAGE_SSL_REFBOX_2013) {
        /*if(refereePacket.ParseFromArray(packet->data.data(), packet->data.size())){
          commandLog << refereePacket.command_timestamp() << " "
                      << refereePacket.command() << "\n";
        }*/
    } else if (packet->type == MESSAGE_SSL_VISION_2014) {
      if(visionPacket.ParseFromArray(packet->data.data(), packet->data.size())){
        if(visionPacket.has_detection()){
          for(int i = 0; i < visionPacket.detection().robots_yellow_size(); i++){
              visionLog << visionPacket.detection().frame_number() << " " 
                        << visionPacket.detection().t_capture() << " " 
                        << visionPacket.detection().t_sent() << " " 
                        << 0 << " "
                        << visionPacket.detection().camera_id() << " " 
                        << visionPacket.detection().robots_yellow(i).robot_id() << " " 
                        << visionPacket.detection().robots_yellow(i).x() << " " 
                        << visionPacket.detection().robots_yellow(i).y() << " " 
                        << visionPacket.detection().robots_yellow(i).orientation() << "\n";
          }
          for(int i = 0; i < visionPacket.detection().robots_blue_size(); i++){
              visionLog << visionPacket.detection().frame_number() << " " 
                        << visionPacket.detection().t_capture() << " " 
                        << visionPacket.detection().t_sent() << " " 
                        << 1 << " "
                        << visionPacket.detection().camera_id() << " " 
                        << visionPacket.detection().robots_blue(i).robot_id() << " " 
                        << visionPacket.detection().robots_blue(i).x() << " " 
                        << visionPacket.detection().robots_blue(i).y() << " " 
                        << visionPacket.detection().robots_blue(i).orientation() << "\n";
          }
        }
      }
      
    } else {
        std::cout << "Error unsupported message type found in log file!" << std::endl;
    }
  }


  return 0;
}