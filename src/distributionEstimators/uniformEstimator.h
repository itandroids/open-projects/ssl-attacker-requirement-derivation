#ifndef DISTRIBUTION_ESTIMATORS_UNIFORM_ESTIMATOR_H
#define DISTRIBUTION_ESTIMATORS_UNIFORM_ESTIMATOR_H

#include "distributionEstimator.h"
#include "representativeGrid.h"
#include <fstream>
#include <vector>
#include <math.h>

class UniformEstimator : public DistributionEstimator{
public:
  struct Model{
    double min, max;
    bool local;
  };
  void setModels(std::vector<Model> models);
  void estimate(RepresentativeGrid&  representativeGrid) override;
private:
  double getProbability(int id, int varId, const std::vector<double>& variableRange);
  double cumulativeProbability(double x, int id);
  std::vector<Model> models;
};

#endif