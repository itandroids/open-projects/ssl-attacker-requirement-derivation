#include "uniformEstimator.h"
#include <iostream>

void UniformEstimator::estimate(RepresentativeGrid&  representativeGrid){
  representativeSamples = representativeGrid.getRepresentativeSamples();
  int nSamples = representativeSamples.size();
  int nVariables = representativeSamples[0].size();
  distribution = std::vector<double>(nSamples,1);

  for(int i = 0; i < nSamples; i++){
    const std::vector<int> & relativeId = representativeGrid.getRelativeId(i);
    for(int j = 0; j < nVariables; j++){
      if(models[j].local){
        const std::vector<double>& variableRange = representativeGrid.getLocalRange(j);
        distribution[i] *= getProbability(relativeId[j],j,variableRange);
      }
      else{
        const std::vector<double>& variableRange = representativeGrid.getGlobalRange(j,relativeId);
        distribution[i] *= getProbability(relativeId[j],j,variableRange);
      }
    }
  }
  return;
}

void UniformEstimator::setModels(std::vector<Model> models){
  this->models = models;
}

double UniformEstimator::getProbability(int id, int varId, const std::vector<double>& variableRange){
  if(id == 0)
    return cumulativeProbability(0.5*variableRange[id]+0.5*variableRange[id+1], varId);

  if(id == (variableRange.size()-1))
    return (1 - cumulativeProbability(0.5*variableRange[id]+0.5*variableRange[id-1], varId));

  return (cumulativeProbability(0.5*variableRange[id]+0.5*variableRange[id+1], varId)-
          cumulativeProbability(0.5*variableRange[id]+0.5*variableRange[id-1], varId));
}

double UniformEstimator::cumulativeProbability(double x, int id){
  if(x < models[id].min)
    return 0;
  else if(x >= models[id].max)
    return 1;
  else
    return (x-models[id].min)/(models[id].max-models[id].min);
}