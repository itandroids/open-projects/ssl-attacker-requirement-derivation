#include "distributionEstimator.h"

void DistributionEstimator::saveDistribution(std::ofstream &fileHandler){
  int nSamples = distribution.size();
  int nVariables = representativeSamples[0].size();
  fileHandler << nSamples << " " << nVariables << "\n";
  for(int i = 0; i < nSamples; i++){
    for(int j = 0; j < nVariables; j++){
      fileHandler << representativeSamples[i][j] << " ";
    }
    fileHandler << distribution[i] << "\n";
  }

  return;
}

void DistributionEstimator::loadDistribution(std::ifstream & fileHandler){
  int nSamples, nVariables;
  fileHandler >> nSamples >> nVariables;
  distribution.resize(nSamples);
  representativeSamples = std::vector<std::vector<double>>(nSamples,std::vector<double>(nVariables,0));
  for(int i = 0; i < nSamples; i++){
    for(int j = 0; j < nVariables; j++){
      fileHandler >> representativeSamples[i][j];
    }
    fileHandler >> distribution[i];
  }

  return;
}