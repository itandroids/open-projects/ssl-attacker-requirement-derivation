#ifndef DISTRIBUTION_ESTIMATOR_H
#define DISTRIBUTION_ESTIMATOR_H

#include <vector>
#include <fstream>
#include "representativeGrid.h"

class DistributionEstimator{
public:
  virtual void estimate(RepresentativeGrid&  representativeGrid) = 0;
  void saveDistribution(std::ofstream &fileHandler);
  void loadDistribution(std::ifstream &fileHandler);
protected:
  std::vector<double> distribution;
  std::vector<std::vector<double>> representativeSamples;
};

#endif