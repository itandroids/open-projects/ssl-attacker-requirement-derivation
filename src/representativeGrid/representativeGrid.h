#ifndef REPRESENTATIVE_GRID_H
#define REPRESENTATIVE_GRID_H

#include <vector>
#include "helper.h"

class RepresentativeGrid{
  struct Variable{
    int parentId, n;
    double min, max;
  };
public:
  RepresentativeGrid(int nVariables);
  void setLinearVariable(int id, double min, double max, int n);
  void setRelativeVariable(int id, int parentId, double min, double max, int n);
  void computeRepresentativeSamples();
  const std::vector<std::vector<double>> &getRepresentativeSamples(); 

  int getClosestSampleId(std::vector<double> sample);

  const std::vector<double> &getGlobalRange(int varId, const std::vector<int> & relativeId);
  const std::vector<double> &getLocalRange(int varId);
  const std::vector<int> &getRelativeId(int id);

  std::vector<int> getPriorityOrder();
private:
  std::vector<int> getPriorityOrder(std::vector<Variable> &variables);

  std::vector<std::vector<double>> samples;
  std::vector<Variable> variables;
  std::vector<double> variableRange;
  std::vector<int> relativeId;
};

#endif