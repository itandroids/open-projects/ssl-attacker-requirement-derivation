#include "representativeGrid.h"
#include "iostream"

RepresentativeGrid::RepresentativeGrid(int nVariables){
  variables.resize(nVariables);
}

void RepresentativeGrid::setLinearVariable(int id, double min, double max, int n){
  variables[id].min = min;
  variables[id].max = max;
  variables[id].n = n;
  variables[id].parentId = -1;
}

void RepresentativeGrid::setRelativeVariable(int id, int parentId, double min, double max, int n){
  variables[id].min = min;
  variables[id].max = max;
  variables[id].n = n;
  variables[id].parentId = parentId;
}

const std::vector<std::vector<double>> &RepresentativeGrid::getRepresentativeSamples(){
  return samples;
}

void RepresentativeGrid::computeRepresentativeSamples(){
  int total = 1, nVariables = variables.size();
  for(int i = 0; i < nVariables; i++){
    total *= variables[i].n;
  }
  samples = std::vector<std::vector<double>>(total, std::vector<double>(nVariables,0));
  
  std::vector<int> priorityOrder = getPriorityOrder(variables);
  
  int r = 0, l = 0, id = 0;
  for(int i = 0; i < total; i++){
    r = i;
    for(int j = 0; j < nVariables; j++){
      id = priorityOrder[j];

      if(variables[id].parentId == -1){
        l = r % variables[id].n;
        r = r / variables[id].n;
        samples[i][id] = variables[id].min + l*(variables[id].max - variables[id].min)/(variables[id].n-1);
      }
      else if(variables[id].parentId < id){
        l = r % variables[id].n;
        r = r / variables[id].n;
        samples[i][id] = samples[i][variables[id].parentId] + 
                        variables[id].min + l*(variables[id].max - variables[id].min)/(variables[id].n-1);
      }
      else{
        throw "Invalid state\n";
      }
    }
  }
}

int RepresentativeGrid::getClosestSampleId(std::vector<double> sample){
  if(sample.size() != variables.size()){
    throw "invalid sample\n";
  }
  int nVariables = variables.size();
  int varId = 0;
  std::vector<int> relativeId(nVariables,0);
  std::vector<double> variableRange;
  std::vector<double> closestSample(nVariables,0);

  std::vector<int> priorityOrder = getPriorityOrder(variables);

  for(int i = 0; i < nVariables; i++){
    varId = priorityOrder[i];
    if(variables[varId].parentId == -1){
      variableRange = linspace(variables[varId].min,
                              variables[varId].max,
                              variables[varId].n);
    }
    else{
      variableRange = linspace(closestSample[variables[varId].parentId]+variables[varId].min,
                              closestSample[variables[varId].parentId]+variables[varId].max,
                              variables[varId].n);
    }
    relativeId[varId] = closest(sample[varId],variableRange);
    closestSample[varId] = variableRange[relativeId[varId]];
  }
  int id = 0, prod = 1;
  for(int i = 0; i < nVariables; i++){
    id+=prod*relativeId[i];
    prod *= variables[i].n;
  }
  return id;
}

std::vector<int> RepresentativeGrid::getPriorityOrder(){
  return getPriorityOrder(variables);
}

std::vector<int> RepresentativeGrid::getPriorityOrder(std::vector<Variable> &variables){
  int nVariables = variables.size();
  int orderId = 0, h = 0;
  std::vector<int> priorityOrder(nVariables,0);
  std::vector<int> onHold(nVariables,0);
  for(int i = 0; i < nVariables; i++){
    if(variables[i].parentId > i){
      onHold[h] = i;
      h++;
    }
    else if(variables[i].parentId < i && variables[i].parentId >= -1){
      priorityOrder[orderId] = i;
      orderId++;
    }
    else{
      std::cout << "Invalid parentId\n";
      throw "Invalid parentId.\n";
    }
  }
  for(int i = h-1; i >= 0; i--){
    priorityOrder[orderId] = i;
    orderId++;
  }

  return priorityOrder;
}

const std::vector<double> & RepresentativeGrid::getLocalRange(int varId){
  variableRange = linspace(variables[varId].min, variables[varId].max, variables[varId].n);
  return variableRange;
}
const std::vector<double> & RepresentativeGrid::getGlobalRange(int varId, const std::vector<int> & relativeId){
  double parentValue;
  if(variables[varId].parentId == -1){
    return getLocalRange(varId);
  }
  else {
    variableRange = getGlobalRange(variables[varId].parentId, relativeId);
    parentValue = variableRange[relativeId[variables[varId].parentId]];
    variableRange = linspace(parentValue+variables[varId].min,
                            parentValue+variables[varId].max,
                            variables[varId].n);
  }
  return variableRange;
}

const std::vector<int> &RepresentativeGrid::getRelativeId(int id){
  int nVariables = samples[0].size();
  relativeId.resize(nVariables);
  for(int i = 0; i < nVariables; i++){
    relativeId[i] = id % variables[i].n;
    id = id / variables[i].n;
  }
  return relativeId;
}