add_library(performanceAnalysis "")

target_sources(performanceAnalysis
PRIVATE
  performanceAnalysis.cpp)

target_include_directories(performanceAnalysis PUBLIC ${CMAKE_CURRENT_LIST_DIR})
target_link_libraries(performanceAnalysis
PUBLIC
  probabilityEstimators)