#ifndef PERFORMANCE_ANALYSIS_H
#define PERFORMANCE_ANALYSIS_H

#include <vector>
#include "probabilityEstimator.h"
#include <fstream>

class PerformanceAnalysis{

public:
  PerformanceAnalysis(int nVariables);
  void setVariableSet(int id, std::vector<double> &variableSet);
  void setProbabilityEstimator(ProbabilityEstimator &estimator);
  void analyze();
  void saveAnalysis(std::ofstream &fileHandler);
private:
  int nVariables;
  ProbabilityEstimator* estimator;
  std::vector<std::vector<double>> variableSet;
  std::vector<double> analysisResult;
};



#endif