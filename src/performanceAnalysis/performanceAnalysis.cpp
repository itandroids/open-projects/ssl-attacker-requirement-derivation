#include "performanceAnalysis.h"
#include "iostream"
#include "chrono"

PerformanceAnalysis::PerformanceAnalysis(int nVariables):
  nVariables(nVariables){
  variableSet.resize(nVariables);
}

void PerformanceAnalysis::setVariableSet(int id, std::vector<double>& variableSet){
  this->variableSet[id] = variableSet;
}

void PerformanceAnalysis::setProbabilityEstimator(ProbabilityEstimator & estimator){
  this->estimator = &estimator;
}

void PerformanceAnalysis::analyze(){
  int total = 1;
  for(int i = 0; i < nVariables; i++){
    total *= variableSet[i].size();
  }
  analysisResult.resize(total);
  std::vector<double> combination(nVariables,0);
  std::vector<int> combinationId(nVariables,0);
  int k = 0;
  auto start = std::chrono::steady_clock::now();
  auto end = start;
  std::chrono::duration<double> elapsedTime;
  int seconds;
  int minutes;
  int hours;
  for(int i = 0; i < total; i++){
    end =  std::chrono::steady_clock::now();
    elapsedTime = end-start;
    hours = ((int)elapsedTime.count())/3600;
    minutes = ((int)elapsedTime.count())/60-60*hours;
    seconds = (int)elapsedTime.count()-60*minutes-3600*hours;
    std::cout << "[t = " << hours << ":" << minutes << ":" << seconds << "] ";
    std::cout << "PerformanceAnalysis: ("<<i+1<< "/" << total << ") - combination:";
    k = i;
    for(int j = 0; j < nVariables; j++){
      std::cout << " " << variableSet[j][k%variableSet[j].size()];
      combination[j] = variableSet[j][k%variableSet[j].size()];
      combinationId[j] = k%variableSet[j].size();
      k /= variableSet[j].size();
    }
    std::cout << "\n";
    analysisResult.at(i) = estimator->estimate(combination);
  }
}

void PerformanceAnalysis::saveAnalysis(std::ofstream & fileHandler){
  int total = analysisResult.size();
  int k = 0;
  for(int i = 0; i < total; i++){
    k = i;
    for(int j = 0; j < nVariables; j++){
      fileHandler << variableSet[j][k%variableSet[j].size()] << " ";
      k /= variableSet[j].size();
    }
    fileHandler << analysisResult.at(i) << "\n";
  }
}