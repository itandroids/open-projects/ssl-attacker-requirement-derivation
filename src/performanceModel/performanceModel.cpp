#include "performanceModel.h"

PerformanceModel::PerformanceModel(TaskModel &taskModel){
  this->taskModel = &taskModel;
}

void PerformanceModel::setTolerance(double tol){
  this->tol = tol;
}
void PerformanceModel::setParameters(std::vector<double> parameters, ParamType type){
  switch(type){
    case ParamType::RESOLUTION:
      taskModel->setResolution(parameters[0], parameters[1]);
      break;
    case ParamType::SENSORNOISE:
      taskModel->setEstimatorVarianceL(parameters[0]);
      taskModel->setEstimatorVarianceR(parameters[1]);
      break;
  }
}

void PerformanceModel::setNumberOfIterations(int n){
  nIterations = n;
}

double PerformanceModel::getPerformance(const std::vector<double> &sample){
  Interception interception;
  Pose robotPose;
  double Bx, By, Tx, Ty, vBx, vBy, d, Ix, Iy;
  if(sample.size() == 3){
    interception.theta = sample[0];
    interception.time = sample[2];
    robotPose.theta = sample[1];
    robotPose.x = GoalieTrajectory::GOALIE_RADIUS * cos(robotPose.theta);
    robotPose.y = GoalieTrajectory::GOALIE_RADIUS * sin(robotPose.theta);
  }
  else if(sample.size() == 4){
    Bx = sample[1]*cos(sample[0]);
    By = sample[1]*sin(sample[0]);
    Tx = -0.5 + sample[3]*1.0;
    Ty = 0;
    vBx = (Tx - Bx) / sqrt((Tx-Bx)*(Tx-Bx)+(Ty-By)*(Ty-By));
    vBy = (Ty - By) / sqrt((Tx-Bx)*(Tx-Bx)+(Ty-By)*(Ty-By));
    d = -(Bx*vBx+By*vBy)-sqrt(
      GoalieTrajectory::GOALIE_RADIUS*GoalieTrajectory::GOALIE_RADIUS-
      (Bx*vBy-By*vBx)*(Bx*vBy-By*vBx));
    Ix = Bx + d*vBx;
    Iy = By + d*vBy;
    interception.theta = atan2(Iy,Ix);
    interception.time = d / GoalSampling::BALL_SPEED;
    robotPose.theta = sample[2];
    robotPose.x = GoalieTrajectory::GOALIE_RADIUS * cos(robotPose.theta);
    robotPose.y = GoalieTrajectory::GOALIE_RADIUS * sin(robotPose.theta);
  }
  else{
    throw "Invalid sample\n";
  }

  int nSuccesses = 0;
  for(int i = 0; i < nIterations; i++){
    nSuccesses += (fabs(taskModel->simulate(interception,robotPose,
                                      randomFactory.getUniformValue(0,1))) < tol);
  }
  return (1.0*nSuccesses) / nIterations;
}