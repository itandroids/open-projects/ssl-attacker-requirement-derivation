#ifndef PERFORMANCE_MODEL_H
#define PERFORMANCE_MODEL_H

#include "task_model.h"
#include "random_factory.h"
#include "types.h"
#include "constants.h"

class PerformanceModel{
public:
enum ParamType{
  RESOLUTION,
  SENSORNOISE
};


  PerformanceModel(TaskModel &taskModel);
  void setTolerance(double tol);
  void setNumberOfIterations(int n);
  void setParameters(std::vector<double> parameters, ParamType type);
  double getPerformance(const std::vector<double> &sample);
private:
  TaskModel* taskModel;
  double tol;
  int nIterations;
  RandomFactory randomFactory;
};


#endif